<?php
class Social_media extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['media']=$this->Crud->getRows('social_media','*');
		$data['content']='social_media/all_links';
		$this->template->admin_template($data);
	}
	
	function add_link()
	{
		$data['content']='social_media/add_link';
		$this->template->admin_template($data);
	}
	function insert_link(){
		$table = 'social_media';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "100" || $height > "90") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 53 * 52 pixels ');
   redirect(SURL.'testimonials/add_testimonial/');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/testimonials/';
                        $projects_folder_path_main = './assets/images/testimonials/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

            $data = array(
				"title" => $title,
				"image" => $filename_f,
				"link" => $link,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Social media link is added successfully</h5>');
                redirect(SURL.'social_media');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add Social media link please try again</h5>');
                redirect(SURL.'social_media/add_link');
            }
		}
	}


	function edit_link($id){
		$table = 'social_media';
		$id = $id;
		$where = array('id' => $id );
		$data['media'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'social_media/update_link';
		$this->template->admin_template($data);
	}

	function update_link($id){
		$table = 'social_media';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "100" || $height > "90") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 100 * 90 pixels ');
   redirect(SURL.'social_media/edit_link/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/testimonials/';
                        $projects_folder_path_main = './assets/images/testimonials/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

			$data = array(
				"title" => $title,
				"image" => $filename_f,
				"link" => $link,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Social Media Link is updated successfully</h5>');
                redirect(SURL.'social_media');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update Social Media Link please try again</h5>');
                redirect(SURL.'social_media/edit_link/'.$id);
            }
		

	}

	function del_row($id){
			$table = 'social_media';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Social media link is deleted successfully</h5>');
                redirect(SURL.'social_media');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete Social media link please try again</h5>');
                redirect(SURL.'social_media');
            }
	}  
    function activate_link($id){

        $table='social_media';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Social media link is activated successfully</h5>');
                redirect(SURL.'social_media');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate Social media link please try again</h5>');
                redirect(SURL.'social_media');
            }
    }
    function deactivate_link($id){
        $table='social_media';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Social media link is deactivated successfully</h5>');
                redirect(SURL.'social_media');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate Social media link please try again</h5>');
                redirect(SURL.'social_media');
            }
    }

}
 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Add Social Media Link</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Social Media Link</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'social_media/insert_link'?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Link</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="link" id="link" />
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select id="status" name="status" class="form-control">
									<option>--SELECT--</option>
									<option value="true">Active</option>
									<option value="false">Deactive</option>
								</select>
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Icon Image</label>
							<div class="col-sm-5">
								<input type="file" name="image" id="image" />
							</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Add Link</button>
							<a role="button" href="<?=SURL?>social_media" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

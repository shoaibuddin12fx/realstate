<?php

class Builders extends MY_Controller {

	function __construct(){

		parent::__construct();
			$this->load->library('pagination');
	}
		
	function index()
	{
       $data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"builder"),'display_order','asc','','');	
       $data['builders']=$this->Crud->getR('project_categories','*',array("status"=>"true"),'','','','');
       $data['content']='builders/all_builders';
       $this->template->listing_page_layout($data);
	}
	function builder_detail($slug='')
	{ 


		$data['builder_detail']=$this->Crud->getRows('project_categories','*',array("slug"=>$slug));
		$cat_id=$data['builder_detail'][0]['cat_id'];

			$page=1;


				  $this->load->library("pagination");
				  $config = array();
				  $config["base_url"] = "#";
				  $config["total_rows"] = $this->Crud->count_all($cat_id);
				  $config["per_page"] = 6;
				  $config["uri_segment"] = 3;
				  $config["use_page_numbers"] = TRUE;
                                  $config["full_tag_open"] = '<ul style="list-style: none;
    display: -webkit-box;">';
                                  $config["full_tag_close"] = '</ul>';
                                  $config["first_tag_open"] = '';
                                  $config["first_tag_close"] = '';
                                  $config["last_tag_open"] = '';
                                  $config["last_tag_close"] = '';
                                  $config['next_link'] = '&gt;';
                                  $config["next_tag_open"] = '<li>';
                                  $config["next_tag_close"] = '</li>';
                                  $config["prev_link"] = "&lt;";
                                  $config["prev_tag_open"] = "<li>";
                                  $config["prev_tag_close"] = "</li>";
                                  $config["cur_tag_open"] = "<li><a  class='active' href='#/1' data-ci-pagination-page='1'>";
                                  $config["cur_tag_close"] = "</a></li>";
                                  $config["num_tag_open"] = "<li>";
                                  $config["num_tag_close"] = "</li>";
                                  $config["num_links"] = 2;
				  $this->pagination->initialize($config);
			  $start = ($page - 1) * $config["per_page"];

				   $data['pagination_link']=$this->pagination->create_links();
				   $data['country_table']= $this->Crud->fetch_details($config["per_page"],$start,$cat_id,'display_order','asc');
		$data['builder_agents']=$this->Crud->getRows('agents','*',array("project_cat"=>$data['builder_detail'][0]['cat_id']));
		$data['content']='builders/builder_detail';
        $this->template->listing_page_layout($data);
	}
       function pagination($page){
                $slug=$this->uri->segment(4);
                $data['builder_detail']=$this->Crud->getRows('project_categories','*',array("slug"=>$slug));
		$cat_id=$data['builder_detail'][0]['cat_id'];
		$sgmnt=count($this->uri->segments);

				  $this->load->library("pagination");
				  $config = array();
				  $config["base_url"] = "#";
				  $config["total_rows"] = $this->Crud->count_all($cat_id);
				  $config["per_page"] = 6;
				  $config["uri_segment"] = 3;
				  $config["use_page_numbers"] = TRUE;
                                  $config["full_tag_open"] = '<ul style="list-style: none;
    display: -webkit-box;">';
                                  $config["full_tag_close"] = '</ul>';
                                  $config["first_tag_open"] = '';
                                  $config["first_tag_close"] = '';
                                  $config["last_tag_open"] = '';
                                  $config["last_tag_close"] = '';
                                  $config['next_link'] = '&gt;';
                                  $config["next_tag_open"] = '<li>';
                                  $config["next_tag_close"] = '</li>';
                                  $config["prev_link"] = "&lt;";
                                  $config["prev_tag_open"] = "<li>";
                                  $config["prev_tag_close"] = "</li>";
                                  $config["cur_tag_open"] = "<li><a  class='active' href='#/1' data-ci-pagination-page='1'>";
                                  $config["cur_tag_close"] = "</a></li>";
                                  $config["num_tag_open"] = "<li>";
                                  $config["num_tag_close"] = "</li>";
                                  $config["num_links"] = 2;
				  $this->pagination->initialize($config);
			  $start = ($page - 1) * $config["per_page"];

				  $output = array(
				   'pagination_link'  => $this->pagination->create_links(),
				   'country_table'   => $this->Crud->fetch_details($config["per_page"],$start,$cat_id,'display_order','asc'));
				  echo json_encode($output);
}

}
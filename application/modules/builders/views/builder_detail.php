<div id="demo" class="carousel slide" data-ride="carousel">

    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <?php if(!empty($builder_detail[0]['builder_gal'])){?>
        <img src="<?=IMG.'builders/'.$builder_detail[0]['builder_gal']?>" alt="Los Angeles" width="1100" height="500">
      <?php } ?>
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
          <p class="address"><?php if(!empty($builder_detail[0]['banner_address'])){echo $builder_detail[0]['banner_address'] ;}?></p>
          <h2><?php if(!empty($builder_detail[0]['banner_heading'])){echo $builder_detail[0]['banner_heading'] ;}?></h2>
          <p class="desc"><?php if(!empty($builder_detail[0]['banner_description'])){echo $builder_detail[0]['banner_description'] ;}?></p>
          <a href="#" class="caption-btn">Inquire Now</a>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-lg-5 mt-lg-3 pb-lg-3 project-overviw">   
   <div class="row mb-4">
      <div class="col-12  col-sm-10 col-lg-6 col-md-11 text-left pl-sm-0 pt-sm-3 pb-sm-2">
        <h4 class="font-weight-bold pb-sm-3">Project Overview</h4>
        <hr>
      </div>
    <div class="col-12  col-sm-10 col-lg-12 col-md-11 text-left pl-sm-0 pt-sm-3 pb-sm-3">
        <?php if(!empty($builder_detail[0]['description'])){
            $desc=nl2br($builder_detail[0]['description'],true) ;
            $desc = str_replace("<br />", "</p><p>", $desc);
            $desc = "<p class='text-justify'>" . $desc . "</p>";
            echo $desc;
          }?>
    </div>
    <div class="col-8  col-sm-6 col-lg-3 col-md-6 mx-auto mx-lg-0 text-center pl-sm-0 pt-sm-3 pb-sm-3 nl--project-detail-btn" >
      <a href="">VIEW PROJECT DETAILS</a>
    </div>
   </div>
  </section>
  <!--Section: Group of personal cards-->
</div>
<div class="nl--builder_projects">
<div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-lg-5 mt-lg-3 pb-lg-3">   
   <div class="row" id="country_table">
      <div class="col-12  col-sm-10 col-lg-11 col-md-11 text-left pl-sm-0 pt-sm-3 pb-sm-3 mb-sm-3">
        <h4 class="font-weight-bold pb-sm-3">Projects</h4>
        <hr>
      </div>
<?php if(!empty($country_table)){echo $country_table;}?>
    <!--Grid row-->
     </div>
        <div class="pag pl-sm-4" id="pagination_link" >
<?php if(!empty($pagination_link)){echo $pagination_link;}?>
        </div>
      
  </section>
  <!--Section: Group of personal cards-->
</div>
</div>
      <!-- listing page section starts here -->
  <!--Section: Group of personal cards-->
  <?php if(!empty($builder_agents)){ ?>
<div class="container">
  <div class="last_slider projects-testimonisl pb-5 col-sm-9 pt-sm-5">
    <h4 class="font-weight-bold pb-sm-3">Property Agents</h4>
    <hr>
    <div id="demo2">
        <div id="owl-demo1" class="owl-carousel">
          <?php foreach ($builder_agents as $key => $value) {
                  
            echo '<div class="item">
                    <div class="brands_col">
                        <div class="col-3">
                          <img src="'.IMG.'agents/'.$value['agent_image'].'" alt="" class="rounded-circle agent_image">
                        <div class="pt-1 ml-5 ml-md-0 pl-3">
                          <p class="ml-0 pl-3 name">'.$value['name'].'</p>
                        </div>
                        </div>
                      </div>
                  </div>';
          } ?>
        </div>
      </div>
    </div>
</div>
<?php } ?>
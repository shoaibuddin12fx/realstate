<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?=IMG.'sliders/'.$sliders[0]['image']?>" alt="Los Angeles" width="1100" height="500">
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
          <p class="address"><?=$sliders[0]['address']?></p>
          <h2><?=$sliders[0]['heading']?></h2>
          <p class="desc"><?=$sliders[0]['description']?></p>
          <a href="<?=$sliders[0]['link']?>" class="caption-btn"><?=$sliders[0]['button_title']?></a>
        </div>
      </div>
    </div>
  </div>

<div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-lg-5 mt-lg-5 pb-lg-3">
    <div class="row nl--projects">
     <div class="col-12 col-sm-2 col-lg-1 col-md-1 nl--icon-box">
     <div class="nl--icon-line-top  d-none d-lg-block"></div> 
        <i class="fa fa-3x fa-briefcase "></i>
      </div>
      <div class="col-12  col-sm-10 col-lg-11 col-md-11 text-left pl-sm-5 pt-sm-1">
        <h4 class="font-weight-bold">BUILDERS</h4>
        <p class="grey-text">Meet our intelligent business partners and learn about their projects. Next Level Real Estate has the best partners in real estate industry.</p>
      </div>
    </div>
              <ul class="nl--builders text-center pt-5 mt-3 mb-3">
                <?php foreach ($builders as $key => $value) {
                echo '<li><a href="'.SURL.'builders/builder_detail/'.$value['slug'].'"><img src="'.IMG.'builders/'.$value['proj_image'].'" class="m-auto img-fluid"></a></li>';
                } ?>
              </ul>
  </section>
  <!--Section: Group of personal cards-->
</div>
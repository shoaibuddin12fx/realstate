<?php

Class Contact_us extends MY_Controller {

		function __construct(){
			parent::__construct();
		}
		function index(){
			$table='sliders';
			$data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"contact"),'id','desc','','');      
			$this->template->contact_us_layout($data);

		}
		function contact_email(){

		if($this->input->post('btn')){
				$this->form_validation->set_rules('subject', 'Subject', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('name', 'Name', 'required');
				$this->form_validation->set_rules('message', 'Message', 'required');
		
		if($this->form_validation->run()== false){
			 $data['content']='contact_us#contact';
             $this->template->contact_us_layout($data);
			}else{
			   $data['footer']=$this->Crud->getRows('site_settings','*','','id','DESC');
               $name=$this->input->post('name');
               $email=$this->input->post('email');
               $subject=$this->input->post('subject');
               $message=$this->input->post('message');

               $name = $name;
			   $from_email = $email;
			   $subject = $subject;
			   $message =$name.'<br><br>'.$email.'<br><br>'.$message;
			   $to_email = $data['footer'][0]['contact_page_email'];	
			   $this->load->library('email');
			   $config = Array(
			    'protocol' =>  $data['footer'][0]['email_protocol'],
			    'smtp_host' =>  $data['footer'][0]['email_host'],
			    'smtp_port' => $data['footer'][0]['email_port'],
			    'smtp_user' => $data['footer'][0]['contact_page_email'],
			    'smtp_pass' => $data['footer'][0]['email_password'],
			     
			    'smtp_crypto'=>'ssl',
			    'mailtype'  => 'html',
			    'charset'   => 'iso-8859-1'
			            );

			 $this->load->library('email',$config); 
   			 $this->email->from($from_email, $name ); 
	         $this->email->to($to_email);
	         $this->email->subject($subject); 
	         $this->email->message($message);

		if($this->email->send()) {

	         	$this->session->set_flashdata('message', 'Your email has been sent');
	         	redirect(SURL."contact_us#contact");
	         }
	         else{	   

	         	$this->session->set_flashdata('failed', 'Failed to send email please try again');
	         	redirect(SURL."contact_us#contact");
	        }
		}
    }
  }
}
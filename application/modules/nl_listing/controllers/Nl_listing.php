<?php

Class Nl_listing extends MY_Controller {

		function __construct(){
			parent::__construct();
			$this->load->library('pagination');
		}

		function index($uri_segment="2"){

			$table='sliders';
			$data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"listing"),'id','desc','','');

                  $config = array();
                  $config["full_tag_open"] = '<ul style="list-style: none;
                display: -webkit-box;">';
                  $config["full_tag_close"] = '</ul>';
                  $config["first_tag_open"] = '';
                  $config["first_tag_close"] = '';
                  $config["last_tag_open"] = '';
                  $config["last_tag_close"] = '';
                  $config['next_link'] = '&gt;';
                  $config["next_tag_open"] = '<li>';
                  $config["next_tag_close"] = '</li>';
                  $config["prev_link"] = "&lt;";
                  $config["prev_tag_open"] = "<li>";
                  $config["prev_tag_close"] = "</li>";
                  $config["cur_tag_open"] = "<li><a  class='active' href=''>";
                  $config["cur_tag_close"] = "</a></li>";
                  $config["num_tag_open"] = "<li>";
                  $config["num_tag_close"] = "</li>";
                  $config["base_url"] = SURL.'nl_listing/index';
                  $config["total_rows"] = $this->Crud->count_properties();
                  $config["per_page"] = 6;
                  $config["uri_segment"] = 3;
                  $config['num_links'] = 6;
                  $this->pagination->initialize($config);
                  $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            			$data["records2"] = $this->Crud->get_all_properties($config["per_page"],$page);

            			$data['total_prop']=$this->Crud->count_properties();
            			$data["links"] = $this->pagination->create_links();//create the link for pagination
            		    //$data['mainpage'] = "nl_listing";
            			$data['content']='nl_listing/listing_view';      
            			$this->template->listing_page_layout($data);
		}
function search_detail(){
        $data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"listing"),'id','desc','','');
      if($this->input->post('submit') != NULL ){
      $search = array('prop_for' => $this->input->post('prop_for'),
                      'prop_title' => $this->input->post('key_words'),
                      'property_type' => $this->input->post('prop_type'),
                      'city' => $this->input->post('city'),
                      'address' => $this->input->post('location'),
                      'sale' => $this->input->post('sale'),
                      'land_estate' => $this->input->post('land_estate'),
                      'rent' => $this->input->post('rent'),
                      'share' => $this->input->post('share'),
                      'min_price' => $this->input->post('min_price'),
                      'max_price' => $this->input->post('max_price'),
                      'min_bedroom' => $this->input->post('min_bedroom'),
                      'max_bedroom' => $this->input->post('max_bedroom'),
                      'min_bathroom' => $this->input->post('min_bathroom'),
                      'max_bathroom' => $this->input->post('max_bathroom'),
                      'comercial' => $this->input->post('comercial'),
                      'residential' => $this->input->post('residential'),
                      'new_build' => $this->input->post('new_build'),
                      'pre_owned' => $this->input->post('pre_owned'));
      $this->session->set_userdata(array("prop_for"=>$search['prop_for'],"prop_title"=>$search['prop_title'],"property_type"=>$search['property_type'],"city"=>$search['city'],"address"=>$search['address'],"sale"=>$search['sale'],"land_estate"=>$search['land_estate'],"rent"=>$search['rent'],"share"=>$search['share'],"min_price"=>$search['min_price'],"max_price"=>$search['max_price'],"min_bedroom"=>$search['min_bedroom'],"max_bedroom"=>$search['max_bedroom'],"min_bathroom"=>$search['min_bathroom'],"max_bathroom"=>$search['max_bathroom'],"comercial"=>$search['comercial'],"residential"=>$search['residential'],"new_build"=>$search['new_build'],"pre_owned"=>$search['pre_owned']));
    }else{
      if($this->session->userdata() != NULL){
        $search =array('prop_for' => $this->session->userdata('prop_for'),'prop_title' => $this->session->userdata('prop_title'),'property_type' => $this->session->userdata('property_type'),'city' => $this->session->userdata('city'),'address' => $this->session->userdata('address'),'sale' => $this->session->userdata('sale'),'land_estate' => $this->session->userdata('land_estate'),'rent' => $this->session->userdata('rent'),'share' => $this->session->userdata('share'),'min_price' => $this->session->userdata('min_price'),'max_price' => $this->session->userdata('max_price'),'min_bedroom' => $this->session->userdata('min_bedroom'),'max_bedroom' => $this->session->userdata('max_bedroom'),'min_bathroom' => $this->session->userdata('min_bathroom'),'max_bathroom' => $this->session->userdata('max_bathroom'),'comercial' => $this->session->userdata('comercial'),'residential' => $this->session->userdata('residential'),'new_build' => $this->session->userdata('new_build'),'pre_owned' => $this->session->userdata('pre_owned'));
      }
    }
                  $config = array();
                  $config["full_tag_open"] = '<ul style="list-style: none;
                display: -webkit-box;">';
                  $config["full_tag_close"] = '</ul>';
                  $config["first_tag_open"] = '';
                  $config["first_tag_close"] = '';
                  $config["last_tag_open"] = '';
                  $config["last_tag_close"] = '';
                  $config['next_link'] = '&gt;';
                  $config["next_tag_open"] = '<li>';
                  $config["next_tag_close"] = '</li>';
                  $config["prev_link"] = "&lt;";
                  $config["prev_tag_open"] = "<li>";
                  $config["prev_tag_close"] = "</li>";
                  $config["cur_tag_open"] = "<li><a  class='active' href=''>";
                  $config["cur_tag_close"] = "</a></li>";
                  $config["num_tag_open"] = "<li>";
                  $config["num_tag_close"] = "</li>";
                  $config['base_url'] = SURL.'nl_listing/search_detail/';
                  $config['total_rows'] = count($this->Crud->getProperties('','',$search));
                  $config["per_page"] = 6;
                  $config["uri_segment"] = 3;
                  $config['num_links'] = 5;
                  $this->pagination->initialize($config);

    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data['records2'] = $this->Crud->getProperties($config['per_page'], $page,$search);
 //     = $results;
     $data["links"] = $this->pagination->create_links();//create the link for pagination
     if($this->Crud->getProperties('','',$search)==0){
      $data['total_prop']=0;
     }else{
      $data['total_prop']=count($this->Crud->getProperties('','',$search));
     }
      
                  $data['content']='nl_listing/listing_view';      
                  $this->template->listing_page_layout($data);
 }
 function sort_prop(){
  $sort_by=$this->input->post('sort_by');
  if($this->input->post('page')){
     $page=$this->input->post('page');
   }else{
    $page=1;
   }
 
  if($this->input->post('sort_by')){
    $srt_by=$sort_by;
    $sot=array('soort_by'=>$srt_by);
    $this->session->set_userdata(array("soort_by"=>$srt_by));
  }else {
    if($this->session->userdata() != NULL){
      $sot=array('soort_by' => $this->session->userdata('soort_by'));
    }
  }
                  $config = array();
                  $config["full_tag_open"] = '<div class="pagee"><ul style="list-style: none;
                display: -webkit-box;">';
                  $config["full_tag_close"] = '</ul></div>';
                  $config["first_tag_open"] = '';
                  $config["first_tag_close"] = '';
                  $config["last_tag_open"] = '';
                  $config["last_tag_close"] = '';
                  $config['next_link'] = '&gt;';
                  $config["next_tag_open"] = '<li>';
                  $config["next_tag_close"] = '</li>';
                  $config["prev_link"] = "&lt;";
                  $config["prev_tag_open"] = "<li>";
                  $config["prev_tag_close"] = "</li>";
                  $config["cur_tag_open"] = "<li><a  class='active' href=''>";
                  $config["cur_tag_close"] = "</a></li>";
                  $config["num_tag_open"] = "<li>";
                  $config["num_tag_close"] = "</li>";
                  $config['base_url'] = SURL.'nl_listing/sort_prop/'.$page;
                  $config['total_rows'] = $this->Crud->count_properties();
                  $config["per_page"] = 6;
                  $config["uri_segment"] = 3;
                  $config['num_links'] = 6;
                  $this->pagination->initialize($config);
                  $page = ($page - 1) * $config["per_page"];
                  $output = array(
                 'pagination_link' =>$this->pagination->create_links(),
                 'properties' => $this->Crud->sorted_properties($config['per_page'],$page,$sot),
                  'total_prop'=> $this->Crud->count_properties()
                  );
                  echo json_encode($output);

 }
  function property_detail($prop_id){

        $data['property_detail']=$this->Crud->getR('properties','*',array("prop_id"=>$prop_id),'','','','');
        $data['agent']=$this->Crud->getR('agents','*',array("status"=>"true","property_id"=>$prop_id),'id','desc','','');
        $data['related_prop']=$this->Crud->getR('properties','*',array("status"=>"true","project_id"=>$data['property_detail'][0]['project_id']),'prop_id','desc','',3);
        $data['floor_plan']=$this->Crud->getR('property_floor_gallery','*',array("property_id"=>$prop_id),'id','desc','',1);
        $data['property_gallery']=$this->Crud->getR('property_gallery','*',array("property_id"=>$prop_id),'id','desc','',1);
        $data['property_variant']=$this->Crud->getR('property_variants','*',array("prope_id"=>$prop_id),'','','','');
        $data['content']='nl_listing/property_detail';      
        $this->template->listing_page_layout($data);
  }
  function property_variations(){
  $prop_id=$this->input->post('prop_id');
  $variations=$this->input->post('variations');
  $output = $this->Crud->getRows('property_variants','*',array("prope_id"=>$prop_id,"no_beds"=>$variations));
    echo json_encode($output);
  }
}
<style>
.col-sm-12{
  padding-right:0;
  padding-left:0;
  
}
html, body {
    max-width: 100%;
    overflow-x: hidden;
}
.nl--agents .detail{
  margin-left: 0;
}
.nl--request-button{
    height: 51px;
    width: 50%;
    text-align: center;
}
.nl--summery .nl--register-button,.nl--property-detail .nl--inquire-button,.nl--summery .nl--download-button{
  text-align:   center; 
  padding: 14px 0px;
}
.nl--card-personal .card img{
  height:   unset;  
}
@media screen and (max-width: 480px) {
.nl--inquire-button{
  width:100% !important;
}
.nl--request-button
{
  width:100% !important;
}
.nl--summery p{
    margin-bottom: 0px;
 }
.text-size-on_small{
  font-size: 1.5rem;
}
.text-size-on_small-1{
    text-align: right;
    margin-top: -23px;
    font-size: 18px;
}
.summery-hr{
  display:  none; 
}
.nl--summery .nl--download-button {
    background-color: black;
    width: 90%;
    margin: 10px auto;
    padding: 14px 20px;
    text-align: center;
}
.nl--summery .nl--register-button {
    background-color: #2c4362;
    width: 90%;
    margin: 10px auto;
    padding: 14px 20px;
    text-align: center;
}
.nl--property-detail .features ul{
      -webkit-column-count: 2;
    column-count: 2;
}
.nl--property-detail .description p{
  width: 100%;
}
.nl--property-detail .nl--inquire-button{
  margin-top: 20px;
  text-align: center;
}
.nl--agents .detail{
  margin-left: 0;
  margin: 0px 20px;
}
.nl--agents .social-links{
  margin-right: 40px;
}
.nl--agents .detail h4{
      text-align: left;
    margin-left: 40px;
}
}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
      <?php if(!empty($property_detail[0]['prop_baner_img'])){?>
        <img src="<?=IMG.'properties/'.$property_detail[0]['prop_baner_img']?>" alt="Los Angeles" width="1100" height="500">
      <?php }else{ ?>
    <img src="<?=IMG.'no_image.png'?>" alt="Los Angeles" width="1100" height="500">
    <?php } ?>
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker">
          <p class="address"><?php if(!empty($property_detail[0]['prop_baner_address'])){echo $property_detail[0]['prop_baner_address'] ;}?></p>
          <h2><?php if(!empty($property_detail[0]['prop_baner_heading'])){echo $property_detail[0]['prop_baner_heading'] ;}?></h2>
          <p class="desc"><?php if(!empty($property_detail[0]['prop_baner_desc'])){echo $property_detail[0]['prop_baner_desc'] ;}?></p>
          <a href="#" data-toggle="modal" data-target="#exampleModalCenter" class="caption-btn">INQUIRE NOW</a>
        </div>
      </div>
    </div>
  </div>
    <!-- Left and right controls -->

  </div>
<!-- summery portion starts here -->
<!-- summery portion starts here -->
    <div class="container"  <?php if(empty($property_detail[0]['prop_baner_img'])){ echo 'style="margin-top:15%"'; } ?>>
      <div class="nl--summery mt-3 mt-md-5">
           <div class="row">
              <div class="col-12 col-md-5">
                 <h3 class="text-size-on_small"><?php if(!empty($property_detail[0]['prop_title'])){echo $property_detail[0]['prop_title'] ;}?></h3><h3 class="mb-0" id="titl"></h3>
                 <p><i class="fa fa-map-marker"></i><?php if(!empty($property_detail[0]['address'])){echo $property_detail[0]['address'] ;}?></p>
              </div>
              <hr class="summery-hr"> 
              <div class="col-12 col-md-6">
                <h3 class="text-size-on_small-1" id="price"><?php if(!empty(number_format($property_detail[0]['price']))){echo $property_detail[0]['price'] ;}?> AED</h3>
              </div>
           </div>
           <div class="row">
             <div class="col-sm-12 col-xs-12 col-md-5 col-lg-8" style="padding-right:2%;padding-bottom:1%">
            <?php if(!empty($property_gallery[0]['gal_images'])){?>
             <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php 
                $gallry_images=explode(' , ',$property_gallery[0]['gal_images']);
                
               for($i=0;$i<count($gallry_images)-1;$i++){
                  if($i==0){
                    $active='active';
                  }else{
                    $active='';
                  }

                  echo  '<div class="carousel-item '.$active.'">
                          <img src="'.IMG.'property_gallery/'.$gallry_images[$i].'" class="img-fluid">
                        </div>';
              }?>
              </div>
            </div> 
              
           <?php }else{
                  echo  '<div class="">
                          <img src="'.IMG.'no_image.png" class="img-fluid">
                        </div>';
                }
           
           
           ?>
             </div>
             <div class="col-sm-12 col-xs-12 col-md-7 col-lg-4">
              <?php if($property_detail[0]['isvariant']!="yes"){?>
              <div class="summery-text-box">
                <h4>Quick Summary</h4>
                <hr>
                <table>
                  <tr>
                    <td>Type</td>
                    <td>:</td>
                    <td><?php if(!empty($property_detail[0]['property_type'])){echo $property_detail[0]['property_type'] ;}?></td>
                  </tr>
                  <tr>
                    <td>Area</td>
                     <td>:</td>
                    <td id="area"><?php if(!empty($property_detail[0]['diemensions'])){echo $property_detail[0]['diemensions'] ;}?> sqft</td>
                  </tr>
                  <tr>
                    <td>Bed</td>
                     <td>:</td>
                    <td><?php if(!empty($property_detail[0]['bedrooms'])){echo $property_detail[0]['bedrooms'] ;}?></td>
                  </tr>
                  <tr>
                    <td>Bath</td>
                     <td>:</td>
                    <td><?php if(!empty($property_detail[0]['washrooms'])){echo $property_detail[0]['washrooms'] ;}?></td>
                  </tr>
                  <tr>
                    <td>Park</td>
                     <td>:</td>
                    <td><?php if(!empty($property_detail[0]['park'])){echo $property_detail[0]['park']; }?></td>
                  </tr>
                  <tr>
                    <td>Overall Rating</td>
                     <td>:</td>
                    <td>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                    </td>
                  </tr>
                </table>
              </div>
            <?php }else{?>
              <div class="summery-text-box">

                <div class="nl--variant pt-4">
                      <select onchange="prop_variations(<?=$property_detail[0]['prop_id']?>)" id="variation" name="variation">
                        <option value="">Select Variation</option>
                      <?php foreach ($property_variant as $key => $value) { ?>
                        <option value="<?=$value['no_beds']?>"><?=$value['v_title']?></option>
                     <?php } ?>
                      </select>
                </div>
                <h4 id="tit">Quick Summary</h4>
                <hr>
                <table  id="tbl">
                  <tr>
                    <td>Type</td>
                    <td>:</td>
                    <td><?php if(!empty($property_detail[0]['property_type'])){echo $property_detail[0]['property_type'] ;}?></td>
                  </tr>
                  <tr>
                    <td>Area</td>
                     <td>:</td>
                    <td id="area"><?php if(!empty($property_detail[0]['diemensions'])){echo $property_detail[0]['diemensions'] ;}?> sqft</td>
                  </tr>
                  <tr>
                    <td>Bed</td>
                     <td>:</td>
                    <td id="bed"><?php if(!empty($property_detail[0]['bedrooms'])){echo $property_detail[0]['bedrooms'] ;}?></td>
                  </tr>
                  <tr>
                    <td>Bath</td>
                     <td>:</td>
                    <td id="bath"><?php if(!empty($property_detail[0]['washrooms'])){echo $property_detail[0]['washrooms'] ;}?></td>
                  </tr>
                  <tr>
                    <td id="kitchen">Park</td>
                     <td>:</td>
                    <td id="no_kit"><?php if(!empty($property_detail[0]['park'])){echo $property_detail[0]['park']; }?></td>
                  </tr>
                  <tr>
                    <td>Overall Rating</td>
                     <td>:</td>
                    <td>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
              <div class="nl--download-button">
                <a href="<?php echo SURL.'assets/images/property_broucher/'.$property_detail[0]['broucher'] ?>" target="_blank">DOWNLOAD BROUCHER</a>
              </div>
               <div class="nl--register-button">
               <a href="" data-toggle="modal" data-target="#exampleModalCenter">REGISTER YOUR INTRESTS</a>
               </div>
               <!-- Modal -->
               <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
<div class="modal-body">
  <div class="container-fluid">
 <div class="forms_outer">
      <form method="post" class="form-horizontal nl--slider-form position-relative nl--propertydetail-model" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>
        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-8">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">
            </div><div class="col-md-4  col-8">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </fieldset>
      </form>
    </div>
    </div>
  </div>
</div>
  </div>
</div>
<!-- Model ends -->
             </div>
           </div>
      </div>
    </div>
    <!-- summery portion ends here -->

    <!-- property detail portion starts here -->
    <div class="container">
      <div class="nl--property-detail">
        <div class="row">
          <?php if(!empty($property_detail[0]['prop_description'])){?>
          <div class="description col-sm-10 col-xs-10 col-md-6 col-lg-6">
            <h5>Property Description</h5>
            <hr>
            <?php
            $desc=nl2br($property_detail[0]['prop_description'],true) ;
            $desc = str_replace("<br />", "</p><p>", $desc);
            $desc = "<p style='text-align:justify'>" . $desc . "</p>";
            echo $desc;?>
          </div>
        <?php } ?>
          <div class="col-sm-10 col-xs-10 col-md-6 col-lg-6 features">
            <?php if(!empty($property_detail[0]['property_features'])){?>
            <h5>Property Features</h5>
            <hr>
            <ul>
              <?php 
              $features=explode(',', $property_detail[0]['property_features']);
              for ($i=0;$i<count($features);$i++) { 
                echo '<li class="fa">'.$features[$i].'</li>';
              }
              ?>
            </ul>
            <div class="nl--inquire-button">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">INQUIRE NOW</a>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <!-- property detail portion ends here -->

    <!-- property location portion starts here -->

    <div class="container">
      <div class="nl--location-portion">
        <div class="row">
          <?php if(!empty($property_detail[0]['location'])){?>
          <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6 location" style="padding-right: 10px; padding-left: 10px;">
            <h5>Property Location</h5>
            <hr>
            <div id="" class="map">
              <?=str_replace('xyz','iframe',$property_detail[0]['location']);?>
            </div>
          </div>
          <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6 for-plan" style="padding-right: 10px; padding-left: 10px;">
                  <h5 class="text-center text-md-left">Floor Plan</h5>
                  <hr>
          <?php } if(!empty($floor_plan)){?>
                  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <?php 
                      $gallry_images=explode(' , ',$floor_plan[0]['gal_images']);
                      
                     for($i=0;$i<count($gallry_images)-1;$i++){
                        if($i==0){
                          $active='active';
                        }else{
                          $active='';
                        }

                        echo  '<div class="carousel-item '.$active.'">
                                <img src="'.IMG.'property_floor_gallery/'.$gallry_images[$i].'" class="img-fluid">
                              </div>';
                        }?>
                    </div>
                  </div>
        <?php }else{
            echo  '<div class="">
                <img src="'.IMG.'no_image1.png" class="img-fluid" style="margin-top: 9px;">
              </div>';
        }
        ?>
          </div>
        </div>
      </div>
    </div>

    <!-- property location portion starts here -->

     <!-- property related portion starts here -->
     <?php if(!empty($related_prop)){?>
     <div class="container">
      <div class="nl--related-properties">
        <h5>Related Properties</h5>
        <hr>
        <!--Section: Group of personal cards-->
        <section class="pt-5 mt-3 pb-3">
        <!--Grid row-->
<div class="row">
        <!--Card group-->

          <!--Card-->
          <?php foreach ($related_prop as $key => $value) {

            echo '<div class="nl--card-personal mb-4 col-lg-4 col-md-4 col-sm-4 ">
                    <div class="card ">
                      <!--Card image-->
                        <img class="card-img-top" src="'.IMG.'properties/'.$value['image'].'" alt="Card image cap">
                        <a href="#!">
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      <!--Card image-->
                      <div class="location">
                        <span class="ml-1"><i class="fa fa-map-marker mr-1"></i>'.substr(ucwords($value['address']),0,25).'...</span>
                      </div>
                      <!--Card content-->
                      <div class="card-body">
                        <!--Title-->
                        <a>
                          <h4 class="card-title">'.ucwords($value['prop_title']).'</h4>
                        </a>
                        <!--Text-->
                        <p class="card-text">'.substr(ucwords($value['prop_short_description']),0,100).'...</p>
                        
                      </div>
                      <div class="card-footer bg-transparent">
                          <span><i class="fa fa-bookmark"></i>'.$value['diemensions'].'</span>
                          <span><i class="fa fa-bed"></i>'.$value['bedrooms'].' bedrooms</span>
                          <span><i class="fa fa-tint"></i>'.$value['washrooms'].' baths</span>
                      </div>
                      <!--Card content-->
                      <a href="" class="link">GET IN TOUCH</a>
                    </div>
                  </div>';

          } ?>
        <!--Card group-->
    </div>
      </section>
      <!--Section: Group of personal cards-->
    </div>
</div>
<?php } ?>
     <!-- property related portion ends here -->


     <!-- agents portion starts here -->
      <?php if(!empty($agent)){?>
      <div class="container">
        <div class="nl--agents">
        <div class="col-sm-10  col-md-10 nl--agents-section">
         <h4 class="heading">Property Agent</h4>
        <hr>
      </div>
        <div class="row">
          <div class=" col-sm-12 col-xs-12  col-md-4 col-lg-3 text-center">
            <img class="img-fluid" src="<?php echo IMG.'agents/'.$agent[0]['agent_image']?>">
          </div>
          <div class="col-sm-12 col-xs-12  col-md-8 col-lg-7 detail margin-left">
          <h4><?php if(!empty($agent[0]['name'])){echo $agent[0]['name'];}?></h4>
          <div class="social-links">
            <ul>
              <li>
                <a href="<?php if(!empty($agent[0]['facebook'])){echo $agent[0]['facebook'];}?>"><i class="fa fa-facebook"></i></a>
              </li>
              <li>
                <a href="<?php if(!empty($agent[0]['twitter'])){echo $agent[0]['twitter'];}?>"><i class="fa fa-twitter"></i></a>
              </li>
              <li>
                <a href="<?php if(!empty($agent[0]['linkdin'])){echo $agent[0]['linkdin'];}?>"><i class="fa fa-linkedin"></i></a>
              </li>
            </ul>
          </div>
          <h5 class="post"><?php if(!empty($agent[0]['position'])){echo $agent[0]['position'];}?></h5>
          <p class="px-3 px-md-0"><?php if(!empty($agent[0]['description'])){echo $agent[0]['description'];}?></p>
          <div class="nl--request-button">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">REQUEST CALL BACK!</a>
              </div>
          </div>
          
        </div>
    </div>
</div>
<?php } ?>
     <!-- agents portion ends here -->
<style>
	html, body {
    max-width: 100%;
    overflow-x: hidden;
}
@media (min-width: 768px) and (max-width: 1024px) {
	.tab-container-style
	{
		margin:0;
	}
}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">
	<!-- The slideshow -->
	<div class="carousel-inner">
		<div class="carousel-item active">
		    <?php if(!empty($sliders[0]['image'])){
			echo '<img src="'.IMG.'sliders/'.$sliders[0]['image'].'" alt="Los Angeles" width="1100" height="500">';
		    }else{
		        '<img src="'.IMG.'no_image.png'.'" alt="Los Angeles" width="1100" height="500">';
		    }?>
			<div class="caption">
				<img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
				<p class="address"><?=$sliders[0]['address']?></p>
				<h2><?=$sliders[0]['heading']?></h2>
				<p class="desc"><?=$sliders[0]['description']?></p>
				<a href="" data-toggle="modal" data-target="#exampleModalCenter" class="caption-btn"><?=$sliders[0]['button_title']?></a>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content bg-transparent border-0">
			<div class="modal-body">
				<div class="container-fluid">

					<div class="forms_outer">      

						<form method="post" class="form-horizontal nl--slider-form position-relative nl--propertydetail-model" name="myForm1" id="myForm1" enctype="multipart/form-data" >
							<div id="reslt" style="text-align: center;"></div>  

							<fieldset>
								<!-- Text input-->
								<div class="form-group row">
									<div class="col-md-6 col-6">
										<input id="f_name" name="f_name" type="text" placeholder="First Name*" class="form-control input-md">
										<div id="fname"></div>
									</div> 
									<div class="col-md-6  col-6">
										<input id="l_name" name="l_name" type="text" placeholder="Last Name*" class="form-control input-md">
										<div id="lname"></div>
									</div> 
								</div>
								<div class="form-group row">

									<div class="col-md-12  col-12">
										<input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
										<div id="mail"></div>
									</div> 
								</div>
								<div class="form-group row">

									<div class="col-md-6  col-6">
										<input id="c_code" name="c_code" type="text" placeholder="Country (+971)" class="form-control input-md">
										<div id="ccode"></div>
									</div> 
									<div class="col-md-6  col-6">
										<input id="number" name="number" type="text" placeholder="Mobile Number*" class="form-control input-md">
										<div id="phone"></div>
									</div> 
								</div>
								<div class="form-group row">

									<div class="col-md-6  col-6">
										<input id="residence" name="residence" type="text" placeholder="Residence*" class="form-control input-md">
										<div id="residc"></div>
									</div> 
									<div class="col-md-6  col-6">
										<input id="nationality" name="nationality" type="text" placeholder="Nationality*" class="form-control input-md">
										<div id="nationalty"></div>
									</div> 
								</div>
								<div class="form-group row">

									<div class="col-md-12  col-12">
										<input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
										<div id="inqury"></div>
									</div> 
								</div>
								<div class="form-group row">

									<div class="col-md-12  col-12">
										<textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
										<div id="msg"></div>
									</div> 
								</div>

								<!-- Text input-->
								<div class="form-group row">

									<div class="col-md-4  col-8">
										<input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">
									</div><div class="col-md-4  col-8">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Model ends -->

<!-- listing page section starts here -->

<div class="nl--listing-page mt-5">
	<div class="container tab-container-style" >
		<div class="row">
			<div class="col-md-4 col-12">
			    <button class="filters"><i class="fa fa-filter"></i>Filter</button>
				<div class=" search-section">
					<h3>SEARCH</h3>
					<div class="form" >
						<form id="form1" method="post" action='<?php echo SURL.'nl_listing/search_detail'?>'>
							<input type="text" name="key_words" placeholder="Enter Keywords...">
							<select name="prop_type" placeholder="Property Type ">
								<option value="">Select Property Type</option>
								<option value="Residential">Residential</option>
								<option value="Comercial">Comercial</option>
							</select>

							<select name="city" placeholder="Property City">
								<option value="">Select City</option>
								<option value="UAE">UAE</option>
								<option value="Dubai">Dubai</option>
								<option value="Sharjah">Sharjah</option>
							</select>
							<input type="text" name="location" placeholder="Select Location 3">

						</div>
						<div class="categories-portion">
							<h3>CATEGORIES</h3>
							<ul class="list-group cat-ul">
								<li class="list-group-item"><input name="sale" value="sale" type="checkbox"> For Sale<span>(5)</span></li>
								<li class="list-group-item"><input name="land_estate" value="land_estate" type="checkbox"> Land & Estates<span>(7)</span></li>
								<li class="list-group-item"><input name="rent" value="rent" type="checkbox"> To Rent<span>(12)</span></li>
								<li class="list-group-item"><input name="share" value="share" type="checkbox"> To Share<span>(8)</span></li>
							</ul>
						</div>
						<div class="saleprice-portion">
							<h3>SALE PRICE</h3>
							<div class="price-section row">
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 property-serach-input" >
									<input type="text" name="min_price" placeholder="Min Saleprice">
								</div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  col-12 property-serach-input">
									<input type="text" name="max_price" placeholder="Max Saleprice">
								</div>
							</div>
						</div>
						<div class="bedroom-portion">
							<h3>BEDROOM</h3>
							<div class="bed-section row">
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  col-12 property-serach-input" >
									<input type="text" name="min_bedroom" placeholder="Min Bedroom">
								</div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  col-12 property-serach-input" >
									<input type="text" name="max_bedroom" placeholder="Max Bedroom">
								</div>
							</div>
						</div>
						<div class="bathroom-portion">
							<h3>BATHROOM</h3>
							<div class="bath-section row">
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  col-12 property-serach-input" >
									<input type="text" name="min_bathroom" placeholder="Min Bathroom">
								</div>
								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12  col-12 property-serach-input" >
									<input type="text" name="max_bathroom" placeholder="Max Bathroom">
								</div>
							</div>
						</div>
						<div class="property-type-portion">
							<h3>PROPERTY TYPES</h3>
							<ul class="list-group type-ul">
								<li class="list-group-item"><input value="Comercial" name="comercial" type="checkbox"> Comercial</li>
								<li class="list-group-item"><input value="Residential" name="residential" type="checkbox"> Residential</li>
							</ul>
						</div>
						<div class="new-homes-portion">
							<h3>NEW HOMES</h3>
							<ul class="list-group homes-ul">
								<li class="list-group-item"><input value="New_build" name="new_build" type="checkbox"> New Build</li>
								<li class="list-group-item"><input value="Pre_owned" name="pre_owned" type="checkbox"> Pre-Owned</li>
							</ul>
						</div>
						<div class="col-md-12 m-0 p-0">
							<button class="searchbtn" type="submit" name="submit" id="submit" value="submit">Search</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-8 listing-section col-12" id="all_properties">
				<h3 class="col-12">LISTING PAGE</h3>
				<hr>
				<span id="total_prop float-left col-md-2 col-3"><?=$total_prop?> Result Listing</span>
				<div class="drpdown float-right col-md-8 col-8 text-right">
					<label>SORT BY:</label>
					<select id="duration" name="duration" onchange="sort_prop()" class="duration">
						<option value="">SELECT TO SORT</option>
						<option value="date_desc">MOST RECENT</option>
						<option value="date_asc">Old Properties</option>
						<option value="price_desc">High To Low Price</option>
						<option value="price_asc">Low To High Price</option>
					</select>
				</div>
				<?php if(!empty($records2)){ ?>


					<div id="prop" class="col mt-4 ">
						<?php 
						foreach ($records2 as $key => $value) {
							$dat=$value->created_at;
							echo  '<div class="property"><hr>
										

								<div class="media">
									<div class="col-md-5 col-lg-6 col-12"> 
										<a class="float-left " href="'.SURL.'nl_listing/property_detail/'.$value->prop_id.'">
										<img class="img-fluid img-responsive" src="'.IMG.'properties/'.$value->image.'">
										</a>
									</div>
									<div class="media-body property-detail col-md-7 col-lg-8 col-12 property-text-center">
										<a class="float-left act_a" href="'.SURL.'nl_listing/property_detail/'.$value->prop_id.'">
											<h4 class="media-heading property-name-text">'.$value->prop_title.'</h4>
											<p class="property-text-size"> <span>By </span>next level realestate<span>  |  '.date('dS,F,Y' ,strtotime($dat)).'</span></p>
											<span class="span2">AED '.$value->price.'</span><hr>
											
											<ul class="list-inline list-unstyled">
												<li><span><i class="fa fa-arrows"></i></span>'.$value->diemensions.'</li>
												<li><span><i class="fa fa-shower"></i></span>'.$value->washrooms.'</li>
												<li><span><i class="fa fa-bed"></i></span>'.$value->bedrooms.'</li>
												<li><span><i class="fa fa-building"></i>Property Type:</span>'.$value->property_type.'</li>
												<li><span class="i-spn"><i class="fa fa-home"></i>New Homes:</span>'.$value->home_type.'</li>
												<li class="property-text-size"><span><i class="fa fa-map-marker"></i></span>'.$value->address.'</li>
											</ul>
										</a>
									</div>
								</div>
												
							</div>';                
						} ?>
					</div>
					<?php 
				} ?> 
				<div class="pagination" id="pagination">
					<?php echo $links;?>
				</div>
			</div>

		</div>
	</div>
</div>



<!-- listing page section ends here -->

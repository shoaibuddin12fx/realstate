 <?php
class Property_variant extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$data['variants']=$this->Crud->join_on_tables('property_variants','properties','property_variants.prope_id=properties.prop_id','*');
		$data['content']='property_variant/property_variant';
		$this->template->admin_template($data);
	}

	function add_variant()
	{        
        $data['properties']=$this->Crud->getRows('properties','*',array("status"=>"true","isvariant"=>"yes"));
		$data['content']='property_variant/add_variant';
		$this->template->admin_template($data);
	}
    function insert_variant(){
        $table = 'property_variants';
         if($this->input->post('create')){
            extract($this->input->post());
            $created_date = date('Y-m-d H:i:s');
            
            $data = array(
                "v_title"=>$title,
                "no_beds"=>$no_beds,
                "no_baths"=>$no_baths,
                "no_kitchen"=>$no_kitchen,
                "v_price"=>$price,
                "prope_id"=>$prope_id,
                "area"=>$area,
            );
            $inserted = $this->Crud->insert($table,$data);
            if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Detail Is added successfully</h5>');
                redirect(SURL.'property_variant');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add detail please try again</h5>');
                redirect(SURL.'property_variant/add_variant');
            }
         }
    }
	function edit_variant($id){
		$table = 'property_variants';
		$id = $id;
        $where=array("id" =>$id);
		$data['variant']=$this->Crud->getRows($table,'*',$where);
        $data['properties']=$this->Crud->getRows('properties','*',array("status"=>"true","isvariant"=>"yes"));
		$data['content'] = 'property_variant/update_variant';
		$this->template->admin_template($data);
	}


	function update_variant($id){
		$table = 'property_variants';
		$id = $id;
		$where = array("id"=> $id);
		if($this->input->post('update_btn')){

			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');
			$data = array(
				"v_title"=>$title,
		 		"no_beds"=>$no_beds,
		 		"no_baths"=>$no_baths,
		 		"no_kitchen"=>$no_kitchen,
		 		"v_price"=>$price,
		 		"prope_id"=>$prope_id,
                "area"=>$area,
		 	);
		 	$updated = $this->Crud->update($table,$data,$where);
		 
		 	if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Record is updated successfully</h5>');
                redirect(SURL.'property_variant');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update record please try again</h5>');
                redirect(SURL.'property_variant/edit_variant/'.$id);
            }
		}
	}
	function del_row($id){
			$table = 'property_variants';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Team member is deleted successfully</h5>');
                redirect(SURL.'property_variant');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete team member please try again</h5>');
                redirect(SURL.'property_variant');
            }
	}
	function activate_member($id){

        $table='property_variant';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Member is activated successfully</h5>');
                redirect(SURL.'property_variant');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate Member please try again</h5>');
                redirect(SURL.'property_variant');
            }
    }
    function deactivate_member($id){
        $table='property_variant';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Member is deactivated successfully</h5>');
                redirect(SURL.'property_variant');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate Member please try again</h5>');
                redirect(SURL.'property_variant');
            }
    }
}
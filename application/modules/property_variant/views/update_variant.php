<?php 

extract($variant[0]); ?>
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Update Property Variant</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Property Variant</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'property_variant/add_variant'?>">
						<i class="fa fa-plus txt-success " title="Add Variant"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'property_variant/update_variant/'.$id?>" enctype="multipart/form-data" class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Select Property</label>
							<div class="col-sm-5">
								<select class="form-control" name="prope_id" id="prope_id">
									<?php foreach ($properties as $key => $value) {?>
					<option value="<?=$value['prop_id']?>" <?php if($prope_id==$value['prop_id']){echo "selected" ;}?>><?=$value['prop_title']?></option>
									<?php } ?>

								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?=$v_title?>" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Area</label>
							<div class="col-sm-4">
								<input type="number" value="<?=$area?>" class="form-control" name="area" id="area" />
							</div>
							<div class="col-sm-2">
								<label class="col-sm-3 control-label">Sqft</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Beds</label>
							<div class="col-sm-5">
								<input type="number" value="<?=$no_beds?>" class="form-control" name="no_beds" id="no_beds" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Baths</label>
							<div class="col-sm-5">
								<input type="text" value="<?=$no_baths?>" class="form-control" name="no_baths" id="no_baths" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Kitchen</label>
							<div class="col-sm-5">
								<input type="number" value="<?=$no_kitchen?>" class="form-control" name="no_kitchen" id="no_kitchen" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Price</label>
							<div class="col-sm-5">
								<input type="number" value="<?=$v_price?>" class="form-control" name="price" id="price" required/>
							</div>
						</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" id="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>our_team" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

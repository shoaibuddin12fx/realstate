<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Admin Login</title>
		<meta name="description" content="description">
		<meta name="author" content="Evgeniya">
		<meta name="keyword" content="keywords">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?=APLUGINS?>bootstrap/bootstrap.css" rel="stylesheet">
<link href="<?=ACSS?>style.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			<!--<div class="text-right">
				<a href="page_register.html" class="txt-default">Need an account?</a>
			</div>-->
			<div class="box">
				<div class="box-content">
					<div class="text-center">
						<?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message'); } ?>
						<h3 class="page-header">Admin Login</h3>
					</div>
					<form method="post" action="<?php echo SURL.'admin/login' ?>" >
					<div class="form-group">
						
						<label class="control-label">Username</label>

						<input type="text" class="form-control" id="identity" name="identity" />
					</div>
					<div class="form-group">
						<label class="control-label">Password</label>
						<input type="password" class="form-control" id="password" name="password" />
					</div>
					<div class="text-center">
						<button class="btn btn-primary" type="submit" id="login" name="login" value="login" >Sign in</button>
					</div>
				</form>
				<center><a class="btn btn-primary" type="submit" id="forgot_pass" name="forgot_pass" href="<?php echo SURL.'admin/forgot_pass' ?>" >Forgot Password</a></center>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

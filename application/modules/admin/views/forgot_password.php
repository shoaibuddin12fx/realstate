<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Admin Login</title>
		<meta name="description" content="description">
		<meta name="author" content="Evgeniya">
		<meta name="keyword" content="keywords">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?=PLUGINS?>bootstrap/bootstrap.css" rel="stylesheet">
<link href="<?=CSS?>style.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			<!--<div class="text-right">
				<a href="page_register.html" class="txt-default">Need an account?</a>
			</div>-->
			<div class="box">
				<div class="box-content">
						<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

                        <div id="infoMessage"><?php echo $message;?></div>

                        <?php echo form_open("admin/forgot_pass");?>

                         <p>
           	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	           <?php echo form_input($identity);?>
                         </p>

                       <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

                        <?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>

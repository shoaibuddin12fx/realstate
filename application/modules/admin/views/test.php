<li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Blogs Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'blogs/add_blog' ;?>">Add Blog</a></li>
                        <li><a class="" href="<?php echo SURL.'blogs' ;?>">All Blogs</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Applications Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'applications' ;?>">All Applications</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Qoute Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'qoute' ;?>">All Qoutes</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Other Office Addresses</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'office_address' ;?>">All Addresses</a></li>
                        <li><a class="" href="<?php echo SURL.'office_address/add_address' ;?>">Add Address</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Blog Categories</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'blog_category/add_category' ;?>">Add Blog Category</a></li>
                        <li><a class="" href="<?php echo SURL.'blog_category' ;?>">All Categories</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Slider Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'slider/add_slider' ;?>">Add Slider</a></li>
                        <li><a class="" href="<?php echo SURL.'slider' ;?>">All Sliders</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">About Us</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'about_us' ;?>">About us page</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Users Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'users_management/create_user' ;?>">Add User</a></li>
                        <li><a class="" href="<?php echo SURL.'users_management' ;?>">All Users</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Our Team</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'our_team/add_member' ;?>">Add Team Member</a></li>
                        <li><a class="" href="<?php echo SURL.'our_team' ;?>">All Team Members</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Careers</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'careers/add_career' ;?>">Add Career</a></li>
                        <li><a class="" href="<?php echo SURL.'careers' ;?>">All Careers</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Our Services</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'our_services/add_service' ;?>">Add Service</a></li>
                        <li><a class="" href="<?php echo SURL.'our_services' ;?>">All Services</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">News Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'news_management/add_news' ;?>">Add News</a></li>
                        <li><a class="" href="<?php echo SURL.'news_management' ;?>">News Management</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Price Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'pricing/add_price' ;?>">Add Price</a></li>
                        <li><a class="" href="<?php echo SURL.'pricing' ;?>">Price Management</a></li>
                        
                    </ul>
                </li>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Our Work</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'our_work/add_work' ;?>">Add Work</a></li>
                        <li><a class="" href="<?php echo SURL.'our_work' ;?>">Our Works</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Work Categories</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'work_category/add_category' ;?>">Add Work category</a></li>
                        <li><a class="" href="<?php echo SURL.'work_category' ;?>">Work categories</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">CMS Pages</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'cms_pages/add_cms' ;?>">Add CMS</a></li>
                        <li><a class="" href="<?php echo SURL.'cms_pages' ;?>">CMS Pages</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Subscribers</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'customers' ;?>">Subscribers</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Testimonials</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'testimonials/add_testimonial' ;?>">Add Testimonial</a></li>
                        <li><a class="" href="<?php echo SURL.'testimonials' ;?>">Testimonial Management</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Role Management</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'roles/new_role' ;?>">Add Role</a></li>
                        <li><a class="" href="<?php echo SURL.'roles' ;?>">All Roles</a></li>
                        
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle">
                        <i class="fa fa-table"></i>
                         <span class="hidden-xs">Site Settings</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="" href="<?php echo SURL.'system_settings' ;?>">Add Settings</a></li>
                    </ul>
                </li>
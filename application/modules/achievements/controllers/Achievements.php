<?php

class Achievements extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}
	function index(){
		$data['achievements']=$this->Crud->getRows('achievements');
		$data['content']='achievements/all_achievements';
		$this->template->admin_template($data);
	}
	function add_achievements(){
		$data['content']='achievements/add_achievement';
		$this->template->admin_template($data);
	}
	function insert_achievement(){
		$table = 'achievements';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels </h5>');
   redirect(SURL.'achievements/add_achievements');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/achievements/';
                        $projects_folder_path_main = './assets/images/achievements/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'achievements/add_achievements');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

			$data = array(
				"title" => $title,
				"image" => $filename_f,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Achievements is added successfully</h5>');
                redirect(SURL.'achievements');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add achievement please try again</h5>');
                redirect(SURL.'achievements/add_achievements');
            }
		}
	}
	function edit_achievement($id){
		$table = 'achievements';
		$id = $id;
		$where = array('id' => $id );
		$data['achievement'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'achievements/edit_achievement';
		$this->template->admin_template($data);
	}
	function update_achievement($id){
		$table = 'achievements';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels ');
   redirect(SURL.'achievements/edit_achievement/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/achievements/';
                        $projects_folder_path_main = './assets/images/achievements/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'achievements/edit_achievement/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
				}

			$data = array(
				"title" => $title,
				"image" => $filename_f,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Achievement is updated successfully</h5>');
                redirect(SURL.'achievements');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update achievement please try again</h5>');
                redirect(SURL.'achievements/edit_achievement/'.$id);
            }
		}
	function del_achievement($id){
			$table = 'achievements';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Achievement is delted successfully</h5>');
                redirect(SURL.'achievements');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete Achievement please try again</h5>');
                redirect(SURL.'achievements');
            }
	}
	function activate_achievement($id){

        $table='achievements';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Achievement is activated successfully</h5>');
                redirect(SURL.'achievements');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate achievement please try again</h5>');
                redirect(SURL.'achievements');
            }
    }
    function deactivate_achievement($id){
        $table='achievements';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Achievement is deactivated successfully</h5>');
                redirect(SURL.'achievements');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate achievement please try again</h5>');
                redirect(SURL.'achievements');
            }
    }
}

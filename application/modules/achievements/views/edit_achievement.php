 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Achievement</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Achievement</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'achievements/add_achievements'?>">
						<i class="fa fa-plus txt-success" title="Add Achievement"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($achievement[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'achievements/update_achievement/'.$id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $title ?>" class="form-control" name="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';}?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';}?> >Deactivate</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-3">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/achievements/".$image." width='100'>";}?>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>achievements" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
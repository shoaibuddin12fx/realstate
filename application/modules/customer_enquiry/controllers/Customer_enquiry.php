<?php
class Customer_enquiry extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['requests']=$this->Crud->getRows('customer_enquiry','*','','id','desc');
		$data['content']='customer_enquiry/all_requests';
                $this->template->admin_template($data);
	}
	function detail($id){
		$data['detail']=$this->Crud->getRows('customer_enquiry','*',array("id"=>$id));
		$data['content']='customer_enquiry/query_detail';
                $this->template->admin_template($data);
	}
	function del_row($id){
			$table = 'customer_enquiry';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
		if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Query is deleted successfully</h5>');
                redirect(SURL.'customer_enquiry');
                }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete Query please try again</h5>');
                redirect(SURL.'customer_enquiry');
            }
	}
}
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Customers Enquiry Requests</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span><b>Customers Enquiry Requests</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Country Code</th>
							<th>Phone</th>
							<th>Residence</th>
							<th>Nationality</th>
							<th>Enquiry About</th>
							<th>Message</th>
							<th>Request Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($requests as $key => $value) {?>
						<tr>
							
							<td><?php echo $value['first_name'].' '.$value['last_name'] ;?></td>
							<td><?php echo $value['email'] ; ?></td>
							<td><?php echo $value['country_code'] ; ?></td>
							<td><?php echo $value['phone'] ; ?></td>
							<td><?php echo $value['residence'] ; ?></td>
							<td><?php echo $value['nationality'] ; ?></td>
							<td><?php echo $value['enquiry'] ; ?></td>
							<td><div class="form-group">
									<div class="col-sm-9 col-sm-offset-3">
										<a role="button" href="<?php echo SURL.'customer_enquiry/detail/'.$value['id'] ;?>" class="btn btn-primary">View Message</a>
									</div>
								</div></td>
							<td><?php echo $value['request_date'] ; ?></td>
							<td>

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='customer_enquiry/del_row'){?>

								<a href="<?php echo SURL.'customer_enquiry/del_row/'.$value['id']?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
							</td>
							
						</tr>
					<?php } ?>
					<!-- End: list_row -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crud extends CI_Model{
    /*
     * Get posts
     */
    function getRows($table,$field='*',$condition = array()){
        $this->db->select($field);
        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->row_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }
    
    /*
     * Insert post
     */
    public function insert($table,$data = array()) {
        $insert = $this->db->insert($table, $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    /*
     * Update post
     */
    public function update($table,$data, $condition=array()) {
        if(!empty($data) && !empty($condition)){
            $update = $this->db->update($table, $data, $condition);
            return $update?true:false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete post
     */
    public function delete($table,$condition= array()){
        $delete = $this->db->delete($table,$condition);
        return $delete?true:false;
    }
}
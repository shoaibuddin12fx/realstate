 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Add Gallery For Property Floor Plan</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Gallery</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'property_floor_gallery/insert_gallery'?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Builders</label>
							<div class="col-sm-5">
								<select id="category_id" name="category_id" class="form-control">
									<option value="">--Select Builder--</option>
									<?php foreach($categories as $key => $value){
									?>
									<option value='<?=$value["cat_id"]?>'><?=$value['cat_title']?></option>
									<?php } ?>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select id="project_id" name="project_id" class="form-control">
									<option value="">--Select Category--</option>
									</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Properties</label>
							<div class="col-sm-5">
								<select id="property_id" name="property_id" class="form-control">
									<option value="">--Select Property--</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Gallery Images</label>
							<div class="col-sm-5">
								<input type="file" multiple="multiple" name="gallery[]" id="gallery" />
							</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Add Gallery</button>
							<a role="button" href="<?=SURL?>property_floor_gallery" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

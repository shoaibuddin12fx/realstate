<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?PHP echo AURL;?>">Dashboard</a></li>
			<li><a>System Settings</a></li>
			
		</ol>
	</div>
</div>
<div class="row">

	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>System Settings</b></span>
				</div>
				<div class="box-icons pull-right">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="pill" href="#home">General</a></li>
						<li><a data-toggle="pill" href="#menu1">Social</a></li>
						<li><a data-toggle="pill" href="#menu2">Email</a></li>
						<li><a data-toggle="pill" href="#menu3">Google</a></li>
					</ul>
<form id="defaultForm" method="post" action="<?php echo SURL."system_settings/add_settings/" ?>" class="form-horizontal bootstrap-validator-form" enctype="multipart/form-data" novalidate="novalidate">	
					<div class="tab-content">
					<?php 
					if(!empty($list)) extract($list[0]) ?>
						<div id="home" class="tab-pane fade in active">
							<h3>General Settings</h3>

                              <div class="col-sm-8" >
								<div class="form-group">
									<label class="col-sm-4 control-label">Site Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $site_name ?>" name="site_name" id="site_name" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Contact Number</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $contact_number ?>" name="contact" id="contact" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Footer Description</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $footer_description ?>" name="f_description" id="f_description" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">country</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $country ?>" name="country" id="country" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">City</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $city ?>" name="city" id="city" />
									</div>
								</div>
					<div class="form-group">
							<label class="col-sm-4 control-label">Street</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo $street ;?>" name="street" id="street" />
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-4 control-label">Open Time</label>
							<div class="col-sm-8">
								<input type="time" class="form-control" value="<?php echo $open_timing ;?>" name="open_time" id="open_time" />
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-4 control-label">Close Time</label>
							<div class="col-sm-8">
								<input type="time" class="form-control" value="<?php echo $close_timing ;?>" name="close_time" id="close_time" />
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-4 control-label">Working Days</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="Mon-Fri" value="<?php echo $work_day ;?>" name="work_day" id="work_day" />
							</div>
					</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Copyrights</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="<?php echo $copy_rights ?>" name="copy_rights" id="copy_rights" />
									</div>
								</div>
                                </div>
							</div>

							<div id="menu1" class="tab-pane fade">
								<h3>Social Settings</h3>

								<div class="form-group">
									<label class="col-sm-3 control-label">Facebook</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" value="<?php echo $facebook ?>" name="fb" id="fb" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Gplus</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="gplus" id="gplus" value="<?php echo $gplus ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Twitter</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="twiter" id="twiter" value="<?php echo $twiter ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Instagram</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="insta" id="insta" value="<?php echo $insta ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Youtube</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="pin" id="pin" value="<?php echo $pin ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">LinkedIn</label>
									<div class="col-sm-5">
									<input type="text" class="form-control" name="linkdin" id="linkdin" value="<?php echo $linkedin ?>"/>
									</div>
								</div>



							</div>

							<div id="menu2" class="tab-pane fade">
								<h3>Emailing Settings</h3>


								<div class="form-group">
									<label class="col-sm-3 control-label">Contact Page Email</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="page_email" id="page_email" value="<?php echo $contact_page_email ?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email Host</label>
									<div class="col-sm-5">
										<input type="text" value="<?php echo $email_host ?>" class="form-control" name="email_host" id="email_host" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Site Email</label>
									<div class="col-sm-5">
										<input type="text" value="<?php echo $news_letter_email ?>" class="form-control" name="news_email" id="news_email" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email Password</label>
									<div class="col-sm-5">
										<input type="password" class="form-control" name="email_pass" id="email_pass" value="<?php echo $email_password ?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email Protocol</label>
									<div class="col-sm-5">
										<input type="text" value="<?php echo $email_protocol ?>" class="form-control" name="email_pro" id="email_pro" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email Port</label>
									<div class="col-sm-5">
										<input type="text" value="<?php echo $email_port ?>" class="form-control" name="email_port" id="email_port" />
									</div>
								</div>
							</div>
							<div id="menu3" class="tab-pane fade">
								<h3>Google Settings</h3>

			                        <div class="form-group">
										<label class="col-sm-3 control-label">Google Map</label>
										<div class="col-sm-5">
											<textarea type="text" class="form-control" onChange="replace_iframe('map');" name="map" id="map" ></textarea>
										</div>
									</div>
							</div>
						</div>
						<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" class="btn btn-primary" id="add_settings" name="add_settings" value="add_settings">Save</button>
						</div>
					</div>
						
					
					</div></form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
	// Load TimePicker plugin and callback all time and date pickers
	
	// Create jQuery-UI tabs
	$("#tabs").tabs();
	// Sortable for elements
	
	
});
</script>

<script type="text/javascript">

function replace_iframe(id) {
text_area_data =$("#"+id).val();
res=text_area_data.replace('iframe ','xyz ');

res=res.replace('</iframe>','</xyz>');
$("#"+id).val(res);
}
$(document).ready(function() {
	$('.form-control').tooltip();
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	 	
});

</script>


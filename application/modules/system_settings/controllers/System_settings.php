<?php
class System_settings extends MY_Controller
{
	function __construct(){
		parent::__construct();

	}

	function index(){
        
        $data['list']=$this->Crud->getRows('site_settings','*','','id','DESC');
        $data['content']='system_settings/settings';
		$this->template->admin_template($data);
	}
	function add_settings()
	{
		$table='site_settings';
          

        if($this->input->post('add_settings'))  
        {        
          extract($this->input->post());

        $data=array("site_name" =>$site_name,"email_port" =>$email_port,"contact_number" =>$contact,"footer_description" =>$f_description,"copy_rights" =>$copy_rights,"facebook" =>$fb,"gplus" =>$gplus,"twiter" =>$twiter,"insta" =>$insta,"pin" =>$pin,"linkedin" =>$linkdin,"contact_page_email" =>$page_email,"email_host" =>$email_host,"news_letter_email" =>$news_email,"email_password" =>$email_pass,"email_protocol" =>$email_pro,"google_map"=>$map,"country" => $country,
			"city" => $city,"street" => $street,"open_timing" => $open_time,"close_timing" => $close_time,"work_day" => $work_day,);

			$qu=$this->Crud->insert($table,$data);

		  }
		  if($qu){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Settings saved successfully</h5>');
                redirect(SURL.'system_settings');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed save settings please try again</h5>');
                redirect(SURL.'system_settings');
            }

	   }

}
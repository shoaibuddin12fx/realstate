<?php

Class About_us extends MY_Controller {

		function __construct(){
			parent::__construct();
		}
		function index(){

            $data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"about"),'id','desc','','');  
			$table='about_us';
			$data['page_detail']=$this->Crud->getR($table,'*','','id','desc','','1');       
			$this->template->about_us_layout($data);

		}
		  function subscribers()
	{
        $eml=$this->input->post('emailaddress');
        if($eml==''){

          echo ' <div class="alert  alert-dismissable nesr-00"><center><b>Please enter a email address</b></center></div>';
            exit();
        }

        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $this->input->post('emailaddress'))) {
           echo ' <div class="alert  alert-dismissable nesr-00"><center><b>Invalid Email ! Please enter a valid Email address</b></center></div>';
        
        }  else {
            
            //print_r($this->input->post('email'));exit;
            $email =$this->Crud->checkSubscribeEmail($this->input->post('emailaddress'));
            
            if (count($email) > 0) {
             
                echo ' <div class="alert  alert-warning nesr-00"><center><b>You Have Already Subscribed To The Newsletters !</b></center></div>';
            
             } else {
                    
                    $created_date = date('Y-m-d G:i:s');
                    $created_by_ip= $_SERVER['REMOTE_ADDR'];

                    $data=array(
                    'email'=>$this->db->escape_str(trim($this->input->post('emailaddress'))),
                    'del_status'=> false,
                    'created_at'=>$this->db->escape_str(trim($created_date)),
                    'created_ip'=>$this->db->escape_str(trim($created_by_ip)),
                    );

            $addSubscribeEmail = $this->Crud->insert("subscribers", $data);
         
            if ($addSubscribeEmail) {
            
                echo ' <div class="alert alert-success nesr-00"><center><b>Subscription Successfull</b></center></div>';
                
                }else{
                    
                echo ' <div class="alert  alert-warning nesr-00"><center><b>Error in Subscribing The Newsletter. Please try again.</b></center></div>';
                }
            }
        }

	}
}
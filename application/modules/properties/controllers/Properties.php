<?php

class Properties extends MY_Controller
{
	function __construct(){
		parent::__construct();
        $this->load->helper('download');
	}
	function index(){
		$data['properties']=$this->Crud->join_on_tbl('properties','our_projects','properties.project_id=our_projects.id','*,properties.image as prop_image','project_categories','our_projects.category_id=project_categories.cat_id');
        $data['agents']=$this->Crud->getRows('agents','*',array("status"=>"true"));
		$data['content']='properties/all_properties';
		$this->template->admin_template($data);
	}
	function add_property(){
        $data['categories']=$this->Crud->getRows("project_categories");
        $data['agents']=$this->Crud->getRows('agents','*',array("status"=>"true"));
		$data['content']='properties/add_property';
		$this->template->admin_template($data);
	}
    function projects($category_id){
        $data=$this->Crud->getRows("our_projects","*",array("category_id"=>$category_id));
        echo json_encode($data);
    }
	function insert_property(){
		$table = 'properties';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels </h5>');
   redirect(SURL.'properties/add_properties');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/properties/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload the image</h5>');
   redirect(SURL.'properties/add_properties');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

                $filename_t='';
list($width, $height) = getimagesize($_FILES["thumb_img"]['tmp_name']);

if($width > "88" || $height > "88") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions For Thumbnail Must Be Or Less Then 88 * 88 pixels </h5>');
   redirect(SURL.'properties/add_properties');
}else{    
                          
                    if ($_FILES['thumb_img']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/properties/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('thumb_img')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload the image</h5>');
   redirect(SURL.'properties/add_properties');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_t= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_t, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
                $broucher='';
                    if ($_FILES['broucher']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/property_broucher/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('broucher')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload the image of pdf for broucher</h5>');
   redirect(SURL.'properties/add_properties');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $broucher= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($broucher, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                    $filename_sli='';

                 
list($width, $height) = getimagesize($_FILES["slider_image"]['tmp_name']);

if($width > "2000" || $height > "1200" || $width < "500" || $height < "500") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Less Then 2000 * 1200 pixels ');
   redirect(SURL.'properties/add_property/');
}else{    
                          
                    if ($_FILES['slider_image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/properties/';
                        $projects_folder_path_main = './assets/images/properties/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('slider_image')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload the image for slider</h5>');
   redirect(SURL.'properties/add_properties');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_sli= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_sli, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
                $property_features=implode(',', $features);
			$data = array(
				"prop_title" => $title,
                "category_id" => $category_id,
                "project_id" => $project_id,
                "diemensions" => $diemensions,
                "bedrooms" => $bedrooms,
                "washrooms" => $washrooms,
                "park" => $park,
                "price" => $price,
                "property_features" => $property_features,
                "property_type" => $property_type,
                "home_type" => $home_type,
                "address" => $address,
                "city" => $city,
                "location"=>$location,
                "isfeature"=>$isfeature,
                "distance" => $distance,
				"image" => $filename_f,
                "broucher" => $broucher,
                "thumb_image" => $filename_t,
                "prop_description" => $description,
                "prop_short_description" => $short_description,
				"created_at" => $created_date,
				"status" => $status,
                "prop_baner_heading" => $heading,
                "prop_baner_address" => $banner_address,
                "prop_baner_desc" => $s_description,
                "prop_baner_img" => $filename_sli,
                "isvariant" => $isvariant,
                "agent" => $agent,    
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">properties is added successfully</h5>');
                redirect(SURL.'properties');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add property please try again</h5>');
                redirect(SURL.'properties');
            }
		}
	}
	function edit_property($id){
		$table = 'properties';
		$id = $id;
		$where = array('prop_id' => $id );
        $data['categories'] = $this->Crud->getRows('project_categories','*');
		$data['property'] = $this->Crud->getRows($table,'*',$where);
        $pro_id=$data['property'][0]['project_id'];
        $data['proj']=$this->Crud->getRows('our_projects','*',array('id'=>$pro_id));
        $data['proj_title']=$data['proj'][0]['title'];
        $data['proj_id']=$data['proj'][0]['id'];
        $data['agents']=$this->Crud->getRows('agents','*',array("status"=>"true"));
		$data['content'] = 'properties/edit_property';
		$this->template->admin_template($data);
	}
	function update_property($id){
		$table = 'properties';
		$id = $id;
		$where = array('prop_id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels ');
   redirect(SURL.'properties/edit_property/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $properties_folder_path = './assets/images/properties/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload the image');
   redirect(SURL.'properties/edit_property/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
				}

                $filename_t = $update_thumb ;


list($width, $height) = getimagesize($_FILES["thumb_img"]['tmp_name']);

if($width > "88" || $height > "88") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions For Thumbnail Must Be Or Less Then 88 * 88 pixels ');
   redirect(SURL.'properties/edit_property/'.$id);
}else{    

            if($_FILES['thumb_img']['name'] != ""){

                        $properties_folder_path = './assets/images/properties/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('thumb_img')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload the right thumb nail image');
   redirect(SURL.'properties/edit_property/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_t= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_t, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

                    $broucher=$up_broucher;
                    if($_FILES['broucher']['name'] != ""){

                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('broucher')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload the image or pdf file for broucher');
   redirect(SURL.'properties/edit_property/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $broucher= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($broucher, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                $filename_sli = $up_slide ;
    if($_FILES['slider_image']['name'] != ""){        
    list($width, $height) = getimagesize($_FILES["slider_image"]['tmp_name']);

if($width > "2000" || $height > "1200" || $width < "500" || $height < "500") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Less Then 2000 * 1200 pixels ');
   redirect(SURL.'project_categories/edit_category/'.$id);
}else{    

            if($_FILES['slider_image']['name'] != ""){

                        $projects_folder_path = './assets/images/properties/';
                        $projects_folder_path_main = './assets/images/properties/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = $width;
                        $config_resize['height'] = $height;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('slider_image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload the image for slider');
   redirect(SURL.'properties/edit_property/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_sli= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_sli, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
            }

                $property_features=implode(',', $features);
			$data = array(
                "prop_title" => $title,
                "category_id" => $category_id,
                "project_id" => $project_id,
                "diemensions" => $diemensions,
                "bedrooms" => $bedrooms,
                "washrooms" => $washrooms,
                "park" => $park,
                "price" => $price,
                "property_features" => $property_features,
                "property_type" => $property_type,
                "home_type" => $home_type,
                "address" => $address,
                "city" => $city,
                "location"=>$location,
                "isfeature"=>$isfeature,
                "image" => $filename_f,
                "broucher" => $broucher,
                "thumb_image" => $filename_t,
                "prop_description" => $description,
                "prop_short_description" => $short_description,
                "updated_at" => $updated_at,
                "status" => $status,
                "prop_baner_heading" => $heading,
                "prop_baner_address" => $banner_address,
                "prop_baner_desc" => $s_description,
                "prop_baner_img" => $filename_sli,
                "isvariant" => $isvariant,
                "agent" => $agent,  
                
            );
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">property is updated successfully</h5>');
                redirect(SURL.'properties');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update property please try again</h5>');
                redirect(SURL.'properties/edit_property/'.$id);
            }
		}
	function del_property($id){
			$table = 'properties';
			$id= $id;
			$where = array("prop_id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">property is delted successfully</h5>');
                redirect(SURL.'properties');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete property please try again</h5>');
                redirect(SURL.'properties');
            }
	}
	function activate_property($id){

        $table='properties';
        $id=$id;
        $data=array("status" => "true");
        $where=array("prop_id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">property is activated successfully</h5>');
                redirect(SURL.'properties');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate property please try again</h5>');
                redirect(SURL.'properties');
            }
    }
    function deactivate_property($id){
        $table='properties';
        $id=$id;
        $where=array("prop_id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">property is deactivated successfully</h5>');
                redirect(SURL.'properties');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate property please try again</h5>');
                redirect(SURL.'properties');
            }
    }
}

 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Property Management</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Property Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'properties/add_property'?>">
						<i class="fa fa-plus txt-success" title="Add Property"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							
							<th>Image</th>
							<th>Thumbnail</th>
							<th>Broucher</th>
							<th>Titles</th>
							<th>Builder</th>
							<th>Address</th>
							<th>Features</th>
							<th>Facilities</th>
							<th>Total Price</th>
							<th>Property Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($properties as $key => $value) {
					extract($value);

					?>
						<tr>
							
							<td> <?php if(!empty($prop_image)){ ?><img src="<?php echo SURL.'assets/images/properties/'.$prop_image ?>">  <?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?> </td>
							<td> <?php if(!empty($thumb_image)){ ?><img src="<?php echo SURL.'assets/images/properties/'.$thumb_image ?>">  <?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?> </td>
							<td><a role="button" href="<?php echo SURL.'assets/images/property_broucher/'.$broucher?>" target="_blank" class="btn btn-primary">broucher</a></td>
							<td>Project:<?php echo $title ?>
							Property:<?php echo $prop_title ?>
							</td>
							<td><?php echo $cat_title ?></td>
							<td>Address:<?php echo $address?>
								City:<?php echo $city?>
							</td>
							<td>Diementions:<?php echo $diemensions?>
								Washrooms:<?php echo $washrooms?>
								Bedrooms:<?php echo $bedrooms?>
								Pakrs:<?php echo $park?>
								Distance:<?php echo $distance?>
							</td>
							<td>
				            <ul>
				              <?php 
				              $features=explode(',', $property_features);
				              for ($i=0;$i<count($features);$i++) { 
				                echo '<li class="fa">'.$features[$i].'</li>';
				              }
				              ?>
				            </ul></td>
							<td><?php echo $price?></td>
							<td>Property Type:<?php echo $property_type?>
								Home Type:<?php echo $home_type?>
								For:<?php echo $prop_for?><br>
								Agent:<?php 
								if(!empty($agents)){
								foreach ($agents as $key => $va) {
									if($va['id']==$agent){
										echo $va['name'];
									}
								}
							}?>
							</td>
							<td>
									
<!--                           <?php if($status == "true"){?>
                                <a href="<?php echo SURL.'properties/deactivate_property/'.$prop_id?>" title="Deactivate"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            <?php }else{?>
                                <a href="<?php echo SURL.'properties/activate_property/'.$prop_id?>" title="Activate"><i class="fa fa-check-circle" aria-hidden="true"></i></a> 
                            <?php }?>-->

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='properties/edit_property'){

								echo'<a href="'.SURL.'properties/edit_property/'.$prop_id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='properties/del_property'){?>

								<a href="<?php echo SURL.'properties/del_property/'.$prop_id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
									
							</td>
						</tr>
					<?php } ?>	
					<!-- End: list_row -->
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

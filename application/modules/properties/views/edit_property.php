 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Property</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Property</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'properties/add_property'?>">
						<i class="fa fa-plus txt-success" title="Add Property"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($property[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'properties/update_property/'.$prop_id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?=$prop_title?>" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Diemensions</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" value="<?=$diemensions?>" name="diemensions" id="diemensions" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Bedrooms</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" value="<?=$bedrooms?>" name="bedrooms" id="bedrooms" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?=$address?>" name="address" id="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">City</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?=$city?>" name="city" id="city" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Location</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('location')" class="form-control" name="location" id="location" ><?=$location?></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Distance</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?=$distance?>" name="distance" id="distance" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Washrooms</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" value="<?=$washrooms?>" name="washrooms" id="washrooms" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Parks</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" value="<?=$park?>" name="park" id="park" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Price</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" value="<?=$price?>" name="price" id="price" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Property Features</label>
							<?php $features=explode(',', $property_features); 

							?>
							<div class="col-sm-9">

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Garage', $features)){echo 'checked';} ?> value="Garage" name="features[]" id="features" />
									<label class="control-label">Garage</label>

								</div>
								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Parquet', $features)){echo 'checked';} ?>  value="Parquet" name="features[]" id="features" />
									<label class="control-label">Parquet</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Air Conditioning', $features)){echo 'checked';} ?>  value="Air Conditioning" name="features[]" id="features" />
									<label class="control-label">Air Conditioning</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Internet', $features)){echo 'checked';} ?>  value="Internet" name="features[]" id="features" />
									<label class="control-label">Internet</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Terrace', $features)){echo 'checked';} ?>  value="Terrace" name="features[]" id="features" />
									<label class="control-label">Terrace</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Roof Rerrace', $features)){echo 'checked';} ?>  value="Roof Rerrace" name="features[]" id="features" />
									<label class="control-label">Roof Terrace</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Car Park', $features)){echo 'checked';} ?>  value="Car Park" name="features[]" id="features" />
									<label class="control-label">Car Park</label>

								</div>
								<div class="col-sm-3">
									<input type="checkbox" <?php if(in_array('Car Garrage', $features)){echo 'checked';} ?>  value="Car Garrage" name="features[]" id="features" />
								<label class="control-label">Car Garrage</label>

								</div>
							</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Property For Rent/Sale</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="prop_for" id="prop_for">
									<option value="">-- Select--</option>
									<option value="Rent" <?php if($prop_for == 'Rent'){echo 'selected';}?> >Rent</option>
									<option value="Sale" <?php if($prop_for == 'Sale'){echo 'selected';}?> >Sale</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Property Type</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="property_type" id="property_type">
									<option value="">-- Select a property --</option>
									<option value="Comercial" <?php if($property_type == 'Comercial'){echo 'selected';}?> >Comercial</option>
									<option value="Residential" <?php if($property_type == 'Residential'){echo 'selected';}?> >Residential</option>
									<option value="land_estate" <?php if($property_type == 'land_estate'){echo 'selected';}?> >Land Estate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Home Type</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="home_type" id="home_type">
									<option value="">-- Select a home --</option>
									<option value="New_build" <?php if($home_type == 'New_build'){echo 'selected';}?> >New Build</option>
									<option value="Pre_owned" <?php if($home_type == 'Pre_owned'){echo 'selected';}?> >Pre-Owned</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Select Agent</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="agent" id="agent">
									<option value="">--Select Agent--</option>
									<?php foreach ($agents as $key => $va) {
									?>
									<option value="<?=$va['id']?>" <?php if($va['id'] == $agent){echo 'selected';}?> ><?=$va['name']?></option>
								<?php } ?>
								</select>
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Short Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="4" name="short_description" id="short_description" ><?=$prop_short_description?></textarea>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="8" name="description" id="description" ><?=$prop_description?></textarea>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Project Category</label>
							<div class="col-sm-5">
								<select id="category_id" name="category_id" class="form-control">
									<option>--Select Category--</option>
									<?php foreach($categories as $key => $value){
									?>
									<option value='<?=$value["cat_id"]?>' <?php if($value["cat_id"]==$category_id){echo "selected" ;} ?> ><?=$value['cat_title']?></option>
									<?php } ?>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select id="project_id" name="project_id" class="form-control">
									<option value="<?php if(!empty($proj_id)){echo $proj_id ;}?>"><?php if(!empty($proj_title)){echo $proj_title ;}?></option>
									
								</select>
							</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';}?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';}?> >Deactivate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Is Feature</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="isfeature" id="isfeature">
									<option value="">-- Select a status --</option>
									<option value="yes" <?php if($isfeature == 'yes'){echo 'selected';}?> >Yes</option>
									<option value="no" <?php if($isfeature == 'no'){echo 'selected';}?> >No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Is Variant</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="isvariant" id="isvariant">
									<option value="">-- Select--</option>
									<option value="yes" <?php if($isvariant == 'yes'){echo 'selected';}?> >Yes</option>
									<option value="no" <?php if($isvariant == 'no'){echo 'selected';}?> >No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Heading</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $prop_baner_heading ?>" class="form-control" name="heading" id="heading" />
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $prop_baner_address ?>" class="form-control" name="banner_address" id="banner_address" />
							</div>
						</div>
						<div class="form-group">
					<label class="col-sm-3 control-label">Banner Description</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $prop_baner_desc ?>" class="form-control" name="s_description" id="s_description" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Banner Image</label>
							<div class="col-sm-3">
								<input type="file"  name="slider_image" id="slider_image" />
								<input type="hidden" id="up_slider" value="<?php echo $prop_baner_img ?>"  name="up_slider">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($prop_baner_img)){ ?><img src="<?php echo SURL.'assets/images/properties/'.$prop_baner_img ?>" width='100%'><?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?>
								
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Property Broucher</label>
							<div class="col-sm-3">
								<input type="file"  name="broucher" id="broucher" />
								<input type="hidden" id="up_broucher" value="<?php echo $broucher ?>"  name="up_broucher">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-3">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/properties/".$image." width='100'>";}?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Thumbnail Image</label>
							<div class="col-sm-3">
								<input type="file"  name="thumb_img" id="thumb_img" />
								<input type="hidden" id="update_thumb" value="<?php echo $thumb_image ?>"  name="update_thumb">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($thumb_image)){echo "<img src=".SURL."assets/images/properties/".$thumb_image." width='100'>";}?>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>properties" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
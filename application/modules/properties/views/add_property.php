 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Add Property</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Property</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'properties/insert_property'?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Project Category</label>
							<div class="col-sm-5">
								<select id="category_id" name="category_id" class="form-control">
									<option>--Select Category--</option>
									<?php foreach($categories as $key => $value){
									?>
									<option value='<?=$value["cat_id"]?>'><?=$value['cat_title']?></option>
									<?php } ?>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select id="project_id" name="project_id" class="form-control">
									<option>--Select Category--</option>
									
								</select>
							</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Diemensions</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="diemensions" id="diemensions" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Bedrooms</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="bedrooms" id="bedrooms" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="address" id="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">City</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="city" id="city" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Location</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('location')" class="form-control" name="location" id="location" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Distance</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="distance" id="distance" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Washrooms</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="washrooms" id="washrooms" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Total Parks</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="park" id="park" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Price</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="price" id="price" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Property Features</label>
							
							<div class="col-sm-9">

								<div class="col-sm-3">
									<input type="checkbox"  value="Garage" name="features[]" id="features" />
									<label class="control-label">Garage</label>

								</div>
								<div class="col-sm-3">
									<input type="checkbox"  value="Parquet" name="features[]" id="features" />
									<label class="control-label">Parquet</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox"  value="Air Conditioning" name="features[]" id="features" />
									<label class="control-label">Air Conditioning</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox"  value="Internet" name="features[]" id="features" />
									<label class="control-label">Internet</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox"  value="Terrace" name="features[]" id="features" />
									<label class="control-label">Terrace</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox"  value="Roof Rerrace" name="features[]" id="features" />
									<label class="control-label">Roof Terrace</label>

								</div>

								<div class="col-sm-3">
									<input type="checkbox"  value="Car Park" name="features[]" id="features" />
									<label class="control-label">Car Park</label>

								</div>
								<div class="col-sm-3">
									<input type="checkbox"  value="Car Garrage" name="features[]" id="features" />
								<label class="control-label">Car Garrage</label>

								</div>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Property For Rent/Sale</label>
							<div class="col-sm-5">
								<select id="prop_for" name="prop_for" class="form-control">
									<option>--SELECT--</option>
									<option value="Rent">Rent</option>
									<option value="Sale">Sale</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Property Type</label>
							<div class="col-sm-5">
								<select id="property_type" name="property_type" class="form-control">
									<option>--SELECT--</option>
									<option value="Comercial">Comercial</option>
									<option value="Residential">Residential</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Home Type</label>
							<div class="col-sm-5">
								<select id="home_type" name="home_type" class="form-control">
									<option>--SELECT--</option>
									<option value="New_build">New Build</option>
									<option value="Pre-owned">Pre-owned</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Select Agent</label>
							<div class="col-sm-5">
								<select id="agent" name="agent" class="form-control">
									<option>--SELECT--</option>
									<?php foreach ($agents as $key => $va) {
									 ?>
									 <option value="<?=$va['id']?>"><?=$va['name']?></option>
									<?php } ?>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Short Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="4" name="short_description" id="short_description" ></textarea>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="8" name="description" id="description" ></textarea>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select id="status" name="status" class="form-control">
									<option>--SELECT--</option>
									<option value="true">Active</option>
									<option value="false">Deactive</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Is Feature</label>
							<div class="col-sm-5">
								<select id="isfeature" name="isfeature" class="form-control">
									<option>--SELECT--</option>
									<option value="yese">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Is Variant</label>
							<div class="col-sm-5">
								<select id="isvariant" name="isvariant" class="form-control">
									<option>--SELECT--</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Banner Heading</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="heading" id="heading" />
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="banner_address" id="banner_address" />
							</div>
						</div>
						<div class="form-group">
					<label class="col-sm-3 control-label">Banner Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" 
								name="s_description" id="s_description" />
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Slider Image</label>
							<div class="col-sm-5">
								<input type="file" name="slider_image" id="slider_image" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Upload Broucher</label>
							<div class="col-sm-5">
								<input type="file" name="broucher" id="broucher" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-5">
								<input type="file" name="image" id="image" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Thumbnail Image</label>
							<div class="col-sm-5">
								<input type="file" name="thumb_img" id="thumb_img" />
							</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Add Property</button>
							<a role="button" href="<?=SURL?>properties" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

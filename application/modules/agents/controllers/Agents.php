<?php
class Agents extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
        $data['agents']=$this->Crud->getRows('agents','*');
		$data['content']='agents/all_agents';
		$this->template->admin_template($data);
	}
	
	function add_agent()
	{
		$data['content']='agents/add_agent';
		$this->template->admin_template($data);
	}
	function insert_agent(){
		$table = 'agents';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "250" || $height > "250") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 250 * 250 pixels ');
   redirect(SURL.'agents/add_agent/');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/agents/';
                        $projects_folder_path_main = './assets/images/agents/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','This type of files are not allowed');
                redirect(SURL.'agents/add_agent');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

            $data = array(
				"name" => $name,
				"agent_image" => $filename_f,
				"description" => $description,
                "position"=>$position,
                "facebook"=>$facebook,
                "linkdin"=>$linkdin,
                "twitter"=>$twitter,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">agent is added successfully</h5>');
                redirect(SURL.'agents');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add agent please try again</h5>');
                redirect(SURL.'agents/add_agent');
            }
		}
	}


	function edit_agent($id){
		$table = 'agents';
		$id = $id;
		$where = array('id' => $id );
		$data['agent'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'agents/update_agent';
		$this->template->admin_template($data);
	}

	function update_agent($id){
		$table = 'agents';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "250" || $height > "250") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 250 * 250 pixels ');
   redirect(SURL.'agents/edit_agent/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/agents/';
                        $projects_folder_path_main = './assets/images/agents/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','This type of files are not allowed');
                redirect(SURL.'agents/edit_agent/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
            $image_path='./assets/images/agents/';
            $filename = $image_path . $udate_image; 
                if (file_exists($filename))
                {
                    unlink($filename);
                }

		$data = array(
                "name" => $name,
                "agent_image" => $filename_f,
                "description" => $description,
                "position"=>$position,
                "facebook"=>$facebook,
                "linkdin"=>$linkdin,
                "twitter"=>$twitter,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">agent is updated successfully</h5>');
                redirect(SURL.'agents');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update agent please try again</h5>');
                redirect(SURL.'agents/edit_agent/'.$id);
            }
		

	}

	function del_row($id){
			$table = 'agents';
			$id= $id;
			$where = array("id"=>$id);
            $record['dt']=$this->Crud->getRows($table,'*',$where);
            $image_path='./assets/images/agents/';
            $filename = $image_path . $record['dt'][0]['agent_image']; 
                if (file_exists($filename))
                {
                    unlink($filename);
                }
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Agent is deleted successfully</h5>');
                redirect(SURL.'agents');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete agent please try again</h5>');
                redirect(SURL.'agents');
            }
	}  
    function activate_agent($id){

        $table='agents';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Agent is activated successfully</h5>');
                redirect(SURL.'agents');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate agent please try again</h5>');
                redirect(SURL.'agents');
            }
    }
    function deactivate_agent($id){
        $table='agents';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Agent is deactivated successfully</h5>');
                redirect(SURL.'agents');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate agent please try again</h5>');
                redirect(SURL.'agents');
            }
    }
    function assigned_project($id){
        $data['projects']=$this->Crud->getRows('our_projects','*',array("status"=>"true"));
        $data['agents']=$this->Crud->getRows('agents','*',array('id'=>$id));
        $data['content'] = 'agents/projects_assigned';
        $this->template->admin_template($data);
    }
}
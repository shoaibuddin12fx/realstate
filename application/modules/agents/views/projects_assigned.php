<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li>Projects Assigned</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span ><b>Projects Assigned To <?=$agents[0]['name']?></b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'agents/add_agent'?>">
						<i class="fa fa-plus txt-success " title="Add Project"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content ">
					<form id="defaultForm" class="form-horizontal"> 
					
					<fieldset>
						<?php foreach ($projects as $key => $value) {
					$agent=explode(',', $value['agent_id']);
					if(in_array($agents[0]['id'], $agent)){
						?>
						<div class="col-sm-3">
						<label class="control-label"><i class="fa fa-star"></i></label>
						<label class="control-label"><?=$value['title']?></label>
						</div>
						<?php } 
					}?>
					</fieldset>

					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<a role="button" href="<?php echo SURL."agents"?>" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

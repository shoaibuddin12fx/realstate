
<?php
class Aboutus extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{   $table='about_us';
	    $data['detail']=$this->Crud->getRows($table,'*','','id','DESC');
		$data['content']='aboutus/edit_page';
		$this->template->admin_template($data);
	}
	function add_page(){

       $table='about_us';

        if($this->input->post('update_page')){
        	extract($this->input->post());

        	$filename_f = $udate_image;
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "2000" || $height > "1200") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 2000 * 1000 pixels ');
   redirect(SURL.'aboutus');
}else{    
            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/sliders/';
                        $projects_folder_path_main = './assets/images/sliders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 1920;
                        $config_resize['height'] = 800;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
        	$slug = trim($title);
                 $slug = strtolower($slug);
                 $slug = str_replace(' ', '-', $slug);
                
            $data=array("title" => $title,"heading" => $heading,"description" => $description,"content" => $editor1,"keywords" =>$keywords,"slug" =>$slug,"image" =>$filename_f,"comitment"=>$comitment,"vision"=>$vision,"who_we_are"=>$who_we_are);

			
			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Data is added successfully</h5>');
                
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add detail please try again</h5>');
               
            }
        }
        $data['detail']=$this->Crud->getRows('about_us','*','','id','DESC');
		$data['content']='aboutus/edit_page';
		$this->template->admin_template($data);

	}

}
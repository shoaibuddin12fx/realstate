<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Add Role</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add New Role</b></span>
				</div>
                               <div class="box-icons">
				
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>

				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL."roles/create_role" ?>" class="form-horizontal bootstrap-validator-form" enctype="multipart/form-data" novalidate="novalidate">
					<fieldset>
						<legend>Assign The Roles</legend>
						<div class="col-sm-12 form-group">
							<div class="col-sm-6"> 
								<label>Role Title</label>
								<input type="text" class="form-control" name="role_title" id="username" />
							</div>
                                                <div class="clearfix"></div>
						</div>

						<div class="row  form-group">

							<div class="col-sm-12">

								<?php 
								$count=1;
								foreach ($list as $key => $value) {
								         $pid = $value['id']; 
								         if($value['parent'] == 0 ) { 
								         	
								         	?>
								<div class="col-sm-4">
									<label><?php echo ucwords($value['title']); ?></label>
									<?php  foreach ($list as $key => $val) { 
										if($pid ==$val['parent']) { ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" value="<?php echo $val['id'] ;?>" name="role_arr[]" id="<?php echo $val['id'] ;?>" /><?php echo ucwords($val['title']); ?>
											<i class="fa fa-square-o"></i>
										</label>
									</div>
                                   <?php }
                                   } ?>
								</div>

								<?php
								if($count%3==0)
								{
					         		echo '<div class="clearfix"></div><br><br>';
					         	}
								 $count++;
									}
								
								} ?>


							</div>

						</div>

						
						<div class="form-group">
						<div class="col-sm-9 col-sm-offset-10">
							<button type="submit" class="btn btn-primary" value="submit" name="submit" id="submit">Add</button>
							<a role="button" href="<?=SURL?>roles" class="btn btn-primary">Cancel</a>
						
						</div>
					</div>
					</fieldset>
				</form>

			</div>	

			

		</div>
	</div>
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>User Roles</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Roles Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?=SURL?>roles/new_role" title="Add Role">
						<i class="fa fa-plus  txt-success "></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div class="form-group">
                        <div class="col-sm-12">
                    <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
                        
                        </div>
                    </div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>Title</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($list as $key => $value) {
					   extract($value)	
					 ?>
						<tr>
							<td><?php echo $title?></td>
							<td>
							<?php if($status=="true"){ 
								echo "Active"; }
							else{
								echo "Deactive";}
							?>	
							</td>
							<td>
                               
                               <?php 
                            if($value['status'] == "true"){
                                ?>
                                <a href="<?php echo SURL."roles/deactivate_role/".$value['id']?>" title="Deactivate"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                <?php
                            }else{
                                ?>
                                <a href="<?php echo SURL."roles/activate_role/".$value['id']?>" title="Activate"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                                <?php } ?>

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='roles/edit_role'){

								echo'<a href="'.SURL."roles/edit_role/".$id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='roles/delete_role'){?>

								<a href="<?php echo SURL."roles/delete_role/".$id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
							  <a href="<?php echo SURL."roles/role_detail/".$id?>"><i title="View" class="fa fa-eye"></i></a></td>

						</tr>
						
						<?php } ?>
					<!-- End: list_row -->
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});

$(document).ready(function() {
	$('.form-control').tooltip();
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	 	
});
</script>

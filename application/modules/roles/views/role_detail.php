<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Role Details</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span><b>Role Details </b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'roles/new_role'?>">
						<i class="fa fa-plus txt-success " title="Add Role"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>

				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Role Title :<?php echo ucwords($role[0]['title']); ?> </h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-md-12">Access:</label>
					</div>
					<div class="form-group">
						<?php
								$count=1;
								 foreach ($list as $key => $value) {
								         $pid = $value['id']; 
								         if($value['parent'] == 0 ) { ?>
								<div class="col-sm-4">
									<label><?php echo ucwords($value['title']) ; ?></label>
									<?php  foreach ($list as $key => $val) { 
										if($pid ==$val['parent']) { ?>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="role_arr[]" value="<?php echo $val['id'] ;?>" id="<?php echo $val['id'] ;?>" <?php echo (in_array($val['id'], 
											$access)) ? 'checked' : '' ?> disabled /><?php echo ucwords($val['title']);?> 
											<i class="fa fa-square-o"></i>
											
										</label>
									</div>
                                   <?php }
                                   } ?>
								</div>

								<?php
								if($count%3==0){
									echo '<div class="clearfix"></div><br><br>';
								 }
								 $count++;
								}
								} ?>

					</div>
			
					
						<div class="form-group">
						<div class="col-sm-9 col-sm-offset-10">
							<a role="button" href="<?=SURL?>roles" class="btn btn-primary">Cancel</a>
						</div>
					</div>
				</form>
			</div>
			
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}

// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	LoadTimePickerScript(AllTimePickers);
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

<?php
class Roles extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
        $data['list']=$this->Crud->getRows('roles','*',array('id !=','1'));
		$data['content']='roles/roles';
		$this->template->admin_template($data);
	}
	function new_role(){
        $data['list']=$this->Crud->getRows('modules','*','','title','ASC');
		$data['content']='roles/create_role';
		$this->template->admin_template($data);

	}
	function create_role(){

		$table='roles';
		extract($this->input->post());
		$access = implode(';',$role_arr);
		if($this->input->post('submit')){

			$data=array("title"=>$role_title,"access"=>$access,"status"=>"true");
			$qu=$this->Crud->insert($table,$data);
		  if($qu){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Role is added successfully</h5>');
                redirect(SURL.'roles');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add role please try again</h5>');
                redirect(SURL.'roles/new_role');
            }

		}
	}
	function edit_role($id){

        $table='roles';
        $id=$id;
        $where=array("id" =>$id);
        $get_role['list']=$this->Crud->getRows($table,'*',$where);
        $data['role']=$this->Crud->getRows($table,'*',$where);
        $get_role['access'] = explode(';', $get_role['list'][0]['access']);
        $data['access']=$get_role['access'];
        $data['list']=$this->Crud->getRows('modules','*','','title','ASC');
		$data['content']='roles/edit_role';
		$this->template->admin_template($data);


	}
	function update_role($id){

        $table='roles';
        $id=$id;
        $where=array("id" =>$id);
		extract($this->input->post());
		$access = implode(';',$role_arr);

		if($this->input->post('update')){

			$data=array("title"=>$role_title,"access"=>$access,"status"=>$status);
            $qu=$this->Crud->update('roles',$data,$where);
		  if($qu){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Role is updated successfully</h5>');
                redirect(SURL.'roles');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update role please try again</h5>');
                redirect(SURL.'roles/edit_role/'.$id);
            }
        }

	}
	function delete_role($id){

		$table='roles';
		$where=array("id" => $id);

		$qu=$this->Crud->delete($table,$where);

		  if($qu){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Role is deleted successfully</h5>');
                redirect(SURL.'roles');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete role please try again</h5>');
                redirect(SURL.'roles');
            }
	}
	function activate_role($id){

		$table='roles';
		$id=$id;
        $data=array("status" => "true");
		$where=array("id" =>$id);
		$this->Crud->update($table,$data,$where);
		$data['list']=$this->Crud->getRows('roles');
		$data['content']='roles/roles';
		$this->template->admin_template($data);
	}
	function deactivate_role($id){

		$table='roles';
		$id=$id;
		$where=array("id" =>$id);
		$data=array("status" => "false");
		$this->Crud->update($table,$data,$where);
	    $data['list']=$this->Crud->getRows('roles');
		$data['content']='roles/roles';
		$this->template->admin_template($data);
	}
	function role_detail($id){
        $table='roles';
        $id=$id;
        $where=array("id" =>$id);
        $get_role['list']=$this->Crud->getRows($table,'*',$where);
        $data['role']=$this->Crud->getRows($table,'*',$where);
        $get_role['access'] = explode(';', $get_role['list'][0]['access']);
        $data['access']=$get_role['access'];
        $data['list']=$this->Crud->getRows('modules');
		$data['content']='roles/role_detail';
		$this->template->admin_template($data);
	}
}
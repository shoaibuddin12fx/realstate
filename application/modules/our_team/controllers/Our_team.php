 <?php
class Our_team extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
	}

	function add_member()
	{        
		$data['content']='our_team/add_member';
		$this->template->admin_template($data);
	}
    function add_director(){
$data['manager']=$this->Crud->getRows('our_team','*',array("slug"=>"managing director"));
        $data['content']='our_team/add_director';
        $this->template->admin_template($data);
    }
    
    function insert_member(){
		$table = 'our_team';
		 if($this->input->post('create')){
		 	extract($this->input->post());
		 	$created_date = date('Y-m-d H:i:s');
		 	$filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "370" || $height > "450") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 370 * 450 pixels ');
   		$data['content']='our_team/add_member';
		$this->template->admin_template($data);
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/team/';
                        $projects_folder_path_main = './assets/images/team/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of files are not allowed please upload the images only');		
   $data['content']='our_team/add_member';
		$this->template->admin_template($data);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
		 	$data = array(
		 		"name"=>$name,
		 		"email"=>$email,
		 		"phone"=>$phoneNumber,
		 		"designation"=>$designation,
                "facebook"=>$facebook,
                "twitter"=>$twitter,
                "gplus"=>$gplus,
                "insta"=>$insta,
		 		"status"=>$status,
		 		"created_at"=>$created_date,
		 		"image"=>$filename_f,	
		 	);
		 	$inserted = $this->Crud->insert($table,$data);
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Team member is added successfully</h5>');
        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add team member please try again</h5>');
                redirect(SURL.'our_team/add_member','refresh');
            }
		 }
	}
        function insert_director(){
        $table = 'our_team';
         if($this->input->post('create')){
            extract($this->input->post());
            $created_date = date('Y-m-d H:i:s');
            
            $filename_f = $udate_image ;

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "500" || $height > "575") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 500 * 575 pixels ');
  $data['manager']=$this->Crud->getRows('our_team','*',array("slug"=>"managing director"));
        $data['content']='our_team/add_director';
        $this->template->admin_template($data);
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/team/';
                        $projects_folder_path_main = './assets/images/team/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of files are not allowed please upload the images only');
   $data['manager']=$this->Crud->getRows('our_team','*',array("slug"=>"managing director"));
        $data['content']='our_team/add_director';
        $this->template->admin_template($data);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
                $slug='managing director';
            $data = array(
                "name"=>$name,
                "email"=>$email,
                "phone"=>$phoneNumber,
                "slug"=>$slug,
                "designation"=>$designation,
                "description"=>$man_description,
                "first_skill"=>$first_skill,
                "first_strength"=>$first_strength,
                "f_description"=>$f_description,
                "second_skill"=>$second_skill,
                "second_strength"=>$second_strength,
                "s_description"=>$sec_description,
                "status"=>$status,
                "created_at"=>$created_date,
                "image"=>$filename_f,   
            );
            $inserted = $this->Crud->update($table,$data,array("slug"=>"managing director"));
            if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Director is updated successfully</h5>');
               $data['manager']=$this->Crud->getRows('our_team','*',array("slug"=>"managing director"));
        $data['content']='our_team/add_director';
        $this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update director please try again</h5>');
               $data['manager']=$this->Crud->getRows('our_team','*',array("slug"=>"managing director"));
        $data['content']='our_team/add_director';
        $this->template->admin_template($data);
            }
         }
    }
	function edit_member($id){
		$table = 'our_team';
		$id = $id;
        
		$where=array("id" =>$id);
		$data['member']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_team/update_member';
		$this->template->admin_template($data);
	}


	function update_member($id){
		$table = 'our_team';
		$id = $id;
		$where = array("id"=> $id);
		if($this->input->post('update_btn')){

			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');
			$filename_f = $udate_image ;
            if($slug=='managing director'){
                $wdth=500;
                $heght=575;
            }else{
                $wdth=370;
                $heght=450;
            }

list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);
if($width > $wdth || $height > $heght) {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then '.$wdth.' * '.$heght.' pixels ');
   		$data['member']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_team/update_member';
		$this->template->admin_template($data);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/team/';
                        $projects_folder_path_main = './assets/images/team/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of files are not allowed please upload the images only');
   		$data['member']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_team/update_member';
		$this->template->admin_template($data);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
			$data = array(
				"name"=>$name,
		 		"email"=>$email,
		 		"phone"=>$phoneNumber,
		 		"updated_at"=>$updated_at,
		 		"status"=>$status,
		 		"designation"=>$designation,
                "facebook"=>$facebook,
                "twitter"=>$twitter,
                "gplus"=>$gplus,
                "insta"=>$insta,
		 		"image"=>$filename_f,
		 	);
		 	$updated = $this->Crud->update($table,$data,$where);
		 
		 	if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Team member is updated successfully</h5>');

            if($slug=='managing director'){
                $url='our_team/add_director';
            }else{
                $url='our_team';
            }
        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }else{
                
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update team member please try again</h5>');
                redirect(SURL.'our_team/edit_member/'.$id);
            }
		}
	}
	function del_row($id){
			$table = 'our_team';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Team member is deleted successfully</h5>');
                       $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete team member please try again</h5>');
                $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }
	}
	function activate_member($id){

        $table='our_team';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Member is activated successfully</h5>');
                        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate Member please try again</h5>');
                        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }
    }
    function deactivate_member($id){
        $table='our_team';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Member is deactivated successfully</h5>');
                        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate Member please try again</h5>');
                        $data['team']=$this->Crud->getRows('our_team','*',array("slug !="=>"managing director"));
		$data['content']='our_team/our_team';
		$this->template->admin_template($data);
            }
    }
}
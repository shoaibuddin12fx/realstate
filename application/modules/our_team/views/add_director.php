<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Add Managing Director</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Managing Director Details</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
				<?php extract($manager[0]);?>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'our_team/insert_director'?>" enctype="multipart/form-data" class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Managing Director Name</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $name?>" name="name" id="name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $email?>" name="email" id="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mobile number</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $phone?>" name="phoneNumber" id="phoneNumber" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Designation</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $designation?>" name="designation" id="designation" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">First Skill</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Skill Title" class="form-control" value="<?php echo $first_skill?>" name="first_skill" id="first_skill" />
							</div>
							<div class="col-sm-1">
								<input type="number" placeholder="Skill Strength %" class="col-sm-1 form-control" value="<?php echo $first_strength?>" name="first_strength" id="first_strength" />
							</div>
							<div class="col-sm-1">
								<label class="col-sm-1 control-label">%</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">First Skill Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $f_description?>" name="f_description" id="f_description" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Second Skill</label>
							<div class="col-sm-3">
								<input type="text" placeholder="Skill Title" class="form-control" value="<?php echo $second_skill?>" name="second_skill" id="second_skill" />
							</div>
							<div class="col-sm-1">
								<input type="number" placeholder="Skill Strength %" class="col-sm-1 form-control" value="<?php echo $second_strength?>"  name="second_strength" id="second_strength" />
							</div>
							<div class="col-sm-1">
								<label class="col-sm-1 control-label">%</label>
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Second Skill Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $s_description?>" name="sec_description" id="sec_description" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea type="text" class="form-control" rows="4" name="man_description" id="man_description" ><?php echo $description?></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-3">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/team/".$image." width='100'>";}?>
							</div>
						</div>
				<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" id="create" value="creat" class="btn btn-primary">Add</button>
							<a role="button" href="<?=SURL?>our_team" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

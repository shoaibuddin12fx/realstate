<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Add Team Member</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Member</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'our_team/insert_member'?>" enctype="multipart/form-data" class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Member name</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="name" id="name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" id="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mobile number</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="phoneNumber" id="phoneNumber" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Facebook Link</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="facebook" id="facebook" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Twitter Link</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="twitter" id="twitter" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Gplus</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="gplus" id="gplus" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Instagram</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="insta" id="insta" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Designation</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="designation" id="designation" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="form-control" name="status" id="status">
									<option value="true">Active</option>
									<option value="false">Deactive</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-5">
								<input type="hidden" name="status" value="true">
								<input type="file" name="image" id="image" />
							</div>
						</div>
				<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" id="create" value="creat" class="btn btn-primary">Add</button>
							<a role="button" href="<?=SURL?>our_team" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

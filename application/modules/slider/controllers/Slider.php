<?php

class Slider extends MY_Controller {

	function __construct(){

		parent::__construct();
	}
		
	function index()
	{	
	
		$data['sliders']=$this->Crud->getRows('sliders','*');
		$data['content']='slider/all_sliders';
		$this->template->admin_template($data);
	
	}
	function add_slider(){

		$data['content']='slider/add_slider';
        $this->template->admin_template($data);

	}
	function insert_slider(){
		$table = 'sliders';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');
                      $filename_f='';
           if ($_FILES['image']['name'] != "") {           
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "2000" || $height > "1200" || $width < "1920" || $height < "1120") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Greater Then 1920 * 1120 Or Less Then Equal To 2000 * 1000 pixels ');
   redirect(SURL.'slider/add_slider');
}else{            
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/sliders/';
                        $projects_folder_path_main = './assets/images/sliders/';
                        $width=1920;
                        $height=800;

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config['quality'] = 100;
                        $config['overwrite'] = TRUE;    
                        $config['width'] = $width;
                        $config['height'] = $height;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload image for slider');
   redirect(SURL.'slider/add_slider');                           
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main,$width,$height);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
            }
          $data = array(
                "heading" => $s_heading,
                "page" => $page,
                "address" => $address,
                "button_title" => $button_title,
                "link" => $link,
                "display_order" => $order_number,
                "description" => $s_description,
				"image" => $filename_f,
				"created_at" => $created_date,
				"status" => $ismobile,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Slider is added successfully</h5>');
                redirect(SURL.'slider');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add slider please try again</h5>');
                redirect(SURL.'slider/add_slider');
            }
		}
	}
	function edit_slider($id){
		$table = 'sliders';
		$id = $id;
		$where = array('id' => $id );
		$data['list'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'slider/update_slider';
		$this->template->admin_template($data);
	}
	function update_slider($id){
		$table = 'sliders';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

if(!empty($udate_image)){
	$filename_f = $udate_image ;
}else{
            
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "2000" || $height > "1200" || $width < "1920" || $height < "1120") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Greater Then 1920 * 1120 Or Less Then Equal To 2000 * 1000 pixels ');
   redirect(SURL.'slider/edit_slider/'.$id);
}else{    
            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/sliders/';
                        $projects_folder_path_main = './assets/images/sliders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 1920;
                        $config_resize['height'] = 800;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload image for slider');
   redirect(SURL.'slider/edit_slider/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
            }
      
			$data = array(
                "heading" => $s_heading,
                "page" => $page,
                "address" => $address,
                "button_title" => $button_title,
                "link" => $link,
                "display_order" => $order_number,
                "description" => $s_description,
				"image" => $filename_f,
				"updated_at" => $updated_at,
				"status" => $status,

			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Slider is updated successfully</h5>');
                redirect(SURL.'slider');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update slider please try again</h5>');
                redirect(SURL.'slider/edit_slider');
            }
		

	}

	function del_row($id){
			$table = 'sliders';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Slider is deleted successfully</h5>');
                redirect(SURL.'slider');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete slider please try again</h5>');
                redirect(SURL.'slider');
            }
	}
	function activate_slider($id){

        $table='sliders';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Slider is activated successfully</h5>');
                redirect(SURL.'slider');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate slider please try again</h5>');
                redirect(SURL.'slider');
            }
    }
    function deactivate_slider($id){
        $table='sliders';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Slider is deactivated successfully</h5>');
                redirect(SURL.'slider');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate slider please try again</h5>');
                redirect(SURL.'slider');
            }
    }

}
 <div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo AURL;?>">Dashboard</a></li>
            <li><a>Sliders Management</a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    
                    <span><b>Sliders</b></span>
                </div>
                <div class="box-icons">
                    <a href="<?php echo SURL.'slider/add_slider'?>">
                        <i class="fa fa-plus txt-success " title="Add Slider"></i>
                    </a>
                    <a class="collapse-link"  title="Collapse">
                        <i class="fa fa-chevron-up  txt-primary"></i>
                    </a>
                    <a class="expand-link"  title="Full Screen">
                        <i class="fa fa-expand  txt-warning"></i>
                    </a>
                    <a class="close-link" title="Close">
                        <i class="fa fa-times  txt-danger "></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content">

                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">                    
                    <div class="form-group">
                        <div class="col-sm-12">
                    <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
                        
                        </div>
                    </div>
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Heading</th>
                            <th>Page</th>
                            <th>Address</th>
                            <th>Button Title</th>
                            <th>Button Link</th>
                            <th>Display Order</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <!-- Start: list_row -->
                    <?php foreach ($sliders as $key => $value) {
                    extract($value);

                    ?>
                        <tr>
                             
                            <td> <?php if(!empty($image)){ ?><img src="<?php echo SURL.'assets/images/sliders/'.$image ?>">  <?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?> </td>
                            <td><?php echo $heading?></td>
                            <td><?php echo $page?></td>
                            <td><?php echo $address?></td>
                            <td><?php echo $button_title?></td>
                            <td><?php echo $link?></td>
                            <td><?php echo $display_order?></td>
                            <td><?php if($status == 'true'){echo 'Activated';}else{echo 'Deactivated';} ?></td>
                            <td>
                          <?php if($status == "true"){?>
                                <a href="<?php echo SURL.'slider/deactivate_slider/'.$id?>" title="Deactivate"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            <?php }else{?>
                                <a href="<?php echo SURL.'slider/activate_slider/'.$id?>" title="Activate"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                            <?php }?>

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='slider/edit_slider'){

								echo'<a href="'.SURL.'slider/edit_slider/'.$id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='slider/del_row'){?>

								<a href="<?php echo SURL.'slider/del_row/'.$id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
                                    
                            </td>
                        </tr>
                    <?php } ?>  
                    <!-- End: list_row -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
    TestTable1();
    LoadSelect2Script(MakeSelect2);
}

$(document).ready(function() {
    // Load Datatables and run plugin on tables 
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();
});
</script>

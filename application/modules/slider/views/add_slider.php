 <div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo AURL;?>">Dashboard</a></li>
            <li><a>Add Slider</a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    
                    <span><b>Add Slider</b></span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link"  title="Collapse">
                        <i class="fa fa-chevron-up  txt-primary"></i>
                    </a>
                    <a class="expand-link"  title="Full Screen">
                        <i class="fa fa-expand  txt-warning"></i>
                    </a>
                    <a class="close-link" title="Close">
                        <i class="fa fa-times  txt-danger "></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                <form id="defaultForm" method="post" action="<?php echo SURL.'slider/insert_slider'?>" class="form-horizontal" enctype="multipart/form-data">
                    <fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                      <?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>                         </div>
                    </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Heading</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="s_heading" id="s_heading" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="address" id="address" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Button Title</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="button_title" id="button_title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Button Link</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="link" id="link" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Display Order</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="order_number" id="order_number" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="s_description" id="s_description" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Page</label>
                            <div class="col-sm-5">
                                <select class="populate placeholder form-control" name="page" id="page">
                                    <option value="">-- Select a page --</option>
                                    <option value="home">Home Page</option>
                                    <option value="contact">Contact Us Page</option>
                                    <option value="about">About Us Page</option>
                                    <option value="listing">Properties Page</option>
                                    <option value="team">Team Page</option>
                                    <option value="builder">Builders Page</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-5">
                                <select class="populate placeholder form-control" name="ismobile" id="ismobile">
                                    <option value="">-- Select a status --</option>
                                    <option value="true">Active</option>
                                    <option value="false">Deactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Image</label>
                            <div class="col-sm-5">
                                <input type="file" name="image" id="image" required />
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" name="create" value="create" class="btn btn-primary">Add</button>
                            <a type="button" name="create" href="<?=SURL?>slider" class="btn btn-primary">Go Back</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

    // Initialize datepicker
    // Load Timepicker plugin
    // Add tooltip to form-controls
    
    LoadBootstrapValidatorScript(DemoFormValidator);
    // Add drag-n-drop feature to boxes
    WinMove();
});
</script>

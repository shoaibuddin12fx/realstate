 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Payment Plan Management</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Payment Plan Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'payment_plan/add_plan'?>">
						<i class="fa fa-plus txt-success" title="Add Payment Plan"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php if(!empty($payment_plans)){?>
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
						</div>
					</div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>First Text</th>
							<th>Second Text</th>
							<th>Third Text</th>
							<th>Project</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($payment_plans as $key => $value) {
					extract($value);

					?>
						<tr>
							<td><?php echo $first_text ?></td>
							<td><?php echo $second_text ?></td>
							<td><?php echo $third_text ?></td>
							<td><?php echo $title ?></td>
							<td>
								<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='payment_plan/edit_plan'){?>
								<a href="<?php echo SURL.'payment_plan/edit_plan/'.$p_id ;?>" >
										<i class="fa fa-pencil" title="Edit plan"></i>
									</a><?php } 
if($sess[$i]=='payment_plan/del_plan'){?>
								<a href="<?php echo SURL.'payment_plan/del_plan/'.$p_id ;?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a>
								<?php }
								} ?>
								</td>
						</tr>
					<?php } ?>	
					<!-- End: list_row -->
					</tbody>
					
				</table>
			<?php } else{?>
				<div id="dashboard-header" class="row">
				    <div class="col-xs-10 col-sm-12">
				        <h3 align="center">NO PAYMENT PLANs FOR THIS PROJECT</h3>
				    </div>
				    <div class="clearfix visible-xs"></div>
				</div>
				<div class="col-sm-12" align="center">
				   <a role="button" href="<?=SURL?>payment_plan" class="btn btn-primary">Go Back</a>
				</div>
                  <?php } ?>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

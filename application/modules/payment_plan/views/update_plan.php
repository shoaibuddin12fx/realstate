 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Payment Plan</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Payment Plan</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($payment_plan[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'payment_plan/update_plan/'.$p_id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-success">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">First Text</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $first_text ?>" class="form-control" name="first_text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Second Text</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $second_text ?>" class="form-control" name="second_text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Third Text</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $third_text ?>" class="form-control" name="third_text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="pro_id" id="pro_id">
									<option value="">-- Select a project --</option>
									<?php foreach ($projects as $key => $value) {?>
									<option value="<?=$value['id']?>" <?php if($project == $value['id']){echo 'selected';}?> ><?=$value['title'] ?></option>
									<?php } ?>
									
								</select>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>payment_plan" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

<?php
class Payment_plan extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
        $data['projects']=$this->Crud->getRows('our_projects','*',array("status"=>"true"));
		$data['content']='payment_plan/projects';
		$this->template->admin_template($data);
	}
	function plans($project_id){
        $data['payment_plans']=$this->Crud->join_on_tables('payment_plan','our_projects','payment_plan.project=our_projects.id','*',array("payment_plan.project"=>$project_id));
        $data['content']='payment_plan/all_plans';
        $this->template->admin_template($data);

    }
	function add_plan()
	{
        $data['projects']=$this->Crud->getRows('our_projects','*');
		$data['content']='payment_plan/add_plan';
		$this->template->admin_template($data);
	}
	function insert_plan(){
		$table = 'payment_plan';
		if($this->input->post('create')){
			extract($this->input->post());

			$created_date = date('Y-m-d H:i:s');

            $data = array(
				"first_text" => $first_text,
				"second_text" => $second_text,
				"third_text" => $third_text,
				"project" => $pro_id,
			);
            $inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Payment Plan is added successfully</h5>');
                redirect(SURL.'payment_plan');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add payment plan please try again</h5>');
                redirect(SURL.'payment_plan/add_plan');
            }
		}
	}


	function edit_plan($id){
		$table = 'payment_plan';
		$id = $id;
		$where = array('p_id' => $id );
		$data['payment_plan'] = $this->Crud->getRows($table,'*',$where);
        $data['projects'] = $this->Crud->getRows('our_projects','*',array("status"=>"true"));
		$data['content'] = 'payment_plan/update_plan';
		$this->template->admin_template($data);
	}

	function update_plan($id){
		$table = 'payment_plan';
		$id = $id;
		$where = array('p_id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$data = array(
				"first_text" => $first_text,
				"second_text" => $second_text,
				"third_text" => $third_text,
				"project" => $pro_id,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Payment Plan is updated successfully</h5>');
                redirect(SURL.'payment_plan');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update payment plan please try again</h5>');
                redirect(SURL.'payment_plan/edit_plan/'.$id);
            }
		

	}

	function del_plan($id){
			$table = 'payment_plan';
			$id= $id;
			$where = array("p_id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Payment Plan is deleted successfully</h5>');
                redirect(SURL.'payment_plan');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete payment plan please try again</h5>');
                redirect(SURL.'payment_plan');
            }
	}

}
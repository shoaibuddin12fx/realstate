 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Gallries Management</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Gallries Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'property_gallery/add_gallery'?>">
						<i class="fa fa-plus txt-success" title="Add Gallery"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
						</div>
					</div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							
							<th>Project Category</th>
							<th>Project Title</th>
							<th>Proprty Title</th>
							<th>Gallery Images</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($gallery as $key => $value) {
					extract($value);

					?>
						<tr>
							<td><?php echo $cat_title ?></td>
							<td><?php echo $title ?></td>
							<td><?php echo $prop_title ?></td>
							<td>
								<div class="col-sm-12">
								<?php
									$gallry_images=explode(' , ',$gal_images);
									
								 for($i=0;$i<count($gallry_images);$i++){
									?>
									<?php if(!empty($gallry_images[$i])){echo "<img src='".SURL."assets/images/property_gallery/".$gallry_images[$i]."' width='100'>";}?>
								
								<?php } ?>
									
								</div>
						    </td>
							<td>
<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='property_gallery/edit_gallery'){

								echo'<a href="'.SURL.'property_gallery/edit_gallery/'.$gal_id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='property_gallery/del_row'){?>

								<a href="<?php echo SURL.'property_gallery/del_row/'.$gal_id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
							</td>
						</tr>
					<?php } ?>	
					<!-- End: list_row -->
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

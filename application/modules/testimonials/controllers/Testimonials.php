<?php
class Testimonials extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['testimonials']=$this->Crud->getRows('testimonial','*');
		$data['content']='testimonials/testimonials';
		$this->template->admin_template($data);
	}
	
	function add_testimonial()
	{
		$data['content']='testimonials/add_testimonial';
		$this->template->admin_template($data);
	}
	function insert_testimonial(){
		$table = 'testimonial';
		if($this->input->post('create')){
			extract($this->input->post());

			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "100" || $height > "90") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 53 * 52 pixels ');
   redirect(SURL.'testimonials/add_testimonial/');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/testimonials/';
                        $projects_folder_path_main = './assets/images/testimonials/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload image');
   redirect(SURL.'testimonials/add_testimonial/');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

            $data = array(
				"name" => $name,
				"image" => $filename_f,
				"description" => $tes_description,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Testimonial is added successfully</h5>');
                redirect(SURL.'testimonials');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add testimonial please try again</h5>');
                redirect(SURL.'testimonials/add_testimonial');
            }
		}
	}


	function edit_testimonial($id){
		$table = 'testimonial';
		$id = $id;
		$where = array('id' => $id );
		$data['testimonial'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'testimonials/update_testimonial';
		$this->template->admin_template($data);
	}

	function update_testimonial($id){
		$table = 'testimonial';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "100" || $height > "90") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 100 * 90 pixels ');
   redirect(SURL.'testimonials/edit_testimonial/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/testimonials/';
                        $projects_folder_path_main = './assets/images/testimonials/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload the image');
   redirect(SURL.'testimonials/edit_testimonial/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

			$data = array(
				"name" => $name,
				"image" => $filename_f,
				"description" => $tes_description,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Testimonial is updated successfully</h5>');
                redirect(SURL.'testimonials');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update testimonial please try again</h5>');
                redirect(SURL.'testimonials/edit_testimonial');
            }
		

	}

	function del_row($id){
			$table = 'testimonial';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Testimonial is deleted successfully</h5>');
                redirect(SURL.'testimonials');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete testimonial please try again</h5>');
                redirect(SURL.'testimonials');
            }
	}  
    function activate_testimonial($id){

        $table='testimonial';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Testimonial is activated successfully</h5>');
                redirect(SURL.'testimonials');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate Testimonial please try again</h5>');
                redirect(SURL.'testimonials');
            }
    }
    function deactivate_testimonial($id){
        $table='testimonial';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Testimonial is deactivated successfully</h5>');
                redirect(SURL.'testimonials');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate Testimonial please try again</h5>');
                redirect(SURL.'testimonials');
            }
    }

}
<?php
class News_management extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$data['news']=$this->Crud->getRows('news');
		$data['content']='news_management/all_news';
		$this->template->admin_template($data);
	}
	
	function add_news()
	{
		$data['content']='news_management/add_news';
		$this->template->admin_template($data);
	}
	function insert_news(){
		$table = 'news';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels </h5>');
   redirect(SURL.'news_management/add_news/');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/news/';
                        $projects_folder_path_main = './assets/images/news/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'news_management/add_news');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }


}

			$data = array(
				"title" => $title,
				"image" => $filename_f,
				"description" => $description,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Blog is added successfully</h5>');
                redirect(SURL.'news_management');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add blog please try again</h5>');
                redirect(SURL.'news_management/add_news');
            }
		}
	}


	function edit_news($id){
		$table = 'news';
		$id = $id;
		$where = array('id' => $id );
		$data['news'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'news_management/update_news';
		$this->template->admin_template($data);
	}

	function update_news($id){
		$table = 'news';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels ');
   redirect(SURL.'news_management/edit_news/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/news/';
                        $projects_folder_path_main = './assets/images/news/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'news_management/edit_news/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }

}

			$data = array(
				"title" => $title,
				"image" => $filename_f,
				"description" => $description,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Blog is updated successfully</h5>');
                redirect(SURL.'news_management');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update blog please try again</h5>');
                redirect(SURL.'news_management/edit_news/'.$id);
            }
		

	}

	function del_row($id){
			$table = 'news';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Blog is deleted successfully</h5>');
                redirect(SURL.'news_management');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete blog please try again</h5>');
                redirect(SURL.'news_management');
            }
	}

}
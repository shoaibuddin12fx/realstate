<?php
class Subscribers extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['subscribers']=$this->Crud->getRows('subscribers');
		$data['content']='subscribers/subscribers';
        $this->template->admin_template($data);
	}
	function activate_customer($id){

        $table='subscribers';
        $id=$id;
        $data=array("del_status" => "true");
        $where=array("id" => $id);
        $this->Crud->update($table,$data,$where);
        redirect(SURL.'subscribers');
    }
    function deactivate_customer($id){
        $table='subscribers';
        $id=$id;
        $where=array("id" => $id);
        $data=array("del_status" => "false");
        $this->Crud->update($table,$data,$where);
        redirect(SURL.'subscribers');
    }

   
    function newslatter()
    {
        
        $data['subscribers']=$this->Crud->getRows('subscribers');
        $data['content']='subscribers/newslatter';
        $this->template->admin_template($data);
    }

    
    function send_newslatter(){
        $where=array("del_status" => "true");
        $subscribers=$this->Crud->getRows('subscribers',' email ',$where);
$emails='';
foreach ($subscribers as $key => $value) {
    $emails.=$value['email'];
    if($key<count($subscribers)-1){
        $emails.=',';
    }
}
        if($this->input->post('send_newslatter')){
            extract($this->input->post());
            $from_email = 'no_reply@shopus.com';
            $name="Newsletter";
            $subject = $title;
            $message = $editor1;
            $to_email = $emails;   
            $this->load->library('email');

            $config = Array(
                'protocol' => 'mail',
                'smtp_host' => 'smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'sajeel@endurancepartner.com',
                'smtp_pass' => 'sajeel123',
                 
                'smtp_crypto'=>'ssl',
                'mailtype'  => 'html',
              'charset'   => 'iso-8859-1'
                        );

             $this->load->library('email',$config); 
   
             $this->email->from($from_email, $name ); 
             $this->email->to($to_email);
             $this->email->subject($subject); 
             $this->email->message($message); 
       
             //Send mail 
             if($this->email->send()) {
                $this->session->set_flashdata('message', '<h5 class="alert alert-success">Newslatter has been sent successfully</h5>');
               
             }
             else{
               
                $this->session->set_flashdata('message', '<h5 class="alert alert-danger">Email sending failed</h5>');
               }
                redirect("subscribers");
 
        }

    }
	function del_row($id){
			$table = 'subscribers';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
		if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Subscriber is deleted successfully</h5>');
                redirect(SURL.'subscribers');
                }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete subscriber please try again</h5>');
                redirect(SURL.'subscribers');
            }
	}

}
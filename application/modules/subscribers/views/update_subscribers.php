<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Update Customer Information</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Customer</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
               <form id="defaultForm" method="post" action="<?php echo SURL."Customers/update_customer/".$list[0]['id']?>" class="form-horizontal">
					
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Name</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="username" value="<?php echo $list[0]['name'];?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" value="<?php echo $list[0]['email'];?>" />
							</div>
						</div>


						<div class="form-group">
							<label class="col-sm-3 control-label">City</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="city" value="<?php echo $list[0]['city'];?>" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="address" value="<?php echo $list[0]['address'];?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="phoneNumber" value="<?php echo $list[0]['phone'];?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Cnic</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="cnic" value="<?php echo $list[0]['cnic'];?>" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="form-control " style="width:30%;" id="status" name="status" >
                                                     
                                            <option value="true">Active</option>
                                            <option value="false">Deactive</option>
                                </select>
							</div>
						</div>


					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update" id="update" value="update" class="btn btn-primary">Save</button>
							<a href="<?=SURL?>Customers" class="btn btn-primary">Cancel</a>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

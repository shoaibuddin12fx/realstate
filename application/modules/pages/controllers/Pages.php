<?php

class Pages extends MY_Controller {

	function __construct(){

		parent::__construct();
	}
		
	function project_detail($id)
	{	
       $data['pro_sliders']=$this->Crud->getR('projects_sliders','*',array("status"=>"true","project_id"=>$id),'display_order','asc','','');
       $data['project_detail']=$this->Crud->getR('our_projects','*',array("status"=>"true","id"=>$id),'','','','');
       $data['project_gallery']=$this->Crud->getR('project_gallery','*',array("project_id"=>$id),'','','',1);
       $data['project_floor']=$this->Crud->getR('floor_project_gallery','*',array("project_id"=>$id),'','','',1);
       $data['our_agents']=$this->Crud->getR('agents','*',array("status"=>"true"),'','','','');
       $data['payment_plan']=$this->Crud->getR('payment_plan','*',array("project"=>$id),'','','','');
       if($data['project_detail'][0]['page_layout']=='listing_1'){
       $data['content']='pages/project_detail';
       }else{
       	$data['content']='pages/project_layout2';
       }
       $this->template->listing_page_layout($data);
	}
}
  <!--slider start-->
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
        <?php if(!empty($project_detail[0]['video'])){?>
          <div id="vid" class="carousel-item active">
            <?php
           echo str_replace('xyz','iframe',$project_detail[0]['video']);?></div>
           <?php }else{
            foreach ($pro_sliders as $key => $slder) {
            if($key==0){
            $active='active';
          }else{
            $active='';
          }?>
            <div id="vid" class="carousel-item <?=$active?>">
                <?php if(!empty($slder['image'])){
                    echo '<img src="'.IMG.'projects_sliders/'.$slder['image'].'" alt="Los Angeles" width="1100" height="500">';
                }else{
                    echo '<img src="'.IMG.'no_image.png'.'" alt="Los Angeles" width="1100" height="500">';
                }?>
        
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
          <p class="address"><?=$slder['address']?></p>
          <h2><?=$slder['heading']?></h2>
          <p class="desc"><?=$slder['description']?></p>
          <a href="<?=$slder['link']?>" class="caption-btn"><?=$slder['button_title']?></a>
        </div>

    </div>
<?php } 
}?>
      </div>
  </div>

  <div class="container">
      <!--Section: Group of personal cards-->
      <section class="pt-0 pt-sm-5 mt-3 mt-sm-5 pb-3 ">
        <div class="row nl--projects">
          <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
            <div class="row">
            <div class="col-2 nl--icon-box">
               <!----slider end----->
              <div class="nl--icon-line-top"></div>
              <i class="fa fa-3x fa-home "></i>
            </div>
            <div class="col-12 col-sm-10 text-left project-detail-text">
              <h4 class="font-weight-bold mt-3">OVERVIEW</h4>
              <ul class="nl-project-options">
                <li><a href="#gal">gallery</a></li>
                <li><a href="#gal">floor plans</a></li>
                <li><a href="#payment">payment plan</a></li>
                <li><a href="#near">whats near</a></li>
              </ul>
            </div>
          </div>
            <div class="nl--project-desc mt-2 mt-sm-3">
            <?php if(!empty($project_detail[0]['description'])){
            $desc=nl2br($project_detail[0]['description'],true) ;
            $desc = str_replace("<br />", "</p><p>", $desc);
            $desc = "<p style='text-align:justify'>" . $desc . "</p>";
            echo $desc;
          }?>
              <h4 class="pl-0 pl-sm-3">Amenities in your community</h4>
              <ul class="nl-project-services ml-0 pt-2 pl-3">
                <li class="mr-3"><img src="<?=IMG?>serice-ico-1.png" class="mr-4"></li>
                <li class="mr-3"><img src="<?=IMG?>serice-ico-2.png" class="mr-4"></li>
                <li class="mr-3"><img src="<?=IMG?>serice-ico-3.png" class="mr-4"></li>
                <li class="mr-3 project-amenties"><img src="<?=IMG?>serice-ico-4.png" class="mr-4"></li>
                <li class="mr-3 project-amenties project-amenties-laptp"><img src="<?=IMG?>serice-ico-5.png" class="mr-4"></li>
              </ul>
              <a href="<?php echo SURL.'assets/images/property_broucher/'.$project_detail[0]['proj_broucher'] ?>" class="ml-0 ml-sm-3 mt-2">Download broucher</a>
            </div>
          </div>
          <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 order-first order-sm-2 text-left mt-0 mt-sm-4">
      <form method="post" class="form-horizontal nl--project-form" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>
        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-12">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">

            </div>
          </div>


          <!-- Text input-->


          <!-- Select Basic -->


        </fieldset>
      </form>
          </div>
         </div>
      </section>
    <!--Section: Group of personal cards-->
  </div>
<div class="nl-black-background" id="gal">
  <div class="container pt-3 nl-project-gallery">
     <ul class="nav nav-tabs">
      <li class="nav-item m-auto m-sm-0">
        <a class="nav-link active btn btn-dark mx-3" href="#home">Gallery</a>
      </li>
      <li class="nav-item m-auto m-sm-0">
        <a class="nav-link btn btn-dark mx-3" href="#menu2">Floor Plan</a>
      </li>
    </ul>
</div>
    <!-- Tab panes -->
    <div class="tab-content ">
      
  
      <div id="home" class=" tab-pane fade show active"><br>
        <section class="center " > 
         <?php 
         if(!empty($project_gallery[0]['gal_images'])){
                      $gallry_images=explode(' , ',$project_gallery[0]['gal_images']);
                      
                     for($i=0;$i<count($gallry_images)-1;$i++){
            echo  '<div class="mb-2 ml-1 mr-1">
                     <img class="img-fluid re--project-detail__image-designing" src="'.IMG.'project_gallery/'.$gallry_images[$i].'">
                   </div>'; 
                        }
                      }?>

        </section>
      </div>    

      <div id="menu2" class=" tab-pane fade"><br>
       <section class="center " >
                 <?php 
                 if(!empty($project_floor[0]['gal_images'])){
                      $gallry_images=explode(' , ',$project_floor[0]['gal_images']);
                      
                     for($i=0;$i<count($gallry_images)-1;$i++){
                      
            echo  '<div class="mb-2 ml-1 mr-1">
                     <img class="img-fluid re--project-detail__image-designing" src="'.IMG.'floor_project_gallery/'.$gallry_images[$i].'">
                   </div>'; 
                        }
                      }?>
        </section>
      </div>
    </div>
  </div>
  <?php if(!empty($payment_plan)){?>
  <div class="payment-section">
  <div class="container">
      <!--Section: Group of personal cards-->
       <section class="pt-5 pb-5 " id="payment">
        <div class="row nl--projects">
            <div class="col-0 col-sm-2 nl--icon-box">
              <div class="project-detail-left project-lft-line"></div>
              <i class="fa fa-3x fa-home "></i>
            </div>
            <div class="col-12 col-sm-10">
              <h4 class="font-weight-bold">PAYMENT PLAN</h4>
              <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>
            </div>
        <div class="col-10 mx-auto">
            <div class="accordion  d-block d-sm-none" id="faqExample">
                <div class="card">
                    <div class="card-header p-2" id="headingOne">
                        <h5 class="mb-0">
                            <button class="payment-plan-btn btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                             <h4 class="font-weight-bold">Payment Plans</h4>
                            </button>
                          </h5>
                    </div>

              <div id="collapseOne" class="collapse show payment-section" aria-labelledby="headingOne" data-parent="#faqExample">
              <ul class="mbile-pricing-plan pt-5 mt-3 mb-3">
                <?php foreach ($payment_plan as $key => $value) {
                  echo '<li><span>'.$value['first_text'].'</span><h3>'.$value['second_text'].'</h3><span>'.$value['third_text'].'</span></li>';
                } ?>
              </ul>
                    </div>
                </div>
            </div>

        </div>
              <ul class="nl-pricing-plan pt-5 mt-3 mb-3">
                <?php foreach ($payment_plan as $key => $value) {
                  echo '<li><span>'.$value['first_text'].'</span><h3>'.$value['second_text'].'</h3><span>'.$value['third_text'].'</span></li>';
                } ?>
              </ul>


         </div>
      </section>
    <!--Section: Group of personal cards-->
  </div>
</div>
<?php } ?>
<div class="nl-black-background">
  <div class="container">
      <!--Section: Group of personal cards-->
      <section class="pt-5 pb-3 " id="near">
        <div class="row nl--projects  mt-5">
            <div class="col-0 col-sm-2 nl--icon-box">
              <div class="nl--icon-line-top"></div>
              <i class="fa fa-3x fa-home text-white "></i>
            </div>
            <!--<div class="col-12 col-sm-10">-->
            <!--  <h4 class="font-weight-bold text-white">SEE WHAT'S NEAR YOUR HOME</h4>-->
            <!--  <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>-->

            <!--</div>-->
         </div>
         <div class="row nl--projects pt-2 pt-sm-5">
            <div class="col-12 col-sm-7">
<?=str_replace('xyz','iframe',$project_detail[0]['location']);?>
            </div>
            <style>
                .mapouter,.gmap_canvas{width:100% !important;}
            </style>
            <div class="col-12 col-sm-5 order-first order-sm-2">
              <ul class="nl-project-nearby pl-0 pl-sm-3 pl-lg-5 mt-2 mt-sm-0">
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
              </ul>
            </div>
         </div>
      </section>
    <!--Section: Group of personal cards-->
  </div>
</div>
<?php if(!empty($our_agents)){?>
    <div class="nl--testimonials pb-5 ">
      <div class="container">
        <!--Section: Group of personal cards-->
        <section class="pt-5 mt-3 d-none d-sm-block pb-3">
        <div class="row nl--projects">
            <div class="col-0 col-sm-2 nl--icon-box">
      <div class="nl--icon-line-top"></div>
              <i class="fa fa-3x fa-quote-right "></i>
            </div>
            <div class="col-12 col-sm-10">
              <h4 class="font-weight-bold">Our Agents </h4>
              <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>

            </div>
         </div>
       </section>
     <div class="last_slider projects-testimonisl">

      <div id="demo2">
        <div id="owl-demo24" class="owl-carousel">
        <?php
        $agent=explode(',', $project_detail[0]['agent_id']);
         foreach ($our_agents as $key => $value) {
         if(in_array($value['id'], $agent)){    
        echo  '<div class="item">
                    <div class="brands_col">
                      <div class="row">
                        <div class="col-6 col-md-12 col-lg-3 pr-0 pr-md-3 mx-auto">
                          <img src="'.IMG.'agents/'.$value['agent_image'].'" alt="" class="rounded-circle agent_img agent_img-testimony">
                        </div>
                        <div class="col-12 col-md-12 col-lg-9">
                         <p class="pr-0 px-2 pr-sm-2 px-lg-0 text-center text-lg-left">'.$value['description'].'</p>
                          <p class="ml-0 pl-0 name text-center text-lg-left">'.$value['name'].'</p>
                        </div>
                      </div>
                    </div>
                  </div>';
          } 
        }?>
        </div>
      </div>
      <a href="<?php echo SURL.'contact_us'?>" class="mt-5">Contact Us </a>
    </div>
  </div>
</div>
<?php } ?>
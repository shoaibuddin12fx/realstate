  <!--slider start-->
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
      <?php foreach ($pro_sliders as $key => $slders) {
          if($key==0){
            $active='active';
          }else{
            $active='';
          }
      ?>
      <div class="carousel-item <?=$active?>">
          <?php if(!empty($image)){
         echo '<img src="'.IMG.'projects_sliders/'.$slders['image'].'" alt="Los Angeles" width="1100" height="500">';
         }else{
        echo '<img src="'.IMG.'no_image.png'.'" alt="Los Angeles" width="1100" height="500">';
        } ?>
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker">
          <p class="address"><?=$slders['address']?></p>
          <h2><?=$slders['heading']?></h2>
          <p class="desc"><?=$slders['description']?></p>
          <a href="<?=$slders['link']?>" class="caption-btn"><?=$slders['button_title']?></a>
        </div>
      </div>
    <?php } ?>
    </div>
  </div>

  <div class="container">
      <!--Section: Group of personal cards-->
      <section class="pt-5 mt-5 pb-5 mb-4">
        <div class="row nl--projects">
          
          <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
            <div class="col-2 nl--icon-box">
               <!----slider end----->
              <div class="nl--icon-line-top"></div>
              <i class="fa fa-3x fa-home "></i>
            </div>
            <div class="col-10 text-left">
              <h4 class="font-weight-bold mt-3">OVERVIEW</h4>
              <ul class="nl-project-options">
                <li><a href="#gal">Gallery</a></li>
                <li><a href="<?php if(!empty($project_detail[0]['payment_plan'])){ echo SURL.'assets/images/property_broucher/'.$project_detail[0]['payment_plan'];}else{echo '#';} ?>" Target="blank">Payment plan</a></li>
                <li><a href="#near">Whats Near</a></li>
              </ul>
            </div>
            <div class="nl--project-desc mt-5">
            <?php if(!empty($project_detail[0]['description'])){
            $desc=nl2br($project_detail[0]['description'],true) ;
            $desc = str_replace("<br />", "</p><p>", $desc);
            $desc = "<p style='text-align:justify'>" . $desc . "</p>";
            echo $desc;
          }?>
              <a href="<?php if(!empty($project_detail[0]['proj_broucher'])){ echo SURL.'assets/images/property_broucher/'.$project_detail[0]['proj_broucher'];}else{echo '#';} ?>" Target="blank" class="nl--project_layout2-desc-a_1 ml-2 mt-3">Download broucher</a>
              <a href="#" class="nl--project_layout2-desc-a_2 ml-2 mt-3">Download Floor Plan</a>
            </div>
			</div>
          
    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 text-left mt-4">
      <form method="post" class="form-horizontal nl--project-form" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>
        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-4  col-12">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">

            </div>
          </div>
        </fieldset>
      </form>
    </div>
         </div>
      </section>
    <!--Section: Group of personal cards-->
  </div>
<div class="nl-black-background" id="gal">
  <div class="container pt-3 nl-project-gallery">
    <div class="row">
    <h5 class="col-6 pt-3 nl--project-layout2-h5 mr-1">Gallery</h5>
    <a href="<?php if(!empty($project_detail[0]['payment_plan'])){ echo SURL.'assets/images/property_broucher/'.$project_detail[0]['payment_plan'];}else{echo '#';} ?>" Target="blank" class="nl--project_layout2-payment-a_2 col-4 ml-5 mt-3">Download Payment Plan</a>
  </div>
    <div class="row pt-4 pb-5">
          <?php if(!empty($project_gallery[0]['gal_images'])){?>
                <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 for-plan">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
         <?php 
         if(!empty($project_gallery[0]['gal_images'])){
                      $gallry_images=explode(' , ',$project_gallery[0]['gal_images']);
                      
                     for($i=0;$i<count($gallry_images)-1;$i++){
                        if($i==0){
                          $active='active';
                        }else{
                          $active='';
                        }

            echo  '<div class="carousel-item '.$active.'">
                     <img class="img-fluid" src="'.IMG.'project_gallery/'.$gallry_images[$i].'">
                   </div>'; 
                        }
                      }?>
                    </div>
                  </div>
                </div>
        <?php } ?>
            <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
              <ul class="nl-project-nearby">
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>
                <li class="fa fa-long-arrow-right">  RESTAURANTS</li>

              </ul>
            </div>
    <!-- Tab panes -->
  </div>
  </div>
<div class="nl-black-background">
  <div class="container">
      <!--Section: Group of personal cards-->
      <section class="pt-4 pb-3" id="near">
        <div class="row nl--projects  mt-5">
            <div class="col-1 nl--icon-box">
              <div class="nl--icon-line-left"></div>
              <i class="fa fa-3x fa-home text-white "></i>
            </div>
            <div class="col-10">
              <h4 class="font-weight-bold text-white">SEE WHAT'S NEAR YOUR HOME</h4>
              <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>

            </div>
         </div>
         <div class="row nl--projects pt-5 pb-4">
            <div class="col-12">
              <?=str_replace('xyz','iframe',$project_detail[0]['location']);?>
            </div>
          </div>
      </section>
    <!--Section: Group of personal cards-->
  </div>
</div>
</div>
<?php if(!empty($our_agents)){?>
    <div class="nl--testimonials pb-5 ">
      <div class="container">
        <!--Section: Group of personal cards-->
        <section class="pt-5 mt-3 pb-3">
        <div class="row nl--projects">
            <div class="col-1 nl--icon-box">
      <div class="nl--icon-line-top"></div>
              <i class="fa fa-3x fa-quote-right "></i>
            </div>
            <div class="col-10">
              <h4 class="font-weight-bold">Our Agents </h4>
              <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>

            </div>
         </div>
       </section>
     <div class="last_slider projects-testimonisl">

      <div id="demo2">
        <div id="owl-demo24" class="owl-carousel">
        <?php
        $agent=explode(',', $project_detail[0]['agent_id']);
         foreach ($our_agents as $key => $value) {
         if(in_array($value['id'], $agent)){    
        echo  '<div class="item">
                    <div class="brands_col">
                      <div class="row">
                        <div class="col-6 col-sm-3 m-auto">
                          <img src="'.IMG.'agents/'.$value['agent_image'].'" alt="" class="rounded-circle agent_img">
                        </div>
                        <div class="col-12 col-sm-9">
                         <p class="pr-0 px-2 pr-sm-2 px-sm-0 text-center text-sm-left">'.$value['description'].'</p>
                          <p class="ml-0 pl-0 name">'.$value['name'].'</p>
                        </div>
                      </div>
                    </div>
                  </div>';
          } 
        }?>
        </div>
      </div>
      <a href="contact.html" class="mt-5">Contact Us </a>
    </div>
  </div>
</div>
<?php } ?>
 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Update Project Slider</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Project slider</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'slider/add_slider'?>">
						<i class="fa fa-plus txt-success " title="Add Slider"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
				<?php extract($list[0]); ?>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'projects_sliders/update_slider/'.$id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Heading</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $heading ?>" class="form-control" name="heading" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $address ?>" class="form-control" name="address" id="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Button Title</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $button_title ?>" class="form-control" name="button_title" id="button_title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Button Link</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $link ?>" class="form-control" name="link" id="link" />
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Display Order</label>
                            <div class="col-sm-5">
                                <input type="text" value="<?=$display_order?>" class="form-control" name="order_number" id="order_number" />
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $description ?>" class="form-control" name="s_description" id="s_description" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';}?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';}?> >Deactivate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="project" id="project">
									<option value="">-- Select a Project --</option>
									<?php foreach ($projects as $key => $value) { ?>
									<option value="<?=$value['id']?>" <?php if($value['id'] == $project_id){echo 'selected';}?> ><?=$value['title']?></option>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-2">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/projects_sliders/".$image." width='100'>";}?>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a type="button" name="create" href="<?=SURL?>projects_sliders" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

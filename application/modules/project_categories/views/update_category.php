 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Builder</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Builder</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($category[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'project_categories/update_category/'.$cat_id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Builder Title</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $cat_title ?>" class="form-control" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Discription</label>
							<div class="col-sm-5">
								<textarea class="form-control" id="description" name="description" rows="8"><?php echo $description ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';}?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';}?> >Deactivate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Builder Logo</label>
							<div class="col-sm-3">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $proj_image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
							<?php if(!empty($proj_image)){ ?><img src="<?php echo SURL.'assets/images/builders/'.$proj_image ?>" width='100%'><?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?>
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Heading</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $banner_heading ?>" class="form-control" name="heading" id="heading" />
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $banner_address ?>" class="form-control" name="banner_address" id="banner_address" />
							</div>
						</div>
						<div class="form-group">
					<label class="col-sm-3 control-label">Banner Description</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $banner_description ?>" class="form-control" name="s_description" id="s_description" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Banner Image</label>
							<div class="col-sm-3">
								<input type="file"  name="slider_image" id="slider_image" />
								<input type="hidden" id="up_slider" value="<?php echo $builder_gal ?>"  name="up_slider">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($builder_gal)){ ?><img src="<?php echo SURL.'assets/images/builders/'.$builder_gal ?>" width='100%'><?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?>
								
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>project_categories" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Add Builder</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Builder</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'project_categories/insert_category'?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Builder Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="title" id="title" />
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="8" name="description" id="description" ></textarea>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select id="status" name="status" class="form-control">
									<option>--SELECT--</option>
									<option value="true">Active</option>
									<option value="false">Deactive</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Builder Logo</label>
							<div class="col-sm-5">
								<input type="file" name="image" id="image" />
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Banner Heading</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="heading" id="heading" />
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Banner Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="banner_address" id="banner_address" />
							</div>
						</div>
						<div class="form-group">
					<label class="col-sm-3 control-label">Banner Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" 
								name="s_description" id="s_description" />
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Banner Image</label>
							<div class="col-sm-5">
								<input type="file" name="slider_image" id="slider_image" />
							</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Add Builder</button>
							<a role="button" href="<?=SURL?>project_categories" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

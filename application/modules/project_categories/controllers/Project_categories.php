<?php
class Project_categories extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['categories']=$this->Crud->getRows('project_categories');
		$data['content']='project_categories/all_categories';
		$this->template->admin_template($data);
	}
	
	function add_category()
	{
		$data['content']='project_categories/add_category';
		$this->template->admin_template($data);
	}
	function insert_category(){
		$table = 'project_categories';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');
			  $filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "250" || $height > "250") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 250 * 250 pixels ');
   redirect(SURL.'project_categories/add_category/');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/builders/';
                        $projects_folder_path_main = './assets/images/builders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload only image');
   redirect(SURL.'project_categories/add_category/');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
             $filename_sli='';

                 
list($width, $height) = getimagesize($_FILES["slider_image"]['tmp_name']);

if($width > "2000" || $height > "1200" || $width < "1920" || $height < "1120") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be 1920 *1120 Or Less Then 2000 * 1200 pixels ');
   redirect(SURL.'project_categories/add_category/');
}else{    
                          
                    if ($_FILES['slider_image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/builders/';
                        $projects_folder_path_main = './assets/images/builders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 100;
                        $config_resize['height'] = 90;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('slider_image')) {
   $this->session->set_flashdata('message','This type of file is not allowed please upload only image');
   redirect(SURL.'project_categories/add_category/');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_sli= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_sli, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }


                 $slug = trim($title);
                 $slug = strtolower($slug);
                 $slug = str_replace(' ', '-', $slug);
                 $dt=substr(str_shuffle('abcdefg'),0,1);
                 
                 xyz:
                 $chk=$this->Crud->getRows('project_categories','*',array("slug"=>$slug));
                                 
                    if(count($chk)>0){
                        $slug=$slug.$dt;
                        goto xyz;
                    }

                      
			$data = array(
				"cat_title" => $title,
				"slug" => $slug,
                                "description"=>$description,
                "banner_heading" => $heading,
                "banner_description" => $s_description,
                "banner_address" => $banner_address,
				"created_at" => $created_date,
			 	"proj_image"=>$filename_f,
				"status" => $status,
                "builder_gal" => $filename_sli,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project category is added successfully</h5>');
                redirect(SURL.'project_categories');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add project category please try again</h5>');
                redirect(SURL.'project_categories/add_category');
            }
		}
	}


	function edit_category($id){
		$table = 'project_categories';
		$id = $id;
		$where = array('cat_id' => $id );
		$data['category'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'project_categories/update_category';
		$this->template->admin_template($data);
	}

	function update_category($id){
		$table = 'project_categories';
		$id = $id;
		$where = array('cat_id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');
			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "250" || $height > "250") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 250 * 250 pixels </h5>');
   redirect(SURL.'project_categories/edit_category/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/builders/';
                        $projects_folder_path_main = './assets/images/builders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = $width;
                        $config_resize['height'] = $height;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload only image</h5>');
   redirect(SURL.'project_categories/edit_category/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
                $filename_sli = $up_slider ;
    if($_FILES['slider_image']['name'] != ""){        
    list($width, $height) = getimagesize($_FILES["slider_image"]['tmp_name']);

if($width > "2000" || $height > "1200") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be 1920 * 1120 Or Less Then 2000 * 1200 pixels </h5>');
   redirect(SURL.'project_categories/edit_category/'.$id);
}else{    

            if($_FILES['slider_image']['name'] != ""){

                        $projects_folder_path = './assets/images/builders/';
                        $projects_folder_path_main = './assets/images/builders/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = $width;
                        $config_resize['height'] = $height;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('slider_image')) {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">This type of file is not allowed please upload only image</h5>');
   redirect(SURL.'project_categories/edit_category/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_sli= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_sli, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
            }

			$data = array(
				"cat_title" => $title,
				"description" => $description,
                "banner_heading" => $heading,
                "banner_description" => $s_description,
                "banner_address" => $banner_address,
				"updated_at" => $updated_at,
				"proj_image"=>$filename_f,
                "builder_gal"=>$filename_sli,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project category is updated successfully</h5>');
                redirect(SURL.'project_categories');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update project category please try again</h5>');
                redirect(SURL.'project_categories/edit_category/'.$id);
            }
		

	}

	function del_category($id){
			$table = 'project_categories';
			$id= $id;
			$where = array("cat_id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is delted successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete project please try again</h5>');
                redirect(SURL.'our_projects');
            }
	}
    function activate_category($id){

        $table='project_categories';
        $id=$id;
        $data=array("status" => "true");
        $where=array("cat_id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project category is activated successfully</h5>');
                redirect(SURL.'project_categories');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate project category please try again</h5>');
                redirect(SURL.'project_categories');
            }
    }
    function deactivate_category($id){
        $table='project_categories';
        $id=$id;
        $where=array("cat_id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project category is deactivated successfully</h5>');
                redirect(SURL.'project_categories');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate project category please try again</h5>');
                redirect(SURL.'project_categories');
            }
    }
}
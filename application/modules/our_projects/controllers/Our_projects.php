<?php
class Our_projects extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		
		$data['projects']=$this->Crud->join_on_tables('our_projects','project_categories','our_projects.category_id=project_categories.cat_id','*,our_projects.description as proj_description,our_projects.status as proj_status');
		$data['content']='our_projects/all_projects';
		$this->template->admin_template($data);
	}
	
	function add_project()
	{
        $data['categories']=$this->Crud->getRows('project_categories','*',array("status"=>"true"));
		$data['content']='our_projects/add_project';
		$this->template->admin_template($data);
	}
function insert_project(){
		$table = 'our_projects';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';

                 
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

// if($width > "1000" || $height > "1000") {
//   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels </h5>');
//   redirect(SURL.'our_projects/add_project/');
// }else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/projects/';
                        $projects_folder_path_main = './assets/images/projects/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'our_projects/add_project');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                //}
                $broucher='';
                    if ($_FILES['broucher']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/property_broucher/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf|jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('broucher')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/add_project');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $broucher= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($broucher, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                $payment='';
                    if ($_FILES['payment']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/property_broucher/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf|jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('payment')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/add_project');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $payment= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($payment, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
 $proj_floor='';
                    if ($_FILES['proj_floor']['name'] != "") {
                        
                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/property_broucher/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('proj_floor')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/add_project');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $proj_floor= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($proj_floor, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }


			$data = array(
				"title" => $title,
                                "project_address" => $address,
                "short_description"=>$short_description,
                "description"=>$description,
                "feature"=>$feature,
                "company_name"=>$company_name,
				"image" => $filename_f,
                "proj_broucher"=>$broucher,
                "proj_floor"=>$proj_floor,
                "payment_plan"=>$payment,
				"created_at" => $created_date,
				"status" => $status,
                "category_id" => $category_id,
                "page_layout" => $layout,
                "layout_image" => $layo_img,
                "video" => $video,
                "display_order" => $display_order,
                "delivery_date" => $delivery_date,
                "one_line_desc" => $one_line,
                "location" => $location,

				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is added successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add project please try again</h5>');
                redirect(SURL.'our_projects/add_project');
            }
		}
	}


	function edit_project($id){
		$table = 'our_projects';
		$id = $id;
		$where = array('id' => $id );
		$data['project'] = $this->Crud->getRows($table,'*',$where);
        $data['categories'] = $this->Crud->getRows('project_categories','*',array("status"=>"true"));
		$data['content'] = 'our_projects/update_project';
		$this->template->admin_template($data);
	}

	function update_project($id){
		$table = 'our_projects';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			
if ($_FILES['image']['name'] != "") {

list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels ');
   redirect(SURL.'our_projects/edit_project/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/projects/';
                        $projects_folder_path_main = './assets/images/projects/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                        $image = './assets/images/projects/'.$udate_image;
                        if (file_exists($image))
                        {
                            unlink($image);
                        }
                    }
                }
}
else{
			$filename_f = $udate_image ;
        }
                                    $broucher=$up_broucher;
                    if($_FILES['broucher']['name'] != ""){

                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf|jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('broucher')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $broucher= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($broucher, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                        $brouch= './assets/images/property_broucher/'.$up_broucher;
                        if (file_exists($brouch))
                        {
                            unlink($brouch);
                        }
                    }
            $payment=$up_payment;
                    if($_FILES['payment']['name'] != ""){

                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/properties/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf|jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('payment')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $payment= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($payment, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    $payment_plan= './assets/images/property_broucher/'.$up_payment;
                    if (file_exists($payment_plan))
                        {
                            unlink($payment_plan);
                        }
                    }
$proj_floor=$up_proj_floor;
                    if($_FILES['proj_floor']['name'] != ""){

                        $properties_folder_path = './assets/images/property_broucher/';
                        $properties_folder_path_main = './assets/images/property_broucher/';

                        $thumb = $properties_folder_path_main . 'thumb';
                        //$f240x240 = $properties_folder_path_main . '240x240';

                        $config['upload_path'] = $properties_folder_path;
                        $config['allowed_types'] = 'pdf|jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = false;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('proj_floor')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image or pdf instead of any other file</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $proj_floor= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($proj_floor, $full_path, $properties_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                        $proj_f= './assets/images/property_broucher/'.$up_proj_floor;
                        if (file_exists($proj_f))
                        {
                            unlink($proj_f);
                        }
                    }
           
			$data = array(
				"title" => $title,
                "project_address" => $address,
				"image" => $filename_f,
                "proj_broucher" => $broucher,
                "payment_plan"=>$payment,
                "proj_floor"=>$proj_floor,
				"description" => $description,
                "feature"=>$feature,
                "short_description" => $short_description,
                "company_name" => $company_name,
				"updated_at" => $updated_at,
				"status" => $status,
                "category_id" => $category_id,
                "page_layout" => $layout,
                "layout_image" => $layo_img,
                "video" => $video,
                "display_order" => $display_order,
                "delivery_date" => $delivery_date,
                "one_line_desc" => $line_desc,
                "location" => $location,

				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is updated successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update project please try again</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
            }
	}
    function activate_project($id){

        $table='our_projects';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is activated successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate project please try again</h5>');
                redirect(SURL.'our_projects');
            }
    }
    function deactivate_project($id){
        $table='our_projects';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is deactivated successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate project please try again</h5>');
                redirect(SURL.'our_projects');
            }
    }

	function del_project($id){
			$table = 'our_projects';
			$id= $id;
			$where = array("id"=>$id);
            $record['dt']=$this->Crud->getRows($table,'*',$where);
            $image = './assets/images/projects/'.$record['dt'][0]['image'];
            $broucher= './assets/images/property_broucher/'.$record['dt'][0]['proj_broucher'];
            $payment_plan= './assets/images/property_broucher/'.$record['dt'][0]['payment_plan'];
            $proj_floor= './assets/images/property_broucher/'.$record['dt'][0]['proj_floor'];
                if (file_exists($image))
                {
                    unlink($image);
                }
                if (file_exists($broucher))
                {
                    unlink($broucher);
                }
                if (file_exists($payment_plan))
                {
                    unlink($payment_plan);
                }
                if (file_exists($proj_floor))
                {
                    unlink($proj_floor);
                }
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Project is delted successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete project please try again</h5>');
                redirect(SURL.'our_projects');
            }
	}
function remove($id,$colom){
        $table='our_projects';
        $where=array("id"=>$id);
        $data=array($colom=>'');
        $record['dt']=$this->Crud->getRows($table,'*',$where);
        if($colom=='proj_floor'){
           $proj_floor= './assets/images/property_broucher/'.$record['dt'][0]['proj_floor'];
            if (file_exists($proj_floor))
            {
                unlink($proj_floor);
            } 
        }
        else if($colom=='proj_broucher'){
            $broucher= './assets/images/property_broucher/'.$record['dt'][0]['proj_broucher'];
                if (file_exists($broucher))
                {
                    unlink($broucher);
                }
        }
        else{

        $payment_plan= './assets/images/property_broucher/'.$record['dt'][0]['payment_plan'];
            if (file_exists($payment_plan))
                {
                    unlink($payment_plan);
                }
        }
        $updated = $this->Crud->update($table,$data,$where);
            if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">File is removed successfully</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to remove file please try again</h5>');
                redirect(SURL.'our_projects/edit_project/'.$id);
            }

    }
    function assigne_agent($id){
        $table='agents';
        $data['id']=$id;
        $data['project']=$this->Crud->getRows('our_projects','*',array("id"=>$id));
        $data['agents']=$this->Crud->getRows($table,'*',array("status"=>"true"));
        $data['content']='our_projects/agents';
        $this->template->admin_template($data);
    }
    function add_agent($id){

        if($this->input->post('create')){
            extract($this->input->post());
            $agents=implode(',', $features);
            $data=array("agent_id"=>$agents);
            $updated = $this->Crud->update('our_projects',$data,array("id"=>$id));
            if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Agents are assigned successfully</h5>');
                redirect(SURL.'our_projects');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to assigne agents please try again</h5>');
                redirect(SURL.'our_projects/assigne_agent/'.$id);
            }
        }
    }
}
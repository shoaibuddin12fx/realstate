<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Assigne Agents</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span ><b>Project : <?=$project[0]['title']?></b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'our_projects/add_project'?>">
						<i class="fa fa-plus txt-success " title="Add Project"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content ">
					<form id="defaultForm" method="post" action="<?php echo SURL.'our_projects/add_agent/'.$project[0]['id']?>" class="form-horizontal" enctype="multipart/form-data"> 
					
					<fieldset>
						<?php foreach ($agents as $key => $value) {
						?>						
                                                  <div class="col-sm-2">
									<input type="checkbox" <?php if(!empty($project[0]['agent_id'])){

$agents_id=explode(',', $project[0]['agent_id']);
 if(in_array($value['id'], $agents_id)){echo 'checked';} } ?>  value="<?=$value['id']?>" name="features[]" id="features" />
									<label class="control-label"><?=$value['name']?></label>
								</div>
								<?php } ?>
</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Assignee Agent</button>
							<a role="button" href="<?php echo SURL."our_projects"?>" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

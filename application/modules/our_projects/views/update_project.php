 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Project</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Update Project</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($project[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'our_projects/update_project/'.$id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $title ?>" class="form-control" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $project_address ?>" class="form-control" name="address" id="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Company Name</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $company_name ?>" class="form-control" name="company_name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Short Discription</label>
							<div class="col-sm-5">
								<textarea class="form-control" id="short_description" name="short_description" rows="4"><?php echo $short_description ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Discription</label>
							<div class="col-sm-5">
								<textarea class="form-control" id="description" name="description" rows="8"><?php echo $description ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Display Order</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $display_order ?>" name="display_order" id="display_order" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">One line Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $one_line_desc ?>" name="line_desc" id="line_desc" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Delivery Date</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $delivery_date ?>" name="delivery_date" id="delivery_date" />
								
							</div>
						</div>
						<div class="form-group">
						<label class="col-sm-3 control-label">Youtube Video Embended Code</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('video')" class="form-control" name="video" id="video" ><?=$video?></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Location</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('location')" class="form-control" name="location" id="location" ><?=$location?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Is Feature</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="feature" id="feature">
									<option value="">-- Select--</option>
									<option value="yes" <?php if($feature == 'yes'){echo 'selected';}?> >Yes</option>
									<option value="no" <?php if($feature == 'no'){echo 'selected';}?> >No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';}?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';}?> >Deactivate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Builder</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="category_id" id="category_id">
									<option value="">-- Select a category --</option>
									<?php foreach ($categories as $key => $value) {
									?>
									<option value='<?=$value["cat_id"]?>' <?php if($value['cat_id'] == $category_id){echo 'selected';}?> ><?=$value['cat_title']?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Broucher</label>
							<div class="col-sm-3">
								<input type="file"  name="broucher" id="broucher" />
								<input type="hidden" id="up_broucher" value="<?php echo $proj_broucher ?>"  name="up_broucher">
							</div>
                   <?php if(!empty($proj_broucher)){ ?>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($proj_broucher)){ echo SURL.'assets/images/property_broucher/'.$proj_broucher ;}else{echo "#";}?>" <?php if(!empty($proj_broucher)){echo 'target="_blank"';}?> class="btn btn-primary" title="View Broucher">Broucher</a>
							</div>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($proj_broucher)){ echo SURL.'our_projects/remove/'.$id.'/proj_broucher' ;}?>" class="btn btn-primary" title="Remove Broucher"><i class="fa fa-window-close"></i></a>
							</div>
<?php } ?>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Project Floor Plan</label>
							<div class="col-sm-3">
								<input type="file"  name="proj_floor" id="proj_floor" />
								<input type="hidden" id="up_proj_floor" value="<?php echo $proj_floor ?>"  name="up_proj_floor">
							</div>
                            <?php if(!empty($proj_floor)){ ?>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($proj_floor)){ echo SURL.'assets/images/property_broucher/'.$proj_floor ;}?>" <?php if(!empty($proj_floor)){echo 'target="_blank"';}?> class="btn btn-primary" title="View Project Floor Plan">Floor Plan</a>
							</div>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($proj_floor)){ echo SURL.'our_projects/remove/'.$id.'/proj_floor' ;}?>" class="btn btn-primary" title="Remove Floor Plan"><i class="fa fa-window-close"></i></a>
							</div>
                                                  <?php } ?>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Upload Payment Plan</label>
							<div class="col-sm-3">
								<input type="file"  name="payment" id="payment" />
								<input type="hidden" id="up_payment" value="<?php echo $payment_plan ?>"  name="up_payment">
							</div>
                            <?php if(!empty($payment_plan)){ ?>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($payment_plan)){ echo SURL.'assets/images/property_broucher/'.$payment_plan ;}?>" <?php if(!empty($payment_plan)){echo 'target="_blank"';}?> class="btn btn-primary" title="View Payment Plan">Payment</a>
							</div>
							<div class="col-sm-1">
								<a role="button" href="<?php if(!empty($payment_plan)){ echo SURL.'our_projects/remove/'.$id.'/payment_plan' ;}?>"  class="btn btn-primary" title="Remove Payment Plan"><i class="fa fa-window-close"></i></a>
							</div>

                                                  <?php } ?>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-3">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/projects/".$image." width='100'>";}?>
							</div>
						</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Page Layout</label>
						<div class="col-sm-2">
							<select id="layout" name="layout" class="form-control">
								<option value="listing_1" <?php if($page_layout == 'listing_1'){echo 'selected';}?> >Layout 1</option>
								<option value="listing_2" <?php if($page_layout == 'listing_2'){echo 'selected';}?>>Layout 2</option>
							</select>
						</div>
						<div class="col-sm-3">
							<img id="layout_img" src="<?php if(!empty($layout_image)){echo IMG.$layout_image ;}else{ echo IMG.'listing-01.png' ;}?>" width='100%'>
							<input value="<?php if(!empty($layout_image)){echo $layout_image ;}else{ echo 'listing-01.png' ;}?>" type="hidden" name="layo_img" id="lay_img">
						</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>our_projects" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
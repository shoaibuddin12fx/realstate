 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Projects Management</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Projects Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'our_projects/add_project'?>">
						<i class="fa fa-plus txt-success" title="Add Projects"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							
							<th>Image</th>
                                                        <th>Broucher</th>
							<th>Title</th>
							<th>Address</th>
							<th>Project Category</th>
							<th>Is Feature</th>
							<th>Display Order</th>
							<th>Assignee Agent</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach ($projects as $key => $value) {
					extract($value);

					?>
						<tr>
							
							<td> <?php if(!empty($image)){ ?><img src="<?php echo SURL.'assets/images/projects/'.$image ?>">  <?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?> </td>
							<td><?php if(!empty($proj_broucher)){?><a role="button" href="<?php echo SURL.'assets/images/property_broucher/'.$proj_broucher?>" target="_blank" class="btn btn-primary" title="View Broucher">Broucher</a><?php } ?><br><?php if(!empty($proj_floor)){?><a role="button" href="<?php echo SURL.'assets/images/property_broucher/'.$proj_floor?>" target="_blank" class="btn btn-primary" title="View Floor Plan">Floor Plan</a><?php } ?><br><?php if(!empty($payment_plan)){?><a role="button" href="<?php echo SURL.'assets/images/property_broucher/'.$payment_plan?>" target="_blank" class="btn btn-primary" title="View Payment Plan">Payment Plan</a><?php } ?></td>
							<td><?php echo $title ?></td>
							<td><?php echo $project_address ?></td>
							<td><?php echo $cat_title ?></td>
							<td><?php if($feature=='yes'){ echo "Yes" ;}else{ echo "No";}?></td>
							<td><?php echo $display_order ?></td>
							<td><a role="button" href="<?php echo SURL.'our_projects/assigne_agent/'.$id?>" class="btn btn-primary" title="Assigne Agents">Agents</a></td>
							<td><?php if($proj_status == 'true'){echo 'Activated';}else{echo 'Deactivated';} ?></td>
							
							<td>

                          <?php if($proj_status == "true"){?>
                                <a href="<?php echo SURL.'our_projects/deactivate_project/'.$id?>" title="Deactivate"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            <?php }else{?>
                                <a href="<?php echo SURL.'our_projects/activate_project/'.$id?>" title="Activate"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                            <?php }?>

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='our_projects/edit_project'){

								echo'<a href="'.SURL.'our_projects/edit_project/'.$id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='our_projects/del_project'){?>

								<a href="<?php echo SURL.'our_projects/del_project/'.$id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
									
							</td>
						</tr>
					<?php } ?>	
					<!-- End: list_row -->
					</tbody>
					
				</table>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Add Project</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Project</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'our_projects/insert_project'?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="title" id="title" />
							</div>
						</div>
                                                <div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="address" id="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Company Name</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="company_name" id="company_name" />
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Short Description</label>
			 				<div class="col-sm-5">
								<textarea class="form-control" rows="4" name="short_description" id="short_description" ></textarea>
							</div>
					</div>

					<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea class="form-control" rows="8" name="description" id="description" ></textarea>
							</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Display Order</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="display_order" id="display_order" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">One line Description</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="one_line" id="one_line" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Delivery Date</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="delivery_date" id="delivery_date" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Youtube Video Embended Code</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('video')" class="form-control" name="video" id="video" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Location</label>
							<div class="col-sm-5">
								<textarea type="text" rows="5" onchange="replace_iframe('location')" class="form-control" name="location" id="location" ></textarea>
							</div>
						</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Is Feature</label>
							<div class="col-sm-5">
								<select id="feature" name="feature" class="form-control">
									<option>--SELECT--</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select id="status" name="status" class="form-control">
									<option>--SELECT--</option>
									<option value="true">Active</option>
									<option value="false">Deactive</option>
								</select>
							</div>
					</div>
					<div class="form-group">
							<label class="col-sm-3 control-label">Builder</label>
							<div class="col-sm-5">
								<select id="category_id" name="category_id" class="form-control">
									<option>--Select Category--</option>
									<?php foreach($categories as $key => $value){
									?>
									<option value='<?=$value["cat_id"]?>'><?=$value['cat_title']?></option>
									<?php } ?>
								</select>
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Upload Broucher</label>
							<div class="col-sm-5">
								<input type="file" name="broucher" id="broucher" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Project Floor Plan</label>
							<div class="col-sm-5">
								<input type="file" name="proj_floor" id="proj_floor" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Upload Payment Plan</label>
							<div class="col-sm-5">
								<input type="file" name="payment" id="payment" />
							</div>
					</div>
                    <div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-5">
								<input type="hidden" name="status" value="true">
								<input type="file" name="image" id="image" />
							</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Page Layout</label>
						<div class="col-sm-2">
							<select id="layout" name="layout" class="form-control">
								<option value="listing_1">Layout 1</option>
								<option value="listing_2">Layout 2</option>
							</select>
						</div>
						<div class="col-sm-3">
							<img id="layout_img" src="<?=IMG.'listing-01.png'?>" width='100%'>
							<input value="Listing-01.png" type="hidden" name="layo_img" id="lay_img">
						</div>
					</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" value="create" class="btn btn-primary">Add Project</button>
							<a role="button" href="<?=SURL?>our_projects" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

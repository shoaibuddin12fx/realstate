 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a >Update Project Floor Plan Gallery</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
				<span><b>Update Project Floor Plan Gallery</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<?php extract($floor_project_gallery[0]); ?>
				<form id="defaultForm" method="post" action="<?php echo SURL.'floor_project_gallery/update_gallery/'.$id?>" class="form-horizontal" enctype="multipart/form-data">
					<fieldset>

					<input type="hidden" id="project_id" name="project_id" value="<?php echo $project_id;?>">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>                         
                        </div>
                    </div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Project Category</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="category_id" id="category_id">
									<option value="">-- Select a category --</option>
									<?php foreach ($categories as $key => $value) {
									?>
									<option value='<?=$value["cat_id"]?>' <?php if($value['cat_id'] == $project_cat){echo 'selected';}?> ><?=$value['cat_title']?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Projects</label>
							<div class="col-sm-5">
								<select id="project_id" name="project_id" class="form-control">
									<option>--Select Category--</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Gallery Images</label>
							<div class="col-sm-3">
								<input type="file"  name="gallery[]" multiple="multiple" id="gallery" />
								<input type="hidden" id="udate_gallery" value="<?php echo $gal_images ?>"  name="udate_gallery">
							</div>
							<div class="col-sm-6">
								<div class="row">
							<?php
								$gallry_images=explode(' , ',$gal_images);
								
							 for($i=0;$i<count($gallry_images)-1;$i++){
								?>
								<div class="col-md-3">
								<?php if(!empty($gallry_images[$i])){echo "<img src='".SURL."assets/images/floor_project_gallery/".$gallry_images[$i]."' width='100'>";}?>
								<a role="button" class="btn btn-primary" href='<?php echo SURL."floor_project_gallery/remove_img/".$floor_project_gallery[0]['id']."/".$gallry_images[$i]?>'>Remove</a>
							   </div>
							<?php } ?>
							   
							   </div>	
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>floor_project_gallery" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
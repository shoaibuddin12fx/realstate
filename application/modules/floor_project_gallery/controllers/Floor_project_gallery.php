<?php
class Floor_Project_gallery extends MY_Controller
{
    function __construct(){
        parent::__construct();
    }

    function index()
    {
        
        $data['gallery']=$this->Crud->join_on_tbl('floor_project_gallery','our_projects','floor_project_gallery.project_id=our_projects.id','*,floor_project_gallery.id as gal_id','project_categories','project_categories.cat_id=our_projects.category_id','');
        $data['content']='floor_project_gallery/all_gallries';
        $this->template->admin_template($data);
    }
    
    function add_gallery()
    {

        $data['categories']=$this->Crud->getRows('project_categories','*',array("status"=>"true"));
        $data['content']='floor_project_gallery/add_gallery';
        $this->template->admin_template($data);
    }
    function projects($category_id){
        $data=$this->Crud->getRows("our_projects","*",array("category_id"=>$category_id,"status"=>"true"));
        echo json_encode($data);
    }
    function insert_gallery(){
        $table = 'floor_project_gallery';
    if($this->input->post('create')){
            extract($this->input->post());
        if($category_id=='' || $project_id==''){
        $this->session->set_flashdata('message','<h5 class="alert alert-danger">Builder and Project are required</h5>');
        redirect(SURL.'floor_project_gallery/add_gallery');
        }
        $filename_g='';
                $count=count($_FILES["gallery"]['tmp_name']);
                if($_FILES["gallery"]['tmp_name'][0]==''){
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please Select Atleast One Image For Gallery Images</h5>');
                          redirect(SURL.'floor_project_gallery/add_gallery');
                }
                else{
                $reslt="yes";
                for ($i=0; $i < $count; $i++) { 

                     list($width, $height) = getimagesize($_FILES["gallery"]['tmp_name'][$i]);
                     if($width < "500" || $height < "300") {
                        
                        $reslt="no";

                    }
                }
                     if($reslt=="no"){
                        $this->session->set_flashdata('message','One off your image size is more then 500 * 300 pixels please upload image with equal to or less 500 * 300 ');
                          redirect(SURL.'floor_project_gallery/add_gallery/');

                     }else{ 
                     $files=$_FILES;
                     for ($i=0; $i < $count ; $i++) {   
                          
                        $_FILES["gallery"]['tmp_name']=$files["gallery"]['tmp_name'][$i];
                        $_FILES["gallery"]['name']=$files["gallery"]['name'][$i];
                        $_FILES["gallery"]['type']=$files["gallery"]['type'][$i];
                        $_FILES["gallery"]['error']=$files["gallery"]['error'][$i];
                        $_FILES["gallery"]['size']=$files["gallery"]['size'][$i];
                        
                        $projects_folder_path = './assets/images/floor_project_gallery/';
                        $projects_folder_path_main = './assets/images/floor_project_gallery/';
                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                       // $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;

                       // $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('gallery')) {
                        $this->session->set_flashdata('message','<h5 class="alert alert-danger">One of your file is not the right type of file to upload</h5>');
                          redirect(SURL.'floor_project_gallery/add_gallery');
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_g.= $data_image_upload['upload_image_data']['file_name'].' , ';
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_g, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
}
            $data = array(
                "project_cat" => $category_id,
                "project_id" => $project_id,
                "gal_images" => $filename_g,
                
            );


            $inserted= $this->Crud->insert($table,$data);
            
            if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Gallery is added successfully</h5>');
                redirect(SURL.'floor_project_gallery');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add gallery please try again</h5>');
                redirect(SURL.'floor_project_gallery/add_gallery');
            }
        }   
    }


    function edit_gallery($id){
        $table = 'floor_project_gallery';
        $id = $id;
        $where = array('id' => $id );        
        $data['categories']=$this->Crud->getRows('project_categories','*',array("status"=>"true"));
        $data['floor_project_gallery'] = $this->Crud->getRows($table,'*',$where);
        $data['content'] = 'floor_project_gallery/edit_gallery';
        $this->template->admin_template($data);
    }

    function update_gallery($id){
        $table = 'floor_project_gallery';
        $id = $id;
        $where = array('id' =>  $id);
        if($this->input->post('update_btn')){
            extract($this->input->post());

            $filename_g= $udate_gallery ;                 
                
                if($_FILES['gallery']['name'][0]!=''){
                $count=count($_FILES["gallery"]['tmp_name']);
                
                $reslt="yes";
                for ($i=0; $i < $count; $i++) { 

                     list($width, $height) = getimagesize($_FILES["gallery"]['tmp_name'][$i]);
                     if($width < "500" || $height < "300") {
                        
                        $reslt="no";

                     }
                 }
                     if($reslt=="no"){
                        $this->session->set_flashdata('message','One off your image size is more then 500 * 300 pixels please upload image with equal to or less 500 * 300 ');
                          redirect(SURL.'floor_project_gallery/add_gallery/');

                     }else{ 
                     $files=$_FILES;
                     for ($i=0; $i < $count ; $i++) {   
                          
                        $_FILES["gallery"]['tmp_name']=$files["gallery"]['tmp_name'][$i];
                        $_FILES["gallery"]['name']=$files["gallery"]['name'][$i];
                        $_FILES["gallery"]['type']=$files["gallery"]['type'][$i];
                        $_FILES["gallery"]['error']=$files["gallery"]['error'][$i];
                        $_FILES["gallery"]['size']=$files["gallery"]['size'][$i];
                        
                        $projects_folder_path = './assets/images/floor_project_gallery/';
                        $projects_folder_path_main = './assets/images/floor_project_gallery/';
                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                       // $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;

                       // $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('gallery')) {
                        $this->session->set_flashdata('message','<h5 class="alert alert-danger">One of your file is not the right type of file to upload</h5>');
                          redirect(SURL.'floor_project_galleryj/edit_gallery/'.$id);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_g.= $data_image_upload['upload_image_data']['file_name'].' , ';
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_g, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    
                }
            }
        }

            $data = array(
                "project_cat" => $category_id,
                "project_id" => $project_id,
                "gal_images" => $filename_g,
            );
        }
            $updated = $this->Crud->update($table,$data,$where);
            if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Gallery is updated successfully</h5>');
                redirect(SURL.'floor_project_gallery');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update gallery please try again</h5>');
                redirect(SURL.'floor_project_gallery/edit_gallery/'.$id);
            }
        

    }

    function del_row($id){
            $table = 'floor_project_gallery';
            $id= $id;
            $where = array("id"=>$id);
            $del=$this->Crud->delete($table,$where);
            
            if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Gallery is deleted successfully</h5>');
                redirect(SURL.'floor_project_gallery');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete gallery please try again</h5>');
                redirect(SURL.'floor_project_gallery');
            }
    }
    function remove_img($id,$image){
        $table='floor_project_gallery';
        $where=array("id"=>$id);
        $img=$image;
        $images='';
        $data['prop_data']=$this->Crud->getRows($table,'*',$where);
        $gallry_images=explode(' , ',$data['prop_data'][0]['gal_images']);
        $c='';
        for($i=0;$i<count($gallry_images)-1;$i++){
            if($img != $gallry_images[$i]){
                $images.=$gallry_images[$i].' , ';
                $c++;
            }
        }
        $data=array("gal_images"=>$images);
        $updated = $this->Crud->update($table,$data,$where);
            if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Image is removed successfully</h5>');
                redirect(SURL.'floor_project_gallery/edit_gallery/'.$id);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to remove image please try again</h5>');
                redirect(SURL.'floor_project_gallery/edit_gallery/'.$id);
            }

    }
}
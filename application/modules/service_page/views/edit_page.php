<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Service Page Management</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add Service Page Detail</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<?php
			if(!empty($detail)){ 
				extract($detail[0]);
				$title=$title;
				$heading=$heading;
				$keywords=$keywords;
				$content=$content;
				$description=$description;
				// $image=$image;
			}else{
				$title='';
				$heading='';
				$keywords='';
				$content='';
				$description='';
				// $image='';
			} ?>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL."about_us/add_page/" ?>" class="form-horizontal" enctype="multipart/form-data">
					 <div class="form-group">
                        <div class="col-sm-12">
                    <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
                        
                        </div>
                    </div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Page Title</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $title ; ?>" name="title" id="title" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Page Heading</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo $heading ; ?>" name="heading" id="heading" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Key Words</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" id="keywords" name="keywords" value="<?php echo $keywords ; ?>" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea type="text" class="form-control" value="" name="description"><?php echo $description ; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Slider Image</label>
							<div class="col-sm-2">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php if(!empty($image)){echo $image;} ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/sliders/".$image." width='100'>";}?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Page Content</label>
							<div class="col-sm-5">
								<textarea type="text" class="form-control" id="editor1" name="editor1" /><?php echo $content ; ?></textarea>
							</div>
						</div>
				        <div class="form-group">
				             <div class="col-sm-9 col-sm-offset-3">
					         <button type="submit" class="btn btn-primary" id="update_page" name="update_page" value="update_page">Save Detail</button>
				        </div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}

// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	LoadTimePickerScript(AllTimePickers);
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

  <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Users</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span><b>Users Management</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'users_management/create_user'?>">
						<i class="fa fa-plus txt-success " title="Add User"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content no-padding">
                    <div class="form-group">
                        <div class="col-sm-12">
                    <?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
                        
                        </div>
                    </div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach($users as $key => $value){
						extract($value);
						?>
						<tr>
							
							<td> <?php echo $name ?> </td>
							<td> <?php echo $email ?> </td>
							
							
							<td> <?php echo $title ?> </td>
							<td> <?php if($user_status == 'true'){echo 'Activated';}else{echo 'Deactivated';} ?> </td>
							
							<td>
<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='users_management/edit_user'){

								echo'<a href="'.SURL.'users_management/edit_user/'.$user_id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='users_management/del_row'){?>

								<a href="<?php echo SURL.'users_management/del_row/'.$user_id ?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
							 <a href="<?php echo SURL."users_management/user_detail/".$user_id?>"><i title="View" class="fa fa-eye"></i></a></td>
						</tr>
						
					<?php } ?>
					<!-- End: list_row -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

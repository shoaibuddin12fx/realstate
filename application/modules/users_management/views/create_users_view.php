 <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Add User</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Add User</b></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'users_management/insert_user'?>" enctype="multipart/form-data" class="form-horizontal">
					
						<div class="form-group">
							<label class="col-sm-3 control-label">Name</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="username" id="name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="email" id="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mobile number</label>
							<div class="col-sm-5">
								<input type="number" class="form-control" name="phoneNumber" id="phone" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="address" id="address" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Password</label>
							<div class="col-sm-5">
								<input type="password" class="form-control" name="password" id="password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Retype password</label>
							<div class="col-sm-5">
								<input type="password" class="form-control" name="confirmPassword" id="" />
							</div>
						</div>
					
						
					
						<div class="form-group">
							<label class="col-sm-3 control-label">Role</label>
							<div class="col-sm-5">
								<input type="hidden" name="status" value="true" >
								<select class="populate placeholder form-control" name="role" id="role">
									<option value="">-- Select a status --</option>
                                    <?php foreach ($roles as $key => $value) {
									?>
			<option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
									
									<?php } ?>
								</select>
							</div>
						</div>
						
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="create" id="create" value="creat" class="btn btn-primary">Add</button>
							<a role="button" href="<?=SURL?>users_management" class="btn btn-primary">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>
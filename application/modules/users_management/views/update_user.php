<?php 
$rolesss='';
 foreach ($role as $key => $val) {
									
			if($list[0]['role_id']==$val['id']){$select='selected';}else{$select='';}
			$rolesss.='<option value="'.$val['id'].'" '.$select.'>'.$val['title'].'</option>';
									
									}

extract($list[0]); ?>
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Update User</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Edit user information</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'users_management/create_user'?>">
						<i class="fa fa-plus txt-success " title="Add New User"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'users_management/update_user/'.$user_id?>" enctype="multipart/form-data" class="form-horizontal">
					
						<div class="form-group">
							<label class="col-sm-3 control-label">Name</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $name ?>" class="form-control" name="username" id="name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $email ?>" class="form-control" name="email" id="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mobile number</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $num?>" class="form-control" name="phoneNumber" id="phone" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Address</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $address ?>" class="form-control" name="address" id="address" />
							</div>
						</div>
						
								<input type="hidden" id="udate_image" value="<?php echo $image ?>" name="udate_image" >
							
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($user_status == 'true'){echo 'selected';} ?> >Activate</option>
									<option value="false" <?php if($user_status == 'false'){echo 'selected';} ?> >Deactivate</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Role</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="role" id="role">
									<option value="">-- Select a Role --</option>
						<?php
echo $rolesss;
						 ?>
								</select>
							</div>
						</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" id="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>users_management" class="btn btn-primary">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

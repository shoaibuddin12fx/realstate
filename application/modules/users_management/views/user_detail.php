<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>User Detail</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span ><b>User Detail</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'users_management/create_user'?>">
						<i class="fa fa-plus txt-success " title="Add New User"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content ">
					<form class="form-horizontal"> <?php extract($list[0]) ?>
					<fieldset>
						
						<div class="form-group">
							<label class="col-sm-2 ">Name:</label>
							<div class="col-sm-5">
								<?=$name?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">Email:</label>
							<div class="col-sm-5">
								<?=$email?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">Phone:</label>
							<div class="col-sm-5">
								<?=$num?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">Status:</label>
							<div class="col-sm-5">
								<?php if($user_status=='true'){echo "Activated";}else{echo "Deactivated";}?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">Address:</label>
							<div class="col-sm-5">
								<?=$address?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">Role</label>
							<div class="col-sm-5">
								<?=$title?>
							</div>
						</div>
					</fieldset>
				</form>

               <a href="<?php echo SURL."users_management/"?>">  <button  type="submit" name="update" id="update" value="update" class="btn btn-primary">Go Back</button></a>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

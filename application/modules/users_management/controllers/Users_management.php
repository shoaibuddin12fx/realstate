 <?php
class Users_management extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$data['users']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status');
		$data['content']='users_management/users_view';
		$this->template->admin_template($data);
	}

	function create_user()
	{        
        $data['roles']=$this->Crud->getRows('roles');
		$data['content']='users_management/create_users_view';
		$this->template->admin_template($data);
	}
	function user_detail($id){
		$id=$id;
		$table='users';
		$where=array("users.id" =>$id);
		$data['list']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status',$where);
        $data['content']='users_management/user_detail';
		$this->template->admin_template($data);
	}


	function insert_user(){
		$table = 'users';
		$localIP = getHostByName(getHostName());
		 if($this->input->post('create')){
		 	extract($this->input->post());
		 	$created_date = date('Y-m-d H:i:s');


			$password=$this->bcrypt->hash($password);

		 	$data = array(
		 		"image"=>'',
		 		"name"=>$username,
		 		"email"=>$email,
		 		"password"=>$password,
		 		"num"=>$phoneNumber,
		 		"role_id"=>$role,
		 		"ip"=>$localIP,
		 		"status"=>$status,
		 		"address"=>$address,
		 		"created_at"=>$created_date,	
		 	);
		 	$inserted = $this->Crud->insert($table,$data);
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">User is added successfully</h5>');
                        $data['roles']=$this->Crud->getRows('roles');
		$data['content']='users_management/create_users_view';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add user please try again</h5>');
                        $data['roles']=$this->Crud->getRows('roles');
		$data['content']='users_management/create_users_view';
		$this->template->admin_template($data);
            }
		 }
	}


	function edit_user($id){
		$table = 'users';
		$id = $id;
        $data['role'] = $this->Crud->getRows('roles');
		$where=array("users.id" =>$id);
		$data['list']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status',$where);
		$data['content'] = 'users_management/update_user';
		$this->template->admin_template($data);
	}


	function update_user($id){
		$table = 'users';
		$id = $id;
		$where = array("id"=> $id);
		if($this->input->post('update_btn')){

			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');


			$data = array(
				"image"=>'',
				"name"=>$username,
		 		"email"=>$email,
		 		"num"=>$phoneNumber,
		 		"updated_at"=>$updated_at,
		 		"role_id"=>$role,
		 		"status"=>$status,
		 		"address"=>$address,
		 	);
		 	$updated = $this->Crud->update($table,$data,$where);
		 
		 	if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">User is updated successfully</h5>');
		$data['users']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status');
		$data['content']='users_management/users_view';
		$this->template->admin_template($data);
            }else{
		$data['list']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status',$where);
		$data['content'] = 'users_management/update_user';
		$this->template->admin_template($data);
            }
		}
	}
	function del_row($id){
			$table = 'users';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">User is deleted successfully</h5>');
		$data['users']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status');
		$data['content']='users_management/users_view';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete user please try again</h5>');
		$data['users']=$this->Crud->join_on_tables('users','roles','users.role_id=roles.id','*,users.id as user_id,users.status as user_status');
		$data['content']='users_management/users_view';
		$this->template->admin_template($data);
            }
	}
}
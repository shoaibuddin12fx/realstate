<?php
class Template extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Crud');
        $this->load->model('File_upload');

	}
	function admin_template($data=NULL)
	{   
		$data['menu']=$this->Crud->getmenu();
		$this->load->view('template/admin_template',$data);
	}
	function realestate_template($data=NULL)
	{   
		$data['footer_data']=$this->Crud->getRows('site_settings','*','','id','desc');
		$data['properties']=$this->Crud->getR('properties','*','','prop_id','desc','',3);
		$data['partners']=$this->Crud->getR('our_partners','*',array("status"=>"true"),'order','asc','',9);
		$this->load->view('template/realestate',$data);
	}
	function about_us_layout($data=NULL){

		$data['footer_data']=$this->Crud->getRows('site_settings','*','','id','desc');
		$data['properties']=$this->Crud->getR('properties','*','','prop_id','desc','',3);
		$data['partners']=$this->Crud->getR('our_partners','*',array("status"=>"true"),'order','asc','',9);
		$this->load->view('template/about_layout',$data);
	}
	function contact_us_layout($data=NULL){

		$data['footer_data']=$this->Crud->getRows('site_settings','*','','id','desc');
		$data['properties']=$this->Crud->getR('properties','*','','prop_id','desc','',3);
		$data['partners']=$this->Crud->getR('our_partners','*',array("status"=>"true"),'order','asc','',9);
		$this->load->view('template/contact_us_layout',$data);
	}
	function listing_page_layout($data=NULL){
		$data['footer_data']=$this->Crud->getRows('site_settings','*','','id','desc');
		$data['properties']=$this->Crud->getR('properties','*','','prop_id','desc','',3);
		$data['partners']=$this->Crud->getR('our_partners','*',array("status"=>"true"),'order','asc','',9);
		$this->load->view('template/nl_listing_layout',$data);
	}
		function property_listing_detail($data=NULL){
		$data['footer_data']=$this->Crud->getRows('site_settings','*','','id','desc');
		$data['properties']=$this->Crud->getR('properties','*','','prop_id','desc','',3);
		$data['partners']=$this->Crud->getR('our_partners','*',array("status"=>"true"),'order','asc','',9);
		$this->load->view('template/property_listing_detail',$data);
	}

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crud extends CI_Model{
    /*
     * Get posts
     */
    /*
     * Get data
     */
     function getRows($table,$field='*',$condition = array(),$orderby='',$sort='', $wherein=array()){
        $this->db->select($field);

        if(!empty($wherein)){
            $this->db->where_in('id',$wherein);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }
    function getR($table,$field='*',$condition = array(),$orderby='',$sort='', $wherein=array(),$limit){
        $this->db->select($field);

        if(!empty($wherein)){
            $this->db->where_in('id',$wherein);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }
    
    /*
     * Insert data
     */
    public function insert($table,$data = array()) {
        $insert = $this->db->insert($table, $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    /*
     * Update data
     */
    public function update($table,$data, $condition=array()) {
        if(!empty($data) && !empty($condition)){
            $update = $this->db->update($table, $data, $condition);
            return $update?true:false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete data
     */
    public function delete($table,$condition= array()){
        $delete = $this->db->delete($table,$condition);
        return $delete?true:false;
    }

    public function getmenu(){
        $controler=$this->uri->segment(1);
        $function=$this->uri->segment(2);
        $access=explode(";",$this->session->userdata('access'));

        $conditions= array('status' =>'true' , 'parent'=>'0');
        $modules=$this->getRows('modules','*',$conditions,'displayorder','ASC');  

        $data=''; 
        if(!empty($modules)){

            foreach ($modules as $key => $value) {
                $sub_modules_cond= array('status' =>'true' , 'parent ='=>$value['id'],'show_in_menu' =>'true' );
                $sub_modules=$this->getRows('modules','*',$sub_modules_cond,'displayorder','ASC',$access);
                //print_r($this->db->last_query());exit;
                if(count($sub_modules)!=0){
                    $liclass='class="dropdown"';
                    $aclass="dropdown-toggle ";
                    $sub_menu="<ul class='dropdown-menu ' ";
                    $links=explode('/',$sub_modules[0]['slug']);
                    if($controler==$links[0])
                    {
                        $sub_menu.=' style="display:block"'; 
                        $aactive=' active';
                    }
                    else
                    {
                        $sub_menu.='';
                        $aactive='';
                    } 
                    $sub_menu.=">";
                    $sub_menu_end='</ul>';
                    $url=$value['slug'];

                }else{
                    $aactive='';
                    $liclass="";
                    $url=SURL.$value['slug'];
                    $aclass="";
                    $sub_menu="";
                    $sub_menu_end='';


                }
                 if(count($sub_modules)!=0){
                $data.= "<li $liclass>
                    <a href='".$url."' class='".$aclass.$aactive ;
                    $data.= "'>
                        <i class='fa fa-".$value['icon']."'></i>
                        <span class='hidden-xs'>".$value['title']."</span>
                    </a>";

                    $data.=$sub_menu;

                    foreach ($sub_modules as $list => $item) {
                        $linkss=explode('/',$item['slug']);
                        $data.= '<li><a  href="'.SURL.$item['slug'].'"  class="';
                        if(array_key_exists(1,$linkss)){
                            if($controler==$linkss[0] && $function==$linkss[1]){$data.='active-parent active'; } 
                        }else{
                            if($controler==$linkss[0] && !isset($function)){$data.='active-parent active'; } 
                        }
                       
                        $data.= '">'.$item['title'].'</a></li>';
                    }
                $data.= $sub_menu_end;
                $data.= "</li>";
            }

            }
        }
        return $data;
    }

    public function join_on_tables($table,$table2,$join,$field='*',$condition = array(),$orderby='',$sort='',$limit=''){
        $this->db->select($field);

        if(!empty($table)){
            $this->db->join($table2,$join);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }
        public function join_tables($table,$table2,$join,$field='*',$condition = array(),$orderby='',$sort='',$limit='',$start='',$wherein=array(),$where_column=''){

        $this->db->select($field);

        if(!empty($wherein)){
            $this->db->where_in($where_column,$wherein);
        }
        if(!empty($table)){
            $this->db->join($table2,$join);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($limit)){
            $this->db->limit($limit,$start);
        }

        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }


    public function join_on_tbl($table,$table2,$join,$field='*',$table3, $join2 ,$condition = array(),$orderby='',$sort='',$group_by=''){
        $this->db->select($field);

        if(!empty($table2)){
            $this->db->join($table2,$join);
        }
        if(!empty($table3)){
            $this->db->join($table3,$join2);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by,$sort);
        }

        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }
    public function join_on_4tbl($table,$table2,$join,$field='*',$table3, $join2,$table4 ,$join3 ,$condition = array(),$orderby='',$sort='',$group_by=''){
        $this->db->select($field);

        if(!empty($table2)){
            $this->db->join($table2,$join);
        }
        if(!empty($table3)){
            $this->db->join($table3,$join2);
        }
        if(!empty($table4)){
            $this->db->join($table4,$join3);
        }

        if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by,$sort);
        }

        if(!empty($condition)){
            $query = $this->db->get_where($table, $condition);
            return $query->result_array();
        }else{
            $query = $this->db->get($table);
            return $query->result_array();
        }
    }

 function create_optimize($filename, $src, $dest) {
    $ci = & get_instance();

    //resize:
    $config_resize['image_library'] = 'gd2';
//     $config_resize['library_path'] = '/usr/bin/composite';
    $config_resize['source_image'] = $src;
    $config_resize['quality'] = 80;
    $config_resize['new_image'] = $dest;
    $config_resize['maintain_ratio'] = TRUE;

    $ci->load->library('image_lib');
    $ci->image_lib->initialize($config_resize);
    $ci->image_lib->resize();

    $ci->image_lib->clear();
}
    function create_thumbnail($filename, $src, $dest, $width = 150, $height = 150) {
        $ci = & get_instance();
    //
    //    //resize:
        // echo $src."<br>";
        $config_resize['image_library'] = 'gd2';
    //    $config_resize['library_path'] = '/usr/bin/mogrify';
        $config_resize['source_image'] = $src;
        $config_resize['quality'] = 80;
        $config_resize['new_image'] = $dest;
    //  $config_resize['file_name'] = $filename;
        $config_resize['maintain_ratio'] = FALSE;
        $config_resize['create_thumb'] = FALSE;
        $config_resize['width'] = $width;
        $config_resize['height'] = $height;
    //
        // echo $src;
        $data = getimagesize($src);
        $width1 = $data[0];
        $height1 = $data[1];
        if ($width1 != $height1) {

            $config_resize['maintain_ratio'] = TRUE;
            $config_resize['master_dim'] = 'auto';
            $ci->load->library('image_lib');
            $ci->image_lib->initialize($config_resize);
            if ($ci->image_lib->resize()) {

                $dfile = $dest . "/" . $filename;

                $im = imagecreatetruecolor($width, $height);
    //        $stamp = imagecreatefromjpeg('img.jpg');

                if (preg_match('/[.](jpg)$/', $filename)) {
                    $stamp = imagecreatefromjpeg($dfile);
                } else if (preg_match('/[.](gif)$/', $filename)) {
                    $stamp = imagecreatefromgif($dfile);
                } else if (preg_match('/[.](png)$/', $filename)) {
                    $stamp = imagecreatefrompng($dfile);
                } else if (preg_match('/[.](jpeg)$/', $filename)) {

                    $stamp = imagecreatefromjpeg($dfile);
                } else if (preg_match('/[.](JPG)$/', $filename)) {

                    $stamp = imagecreatefromjpeg($dfile);
                }


                $red = imagecolorallocate($im, 255, 255, 255);
                imagefill($im, 0, 0, $red);



                $sx = imagesx($stamp);
                $sy = imagesy($stamp);


                $oy = imagesx($stamp);
                $ox = imagesy($stamp);


                $d = getimagesize($dfile);
                $wd = $d[0];
                $hg = $d[1];

                if ($wd < $width) {
                    $mg = $width - $wd;
                    $marge_right = $mg / 2;
                } else {
                    $marge_right = 0;
                }
                if ($hg < $height) {

                    $mgh = $height - $hg;
                    $marge_bottom = $mgh / 2;
                } else {
                    $marge_bottom = 0;
                }
                $imgg = imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, $sx, $sy);

    // echo "resized ".$src;
                header('Content-type: image/png');
                imagejpeg($im, $dfile);
                imagepng($im, $dfile);
                imagedestroy($im);
            } else {

                // echo $ci->image_lib->display_errors();
            }
        } else {

    // echo "resized ".$src;
            $ci->load->library('image_lib');
            $ci->image_lib->initialize($config_resize);
            $ci->image_lib->resize();
        }

    //      $ci->image_lib->fit();
    //
    //
    //
    //    $ci->image_lib->clear();
    }

     function checkSubscribeEmail($email){
        
       
                
        $query="select * from subscribers where email='".$email."'";
        $query=$this->db->query($query);
        $result=$query->result_array();
        return $result;
        }
    function sorted_properties($limit='', $start='',$sort='')
    {
        if($sort['soort_by']=="date_desc"){
            $orderby='created_at';
            $sort='desc';
        }
        else if($sort['soort_by']=="date_asc"){
            $orderby='created_at';
            $sort='asc';
        }
        else if($sort['soort_by']=="price_desc"){
            $orderby='price';
            $sort='desc';
        }
        else if($sort['soort_by']=="price_asc"){
            $orderby='price';
            $sort='asc';
        }
        else{
            $orderby='';
            $sort='';
        }

        $this->db->limit($limit, $start);
        $this->db->select('*');
        $this->db->from('properties');
        $this->db->where(array('status'=>'true'));
        if(!empty($orderby)){
                $this->db->order_by($orderby,$sort);
            }
        $q = $this->db->get();
$proptz='';
        if($q->num_rows()>0)
        {

            foreach($q->result() as $row)
              {
                $dat=$row->created_at;
         $proptz.= '<div class="property">
                        <hr>
                        <div class="row">
                         <div class="well">

                          <div class="media">
                            <a class="pull-left" href="'.SURL.'nl_listing/property_detail/'.$row->prop_id.'">
                              <img class="media-object" src="'.IMG.'properties/'.$row->image.'">
                            </a>
                            <div class="media-body property-detail">
                            <a class="pull-left act_a" href="'.SURL.'nl_listing/property_detail/'.$row->prop_id.'">
                              <h4 class="media-heading">'.$row->prop_title.'</h4>
                              <p> <span>By </span>next level realestate<span>  |  '.date('dS,F,Y' ,strtotime($dat)).'</span></p>
                              <span class="span2">AED '.$row->price.'</span>
                              <hr>
                              <ul class="list-inline list-unstyled">
                                <li><span><i class="fa fa-arrows"></i></span>'.$row->diemensions.'</li>
                                <li><span><i class="fa fa-shower"></i></span>'.$row->washrooms.'</li>
                                <li><span><i class="fa fa-bed"></i></span>'.$row->bedrooms.'</li>
                                <li><span><i class="fa fa-building"></i>Property Type:</span>'.$row->property_type.' 
                                     <span class="i-spn"><i class="fa fa-home"></i>New Homes:</span>'.$row->home_type.'</li>
                                <li><span><i class="fa fa-map-marker"></i></span>'.$row->address.'</li>
                              </ul></a>
                            </div>
                          </div>
                        </div>
                        </div>
                      </div>';   
              }

            return $proptz;
        }
        else 
        {
            return false;
        }
    }
    function get_all_properties($limit='', $start='',$orderby='',$sort='')
    {
    $this->db->limit($limit, $start);
    $this->db->select('*');
    $this->db->from('properties');
    $this->db->where(array('status'=>'true'));
    if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
        }
    $q = $this->db->get();
    if($q->num_rows()>0)
    {
        return $q->result();
    }
    else 
    {
        return false;
    }
}
    function getProperties($limit, $start,$search_data){

        $prop_title=$search_data['prop_title'];
        $prop_for=$search_data['prop_for'];
        $prop_type=$search_data['property_type'];
$city=$search_data['city'];
        $location=$search_data['address'];
        $min_price=$search_data['min_price'];
        $max_price=$search_data['max_price'];
        $min_bedroom=$search_data['min_bedroom'];
        $max_bedroom=$search_data['max_bedroom'];
        $min_bathroom=$search_data['min_bathroom'];
        $max_bathroom=$search_data['max_bathroom'];
        $property_com=(!empty($search_data['Comercial']))?$search_data['Comercial']:"";
        $property_res=(!empty($search_data['Residential']))?$search_data['Residential']:"";
        $new_build=(!empty($search_data['new_build']))?$search_data['new_build']:"";
        $pre_owned=(!empty($search_data['pre_owned']))?$search_data['pre_owned']:"";
        $sale=(!empty($search_data['sale']))?$search_data['sale']:"";
        $land_estate=(!empty($search_data['land_estate']))?$search_data['land_estate']:"";
        $rent=(!empty($search_data['rent']))?$search_data['rent']:"";
        $share=(!empty($search_data['share']))?$search_data['share']:"";

          $this->db->limit($limit, $start);
          $this->db->select('*');
          $this->db->from('properties');
          
          if($prop_title!='')
          {
             $this->db->like('prop_title', $prop_title);
          }
          if($prop_for!='')
          {
             $this->db->like('prop_for', $prop_for);
          }
          if($prop_type!='')
          {
              $this->db->or_like('property_type', $prop_type);
          }
          if($city!='')
          {
              $this->db->or_like('city', $city);
          }
          if($property_com!='')
          {
              $this->db->or_like('property_type', $property_com);
          }
          if($property_res!='')
          {
              $this->db->or_like('property_type', $property_res);
          }
          if($location!='')
          {
              $this->db->or_like('address', $location);
          }
          if($new_build!='')
          {
              $this->db->or_like('home_type', $new_build);
          }
          if($pre_owned!='')
          {
              $this->db->or_like('home_type', $pre_owned);
          }
          if($sale!='')
          {
         $this->db->or_like('prop_for', $sale);
          }
          if($land_estate!='')
          {
              $this->db->or_like('property_type', $land_estate);
          }
          if($rent!='')
          {
              $this->db->or_like('prop_for', $rent);
          }
          if($share!='')
          {
              $this->db->or_like('property_type', $share);
          }
          if($min_price)
          {
              $this->db->where('price <=', $min_price);
          }
          if($max_price)
          {
              $this->db->where('price >=', $max_price);
          }
          if($min_bathroom)
          {
              $this->db->where('washrooms <=', $min_bathroom);
          }
          if($max_bathroom)
          {
              $this->db->where('washrooms >=', $max_bathroom);
          }
          if($min_bedroom)
          {
              $this->db->where('bedrooms <=', $min_bedroom);
          }
          if($max_bedroom)
          {
              $this->db->where('bedrooms >=', $max_bedroom);
          }
          $this->db->where('status=','true');
          $q = $this->db->get();
              if($q->num_rows()>0)
                {
                    return $q->result();
                }
                else 
                {
                    return false;
                }
    }   
    function count_all($id)
     {
        $this->db->select('*');
        $this->db->from('our_projects');
        $this->db->where(array('category_id'=>$id,'status'=>'true'));
        $query = $this->db->get();
        return $query->num_rows();
     }    
     function count_properties()
     {
        $this->db->select('*');
        $this->db->from('properties');
        $this->db->where(array('status'=>"true"));
        $query = $this->db->get();
        return $query->num_rows();
     }
    function fetch_details($limit, $start,$id,$orderby,$sort)
         {
          $output = '';
          $this->db->select("*");
          $this->db->from("our_projects");
          $this->db->where(array("category_id"=>$id,"status"=>"true"));
          $this->db->limit($limit, $start);
          if(!empty($orderby)){
            $this->db->order_by($orderby,$sort);
            }
          $query = $this->db->get();
          $output .= '';
          foreach($query->result() as $row)
          {
           $output .= ' <div class="nl--card-personal mb-4 col-lg-4 col-md-4 pl-0 col-sm-4 ">
                          <div class="card ">
                              <!--Card image-->
                            <a href="'.SURL.'pages/project_detail/'.$row->id.'">
                              <img class="card-img-top" src="'.IMG.'projects/'.$row->image.'" alt="Card image cap">
                                <div class="mask rgba-white-slight"></div>
                              </a>
                            <!--Card image-->
                            <div class="location">
                            <span class="ml-1"><i class="fa fa-map-marker mr-1"></i>'.$row->project_address.'</span>
                            </div>
                            <!--Card content-->
                            <div class="card-body crd-body"><a class="link-a" href="'.SURL.'pages/project_detail/'.$row->id.'">
                            <!--Title-->
                              <h4 class="card-title">'.$row->title.'</h4>
                              <!--Text-->
                              <p class="card-text">'.word_limiter($row->short_description,18).'</p>
                            </a></div>
                            <!--Card content-->

                            <a href="'.SURL.'pages/project_detail/'.$row->id.'" class="link">GET IN TOUCH</a>
                          </div>
                        </div>';
          }
          return $output;
         }   
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class File_upload extends CI_Model{

    public function upload_image($projects_folder_path,$file_name){

         $projects_folder_path=$projects_folder_path;
       $fname=$_FILES[$file_name]['tmp_name'];
           
            if ($fname != "") {

     
                        $projects_folder_path = $projects_folder_path;
                        $projects_folder_path_main = $projects_folder_path;

                       
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                       

                        $this->load->library('image_lib', $config);

                        
                        

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload($file_name)) {

                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        }
                            else {
                            
                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                           return $filename_f; 
                        }
                    }

                    
    }

function create_optimize($filename, $src, $dest) {
    $ci = & get_instance();

    //resize:
    $config_resize['image_library'] = 'gd2';
//     $config_resize['library_path'] = '/usr/bin/composite';
    $config_resize['source_image'] = $src;
    $config_resize['quality'] = 80;
    $config_resize['new_image'] = $dest;
    $config_resize['maintain_ratio'] = TRUE;

    $ci->load->library('image_lib');
    $ci->image_lib->initialize($config_resize);
    $ci->image_lib->resize();

    $ci->image_lib->clear();
}
function create_thumbnail($filename, $src, $dest, $width = 150, $height = 150) {
    $ci = & get_instance();
//
//    //resize:
    // echo $src."<br>";
    $config_resize['image_library'] = 'gd2';
//    $config_resize['library_path'] = '/usr/bin/mogrify';
    $config_resize['source_image'] = $src;
    $config_resize['quality'] = 80;
    $config_resize['new_image'] = $dest;
//  $config_resize['file_name'] = $filename;
    $config_resize['maintain_ratio'] = FALSE;
    $config_resize['create_thumb'] = FALSE;
    $config_resize['width'] = $width;
    $config_resize['height'] = $height;
//
    // echo $src;
    $data = getimagesize($src);
    $width1 = $data[0];
    $height1 = $data[1];
    if ($width1 != $height1) {

        $config_resize['maintain_ratio'] = TRUE;
        $config_resize['master_dim'] = 'auto';
        $ci->load->library('image_lib');
        $ci->image_lib->initialize($config_resize);
        if ($ci->image_lib->resize()) {

            $dfile = $dest . "/" . $filename;

            $im = imagecreatetruecolor($width, $height);
//        $stamp = imagecreatefromjpeg('img.jpg');

            if (preg_match('/[.](jpg)$/', $filename)) {
                $stamp = imagecreatefromjpeg($dfile);
            } else if (preg_match('/[.](gif)$/', $filename)) {
                $stamp = imagecreatefromgif($dfile);
            } else if (preg_match('/[.](png)$/', $filename)) {
                $stamp = imagecreatefrompng($dfile);
            } else if (preg_match('/[.](jpeg)$/', $filename)) {

                $stamp = imagecreatefromjpeg($dfile);
            } else if (preg_match('/[.](JPG)$/', $filename)) {

                $stamp = imagecreatefromjpeg($dfile);
            }


            $red = imagecolorallocate($im, 255, 255, 255);
            imagefill($im, 0, 0, $red);



            $sx = imagesx($stamp);
            $sy = imagesy($stamp);


            $oy = imagesx($stamp);
            $ox = imagesy($stamp);


            $d = getimagesize($dfile);
            $wd = $d[0];
            $hg = $d[1];

            if ($wd < $width) {
                $mg = $width - $wd;
                $marge_right = $mg / 2;
            } else {
                $marge_right = 0;
            }
            if ($hg < $height) {

                $mgh = $height - $hg;
                $marge_bottom = $mgh / 2;
            } else {
                $marge_bottom = 0;
            }
            $imgg = imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, $sx, $sy);

// echo "resized ".$src;
            header('Content-type: image/png');
            imagejpeg($im, $dfile);
            imagepng($im, $dfile);
            imagedestroy($im);
        } else {

            // echo $ci->image_lib->display_errors();
        }
    } else {

// echo "resized ".$src;
        $ci->load->library('image_lib');
        $ci->image_lib->initialize($config_resize);
        $ci->image_lib->resize();
    }

//      $ci->image_lib->fit();
//
//
//
//    $ci->image_lib->clear();
}



}
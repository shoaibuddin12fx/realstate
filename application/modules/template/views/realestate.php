<?php $this->load->view("include/header_nav"); ?>
<!--slider start-->



<?php $this->load->view('include/slider'); ?>

<!----slider end----->
<!-- our projects start-->

<?php $this->load->view('include/our_projects'); ?>

<!-- our projects ends-->

<div class="nl-black-background">
  <section class="pt-md-5 mt-md-3 pb-md-5 mb-md-5 pt-2 mt-4 pb-2 mb-4 text-center">
    <!--Grid row-->
    <p class="text-light mb-0">HARD TO FIND A PROPERTY? GIVE US A CALL, WE’LL HELP YOU +(971) 4-454-2828</p>
  </section>
</div>

<!-- our properties section starts here-->
<?php $this->load->view("include/our_properties")?>
<!-- our properties section ends here -->

<!-- testimonial section starts here -->
<?php $this->load->view("include/testimonials")?>
<!-- testimonial section ends here -->


<!-- our achievements section starts -->
<?php $this->load->view("include/our_achievements")?>
<!-- our achievements section ends -->



<div class="nl--invest pt-1 mt-5">
<div class="container">
  <!--Section: Group of personal cards-->
    <section >
      <div class="row">       
        <div class="col-12 col-sm-1 nl--icon-box text-center mt-3">
        <div class="nl--icon-line-top d-none d-lg-block"></div> 
          <i class="fa fa-3x fa-home "></i>
        </div>
        <div class="col-12 col-sm-11 text-left pl-sm-5 pt-lg-5 mt-lg-3">
          <h4 class="text-white">Why Next Level Real Estate?</h4>
          <p class="grey-text display-desk">Next Level Real Estate is a trusted property agency working in Dubai for over 5 years. With years of experience and highly competent and skilled staff, we know what it takes to lease out your favorite piece of land. At the heart of our culture, the desire to provide our best services to our clients make us push further and do what needs to be done. We have set the highest standards in client satisfaction and we deliver our services according to that standard. We believe in creating long term professional relationship with our clients and deliver quality services. .</p>
          <p class="grey-text display-mobile">Next Level Real Estate is a trusted property agency working in Dubai for over 5 years. With years of experience and highly competent and skilled staff, we know what it takes to lease out your favorite piece of land. At the heart of our culture, the desire to provide our best services to our clients make us push further and do what needs to be done. <span id="dots">...</span><span id="more" class="more">We have set the highest standards in client satisfaction and we deliver our services according to that standard. We believe in creating long term professional relationship with our clients and deliver quality services.</span>
            <button onclick="myFunction()" id="myBtn" class="team-desc-btn">read more</button></p>
          

        </div>
        <div class="nl--invest-button col-md-12 col-12 pl-sm-5 pt-sm-5 text-center">
          <a href="" class="m-auto" data-toggle="modal" data-target="#exampleModalCenter">FEEDBACK</a>
        </div>
      </div>
    </section>
  </div>
</div>

<!-- blogs section starts here -->
<?php $this->load->view("include/blogs")?>

<!-- blogs section ends here -->


  <!-- SCRIPTS -->

  <?php $this->load->view("include/front_footer"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  <!-- Stylesheet #1: Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
  <!-- Stylesheet #2: Vanilla CSS -->

  <link rel="stylesheet" href="<?=STYLES?>slick.css">
  <link rel="stylesheet" href="<?=STYLES?>slick-theme.css">
  <link rel="stylesheet" href="<?=STYLES?>app.css">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



  <!-- LOADING FONTS AND ICONS -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400%7CRoboto:500" rel="stylesheet" property="stylesheet" type="text/css" media="all">


  <link rel="stylesheet" type="text/css" href="<?=FONTS?>pe-icon-7-stroke/css/pe-icon-7-stroke.css">
  <link rel="stylesheet" type="text/css" href="<?=FONTS?>font-awesome/css/font-awesome.css">

  <!-- REVOLUTION STYLE SHEETS -->
  <!-- REVOLUTION LAYERS STYLES -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <style type="text/css">
    .alert-warning{
      color: red;
      background-color:transparent;
      border-color:transparent; 
    }
    .error{
      color:red;
    }
  </style>

  <!-- ADD-ONS CSS FILES -->

  <!-- ADD-ONS JS FILES -->

  <!-- JQUERY LIBRARY Revolution End-->

</head>

<body>
  <div class="container">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg" data-spy="affix" data-offset-top="60">

      <!-- Navbar brand -->
      <a class="navbar-brand" href="<?php echo SURL.'home';?>"><img src="<?=IMG?>logo-black.png"></a>

      <!-- Collapse button -->
      <button class="navbar-toggler  navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse nl--menu" id="navbarSupportedContent2">

      <!-- Links -->
      <ul class="navbar-nav ml-auto">

        <!-- Features -->
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'home';?>" id="navbarDropdownMenuLink2">Home
          </a>
        </li>      
        <li class="nav-item dropdown">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'about_us'?>" id="navbarDropdownMenuLink2">About Us
          </a>
        </li>      
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'team'?>" id="navbarDropdownMenuLink2">Team
          </a>
        </li>
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'nl_listing'?>" id="navbarDropdownMenuLink2">Properties
          </a>
        </li>
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'builders'?>" id="navbarDropdownMenuLink2">Builders
          </a>
        </li>
        
        
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'contact_us' ?>" id="navbarDropdownMenuLink2">Contact Us
          </a>
        </li>   

      </ul>
      <!-- Links -->
    </div>
    <!-- Collapsible content -->
  </nav>
</div>
<?php $this->load->view($content) ;?>
<footer>
  <?php extract($footer_data[0]);?>
    <!-- Footer starts here -->
    <div class="nl--footer">     
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-3 col-12 p-2 about-nl">
            <img src="<?=IMG?>footer-logo.png" alt="logo" class="img-fluid">
            <p><?=$footer_description?>
            </p>
            <ul>
              <li><i class="fa fa-map-marker"></i><?=$street.' '.$city.' '.$country?></li>
              <li><i class="fa fa-phone"></i><?=$contact_number?></li>
              <li><i class="fa fa-envelope"></i><?=$contact_page_email?></li>
            </ul>
            <div class="social-links">
              <ul>
                <li><a class="a_link_footer" href="<?=$facebook?>"><i class="fa fa-facebook "></i></a></li>
                <li><a class="a_link_footer" href="<?=$insta?>"><i class="fa fa-instagram"></i></a></li>
                <li><a class="a_link_footer" href="<?=$pin?>"><i class="fa fa-youtube"></i></a></li>
                <li><a class="a_link_footer" href="<?=$linkedin?>"><i class="fa fa-linkedin"></i></a></li>
                <li><a class="a_link_footer" href="<?=$gplus?>"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-6 col-lg-4 latest-properties">
            <h5>LATEST PROPERTIES</h5>
            <hr>
            <?php foreach ($properties as $key => $value) {
              echo '<div>
                      <div class="row p-2 text-left">
                        <img class="img-fluid" src="'.IMG.'properties/'.$value['thumb_image'].'">
                        <p>'.$value['prop_short_description'].'</p>
                      </div>
                    </div>';
            } ?>
          </div>
          <div class="col-12 col-sm-6 col-md-6 col-lg-3 nl--partners">
            <h5>OUR PARTNERS</h5>
            <hr>
            <?php 
            $img='';
            $count='';
            echo '<div class="row">';
            foreach ($partners as $key => $value) {
                          
                echo   '
                          <div class="col-sm-4 col-lg-4 col-4 p-1">
                            <img src="'.IMG.'partners/'.$value['image'].'" class="img-fluid">
                          </div>
                        ';
            } 
echo '</div>';
            ?>
          </div>
          <div class="col-md-6 col-lg-2 col-12 col-sm-12 nl--useful-links">
            <h5>USEFULL LINKS</h5> 
            <hr>
            <ul>
              <li><a href="<?php echo SURL.'home'?>">HOME</a></li>
              <li><a href="<?php echo SURL.'team'?>">TEAM</a></li>
              <li><a href="<?php echo SURL.'about_us'?>">ABOUT US</a></li>
              <li><a href="<?php echo SURL.'nl_listing'?>">LISTING PAGE</a></li>
              <li><a href="<?php echo SURL.'builders'?>">BUILDERS PAGE</a></li>
              <li><a href="<?php echo SURL.'contact_us'?>">CONTACT US</a></li>
            </ul>
            <p><?=$work_day.' '.$open_timing.'am'.'<br>to <br>'.$close_timing.'pm'?> 
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="nl--copy-rights">
      <p><?=$copy_rights?></p>
    </div>

  </footer>

</body>

<script src="<?=SURL?>assets/bower_components/jquery/src/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
  $('#navbarSupportedContent2 ul li').mouseover(function () {
    $(this).siblings().removeClass('show');
    $(this).children('div').siblings().removeClass('show');
    $(this).addClass('show');
    $(this).children('div').addClass('show');
  });
  $('#navbarSupportedContent2 ul li').mouseleave(function () {
    $(this).removeClass('show');
    $(this).children('div').removeClass('show');
  });

</script>
<script>

 $(document).ready(function () {
  var slideWidth = ($('.center').outerWidth())/8 +'px';
  

  $('.center').slick({
  centerMode: true,
  centerPadding: '50px',
  autoplay: true,
  slidesToShow: 2,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        autoplay: true,
        centerPadding: '30px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        autoplay: true,
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 1
      }
    }
  ]
});
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
        $(".center").not('.slick-initialized').slick()
        
    });
});
</script>
<script type="text/javascript">

  $(document).ready(function() {
    $('.nl-form-drager').on('click',function(){
      if($('.nl--slider-form').css('display')=='none'){
        $('.nl--slider-form').show('1000');
        $('.nl-form-drager').html('<i class="fa fa-angle-right"></i>');
      }else{
        $('.nl--slider-form').css('display','none');
        $('.nl--slider-form').hide('1000');
        $('.nl-form-drager').html('<i class="fa fa-angle-left"></i>');
      }
  })

  });

</script>

    <script>
function initialize() {
    var myLatlng = new google.maps.LatLng(43.565529, -80.197645);
    var mapOptions = {
        zoom: 8,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

     //=====Initialise Default Marker    
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'marker'
     //=====You can even customize the icons here
    });

     //=====Initialise InfoWindow
    var infowindow = new google.maps.InfoWindow({
      content: "<B>Skyway Dr</B>"
  });

   //=====Eventlistener for InfoWindow
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">

   function subscribes(){

  var emailaddress= $('#emailaddress').val();
    $.ajax({
      method: "Post",
        url: "about_us/subscribers",
        data: {emailaddress:emailaddress},
        success: function(response) {
          $('#error').html(response);
          $('.alert').fadeOut( 10000, function() {});
        }

    });
  }


</script>
<script type="text/javascript">
  function sort_list(vals){
    var sort_by=vals;
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];

        $.ajax({
      method: "Post",
        url: "nl_listing/search_sort",
        data: {sort_by:sort_by,last_part:last_part},
        success: function(response) {
          $('#all_properties').html(response);
        }

    });
  };
</script>
<script>
   function page_sort(){
    var sort_by=$('#list_sort').value;

   $.ajax({
   type: "POST",
   url: $(this).attr("href"),
   data:"",
   success: function(response){
        $('#all_properties').html(response);
   }
   });
   return false;
   };
   $(document).ready(function(){
    $('iframe').attr('width','100%');
    $('iframe').attr('height','382');
   });
</script>
 <script src="<?=SURL?>assets/bower_components/jquery/src/owl.carousel.js"></script>
<script type="text/javascript">

   $(document).ready(function() {

          var owl = $("#owl-demo25");

      owl.owlCarousel({
          autoPlay: 3000,

        // Define custom and unlimited items depending from the width
        // If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled
        // For better preview, order the arrays by screen size, but it's not mandatory
        // Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
        // In the example there is dimension with 0 with which cover screens between 0 and 450px
        
        itemsCustom : [
          [0, 1],
          [450, 1],
          [600, 2],
          [700, 2],
          [1000, 3],
          [1200, 3],
          [1400, 3],
          [1600, 3]
        ],
        navigation : true
});

      var owl = $("#owl-demo24");

      owl.owlCarousel({
          autoPlay: 3000,

        // Define custom and unlimited items depending from the width
        // If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled
        // For better preview, order the arrays by screen size, but it's not mandatory
        // Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
        // In the example there is dimension with 0 with which cover screens between 0 and 450px
        
        itemsCustom : [
          [0, 1],
          [450, 1],
          [600, 3],
          [700, 3],
          [1000, 3],
          [1200, 3],
          [1400, 3],
          [1600, 3]
        ],
        navigation : true
});

      var owl = $("#owl-demo1");

    owl.owlCarousel({
      autoPlay: 3000,

// Define custom and unlimited items depending from the width
// If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled
// For better preview, order the arrays by screen size, but it's not mandatory
// Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
// In the example there is dimension with 0 with which cover screens between 0 and 450px

itemsCustom : [
[0, 1],
[450, 1],
[600, 2],
[700, 3],
[1000, 4],
[1200, 5],
[1400, 5],
[1600, 5]
],
navigation : true

});



  });
</script>
</script>
<script>

    function inquerys() {
          

          $.ajax({
             url:'<?php echo SURL."home/inquery_from" ;?>',
             type: 'POST',
             data: $("#myForm1").serialize(),
             success: function(data){
              var res = $.parseJSON(data);
              if (res.phone) {
                  $('#number').focus();
                  $('#phone').html('<span class="text-danger">' + res.phone+'<span>');
                  $("#phone").fadeIn();
                  $("#phone").fadeOut(8000);

                    }
              if (res.email) {
                  $('#email').focus();
                  $('#mail').html('<span class="text-danger">' + res.email+'<span>');
                  $("#mail").fadeIn();
                  $("#mail").fadeOut(8000);
                    }
              if (res.message) {
                  $('#msg').focus();
                  $('#msg').html('<span class="text-danger">' + res.message+'<span>');
                  $("#msg").fadeIn();
                  $("#msg").fadeOut(8000);
                    }
              if (res.f_name) {
                  $('#f_name').focus();
                  $('#fname').html('<span class="text-danger">' + res.f_name+'<span>');
                  $("#fname").fadeIn();
                  $("#fname").fadeOut(8000);
                    }
              if (res.c_code) {
                  $('#c_code').focus();
                  $('#ccode').html('<span class="text-danger">' + res.c_code+'<span>');
                  $("#ccode").fadeIn();
                  $("#ccode").fadeOut(8000);
                    }
              if (res.nationality) {
                  $('#nationality').focus();
                  $('#nationalty').html('<span class="text-danger">' + res.nationality+'<span>');
                  $("#nationalty").fadeIn();
                  $("#nationalty").fadeOut(8000);
                    }
              if (res.inquery) {
                  $('#inquery').focus();
                  $('#inqury').html('<span class="text-danger">' + res.inquery+'<span>');
                  $("#inqury").fadeIn();
                  $("#inqury").fadeOut(8000);
                    }
              if(res.reslt){
$('#myForm1').find("input[type=text], textarea").val("");
                $('#reslt').html(res.reslt);
$('#reslt').fadeIn(1);
                $('#reslt').fadeOut(10000);
              }
             }
         });

    }
</script>
<script>

 function load_country_data(page)
 {
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
  $.ajax({
   url:"<?php echo SURL?>builders/pagination/"+page+"/"+last_part,
   method:"GET",
   dataType:"json",
   success:function(data)
   {
    $('#country_table').html(data.country_table);
    $('#pagination_link').html(data.pagination_link);
   }
  });
 }
 


  $(document).on("click", ".pag li a", function(event){
  event.preventDefault();
  var page = $(this).data("ci-pagination-page");
  load_country_data(page);
 });

</script>
<script>

function sort_prop(page){
  var sort_by = $("select[name='duration'] option:selected").val();
    $.ajax({
   url:"<?php echo SURL?>nl_listing/sort_prop",
   method: "Post",
   data: {sort_by:sort_by,page:page},
   dataType:"json",
   success:function(data)
   {
    $('#prop').html(data.properties);
    $('#pagination').html(data.pagination_link);
    $('#total_prop').html(data.total_prop +" "+"Result Listing");
   }
  });
  }

  $(document).on("click", ".pagee li a", function(event){
  event.preventDefault();
  var page = $(this).data("ci-pagination-page");
  sort_prop(page);
 });

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#vid iframe").width(1350);
        $("#vid iframe").height(700);
    });
</script>
<script>

function prop_variations(prop_id){
  var prop_id= prop_id;
  var variations = $("select[name='variation'] option:selected").val();
    $.ajax({
   url:"<?php echo SURL?>nl_listing/property_variations",
   method: "Post",
   data: {variations:variations,prop_id:prop_id},
   dataType:"json",
   success:function(data)

   { 
    console.log(data);
    $("#area").html(data[0].v_area);
    $("#tit").html(data[0].v_title);
    $("#bed").html(data[0].no_beds);
    $("#bath").html(data[0].no_baths);
    $("#kitchen").html("kitchen");
    $("#no_kit").html(data[0].no_kitchen);
    $("#price").html(data[0].v_price+" AED");
    $("#titl").html(data[0].v_title);
   }
  });
  }

</script>
<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "read less"; 
    moreText.style.display = "inline";
  }
}
</script>


</html>
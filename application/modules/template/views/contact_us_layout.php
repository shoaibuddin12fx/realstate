<?php $this->load->view("include/header_nav"); ?>
<!--slider start-->
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?=IMG.'sliders/'.$sliders[0]['image']?>" alt="Los Angeles" width="1100" height="500">
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
          <p class="address"><?=$sliders[0]['address']?></p>
          <h2><?=$sliders[0]['heading']?></h2>
          <p class="desc"><?=$sliders[0]['description']?></p>
          <a href="" data-toggle="modal" data-target="#exampleModalCenter" class="caption-btn"><?=$sliders[0]['button_title']?></a>
        </div>
      </div>
    </div>
  </div>
                 <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
<div class="modal-body">
  <div class="container-fluid">

 <div class="forms_outer">      

      <form method="post" class="form-horizontal nl--slider-form position-relative nl--propertydetail-model" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>  

        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-8">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">
            </div><div class="col-md-4  col-8">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </fieldset>
      </form>
    </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- Model ends -->
<!----slider end----->


<!--page content start-->
<div class="nl--contact-us">
        <div class="container">
          <div class="contact">
            <div class="row">
             <div class="col-12 col-sm-2 col-lg-1 col-md-2 nl--icon-box text-center">
               <div class="nl--icon-line-top d-none d-lg-block"></div> 
                  <i class="fa fa-3x fa-headphones"></i>
                </div>
              <div class="col-12  col-sm-10 col-lg-11 col-md-10  mt-md-5 pt-md-3 text-center text-md-left pl-lg-5 pl-md-0 pt-lg-5 ">
              <h3>We Are Here To Help You</h3>
              <p>Contact the customer department of Next Level Real Estate anytime and ask all your queries regarding real estate. We are here to listen and solve your issues.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-12 col-lg-4">
              <div class="row">
              <i class="col-md-2 col-lg-2 col-12  fa fa-phone icon-size"></i>
              <div class="col-md-10 col-12 text-center text-md-left ">
              <h5>Phone</h5>
              <div>Telephone : <?=$footer_data[0]['contact_number']?></div>
              <div>Email: <?=$footer_data[0]['contact_page_email']?></div>
            </div>
             </div>
            </div>
            <div class="col-md-4 col-12 col-lg-4">
              <div class="row">
              <i class="col-md-2 col-lg-2 col-12  fa fa-map-marker icon-size"></i>
              <div class="col-md-10 col-12 text-center text-md-left ">
              <h5>Address</h5>
              <div><?=$footer_data[0]['street']?></div>
              <div><?=$footer_data[0]['city'].' '.$footer_data[0]['country']?></div>
            </div>
             </div>
            </div>
            <div class="col-md-4 col-12 col-lg-4">
              <div class="row">
              <i class="col-md-2 col-lg-2 col-12  fa fa-envelope icon-size"></i>
              <div class="col-md-10 col-12 text-center text-md-left ">
              <h5>Email</h5>
              <div><?=$footer_data[0]['contact_page_email']?></div>
              <div><?=$footer_data[0]['work_day'].' '.substr($footer_data[0]['open_timing'],0,strlen($footer_data[0]['open_timing'])-3).'am'.'-'.substr($footer_data[0]['close_timing'],0,strlen($footer_data[0]['close_timing'])-3).'pm'?></div>
            </div>
             </div>
            </div>
          </div>
          <div class="row mt-lg-5 pt-lg-5" id="contact">
            <div class="col-12 col-sm-2 col-lg-1 nl--icon-box text-center">
              <div class="nl--icon-line-left d-none d-lg-block lft-line"></div>
                <i class="fa fa-3x fa fa-envelope "></i>
              </div>
              <div class="col-12  col-sm-10 col-lg-11 col-md-10  mt-md-5 pt-md-3 text-center text-md-left pl-lg-5 pl-md-0 pt-lg-3 ">
              <h3>We Are Here To Help You</h3>
              <p>Contact the customer department of Next Level Real Estate anytime and ask all your queries regarding real estate. We are here to listen and solve your issues. </p>
            </div>
            <form id="defaultForm" method="post" action="<?php echo SURL.'contact_us/contact_email#contact'?>" class="form-horizontal">
              <div class="clearfix"></div>
            <?php if(!empty($this->session->flashdata('message'))){echo '<h4 id="msg" class="alert alert-success">'.$this->session->flashdata('message').'</h4>';}?>
              <div class="row">
              <div class="col-md-4 mb-lg-5 pd-lg-5">
              <input class="form-control" type="" placeholder="Enter your name" name="name" id="name">
              <?php echo form_error('name','<span class="error">', '</span>'); ?>
              </div>
              <div class="col-md-4 mb-lg-5 pd-lg-5">
              <input class="form-control" type="" placeholder="Email" name="email" id="name">
              <?php echo form_error('email','<span class="error">', '</span>'); ?>
              </div>
              <div class="col-md-4 mb-lg-5 pd-lg-5">
              <input class="form-control" type="" placeholder="Subject" name="subject" id="subject">
              <?php echo form_error('subject','<span class="error">', '</span>'); ?>
              </div>
              <div class="col-md-12 mb-lg-5 pd-lg-5">
              <textarea class="form-control" rows="5" type="" name="message" id="message" placeholder="Message"></textarea>
              <?php echo form_error('message','<span class="error">', '</span>'); ?>
              </div>
            </div>
            <div class="col-md-2 mb-lg-5 pd-lg-5 mt-3 mb-3">
              <button type="submit" value="btn" name="btn" id="btn">SEND</button>
            </div>
            </form>
          </div>
        </div>
      </div>
      <div class="map-contactus" id="map-canvas">
      </div>
    </div>
<!--page content ends-->

<!-- SCRIPTS -->
<?php $this->load->view("include/front_footer"); ?>
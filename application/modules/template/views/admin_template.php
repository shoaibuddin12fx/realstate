<?php $controler=$this->uri->segment(1);
$function=$this->uri->segment(2);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Real Estate | <?php if(empty($function)){echo ucwords($controler);}else{echo ucwords(str_replace("_"," ",$function));}?></title>
        <base href="<?=SURL?>">
        <meta name="description" content="description">
        <meta name="author" content="DevOOPS">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
        <link href="<?=APLUGINS?>bootstrap/bootstrap.css" rel="stylesheet">
        <link href="<?=APLUGINS?>jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
        <link href="<?=APLUGINS?>fancybox/jquery.fancybox.css" rel="stylesheet">
        <link href="<?=APLUGINS?>fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="<?=APLUGINS?>xcharts/xcharts.min.css" rel="stylesheet">
        <link href="<?=APLUGINS?>select2/select2.css" rel="stylesheet">
        <link href="<?=ACSS?>style.css" rel="stylesheet">




        <link rel="stylesheet" href="<?=ADIST;?>icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
        <link rel="stylesheet" href="<?=ADIST;?>icon-fonts/map-icons-2.1.0/css/map-icons.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/font/octicons.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/1.3.1/css/weather-icons.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.8.0/css/flag-icon.min.css"/>
        <link rel="stylesheet" href="<?=ADIST;?>css/bootstrap-iconpicker.css"/>

        <style type="text/css">
            .bg-gray {
                color: #777;
                background-color: #eee;
            }
            #btnGithub, #btnDonate {
                width: 200px;
            }
            #btnGithub span, #btnDonate span {
                font-style: italic;
            }
            #btnStars, #btnForks, #btnReleases, #btnContributors {
                font-weight: bold;
            }
            .tab-content {
                padding: 12px;
                border-left: 1px solid #ddd;
                border-right: 1px solid #ddd;
                border-bottom: 1px solid #ddd;
            }
            code.code-default {
                color: #ffffff;
                background-color: #f0ad4e;
            }
            code.code-info {
                color: #ffffff;
                background-color: #5bc0de;
            }
            code.code-options {
                color: #ffffff;
                background-color: #9b59b6;
            }
            code.code-event {
                color: #ffffff;
                background-color: #449d44;
            }
            code.code-method {
                color: #ffffff;
                background-color: #357ebd;
            }
            #console {
                height: 135px;
                overflow-y: scroll;
                color: white;
                background-color: black;
            }
        </style>




        <script src="<?=APLUGINS;?>jquery/jquery-2.1.0.min.js" ></script>
        <script src="<?=APLUGINS;?>jquery-ui/jquery-ui.min.js"></script>
                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                        <script src="http://getbootstrap.com/docs-assets/<?=JS?>/html5shiv.js"></script>
                        <script src="http://getbootstrap.com/docs-assets/<?=JS?>/respond.min.js"></script>
                <![endif]-->
       
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="<?=APLUGINS;?>bootstrapvalidator/bootstrapValidator.min.js"></script>
        <script src="<?=APLUGINS;?>justified-gallery/jquery.justifiedgallery.min.js"></script>
        <script type="text/javascript" src="<?=ADIST;?>js/bootstrap-iconpicker-iconset-all.js"></script>
        <script type="text/javascript" src="<?=ADIST;?>js/bootstrap-iconpicker.js"></script>
        <!--End Container-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="http://code.jquery.com/jquery.js"></script>-->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?=APLUGINS;?>bootstrapvalidator/bootstrapValidator.min.js"></script>
        <script src="<?=APLUGINS;?>justified-gallery/jquery.justifiedgallery.min.js"></script>
        <script src="<?=APLUGINS;?>tinymce/tinymce.min.js"></script>
        <script src="<?=APLUGINS;?>tinymce/jquery.tinymce.min.js"></script>
        <!-- All functions for this theme + document.ready processing -->
        <script src="<?=AJS;?>devoops.js"></script>
        <script src="<?=SURL;?>assets/ckeditor/ckeditor.js"></script>


    </head>
<body>
<!--Start Header-->
<div id="screensaver">
    <canvas id="canvas"></canvas>
    <i class="fa fa-lock" id="screen_unlock"></i>
</div>
<div id="modalbox">
    <div class="devoops-modal">
        <div class="devoops-modal-header">
            <div class="modal-header-name">
                <span>Basic table</span>
            </div>
            <div class="box-icons">
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="devoops-modal-inner">
        </div>
        <div class="devoops-modal-bottom">
        </div>
    </div>
</div>
<header class="navbar">
    <div class="container-fluid expanded-panel">
        <div class="row">
            <div id="logo" class="col-xs-12 col-sm-2">
                <a href="<?PHP echo AURL;?>">Real Estate</a>
            </div>
            <div id="top-panel" class="col-xs-12 col-sm-10">
                <div class="row">
                    <div class="col-xs-4 col-sm-12 top-panel-right">
                        <ul class="nav navbar-nav pull-right panel-menu">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                    <i class="fa fa-angle-down pull-right"></i>
                                    <div class="user-mini pull-right">
                                        <span class="welcome">Welcome,</span>
                                        <span><?php echo $this->session->userdata('name');?></span>
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo SURL.'admin/admin_detail'?>">
                                            <i class="fa fa-user"></i>
                                            <span class="hidden-sm text">Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo SURL.'admin/logout' ;?>">
                                            <i class="fa fa-power-off"></i>
                                            <span class="hidden-sm text">Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" class="container-fluid">
    <div class="row">
        <div id="sidebar-left" class="col-xs-2 col-sm-2">
            <ul class="nav main-menu">
                <li>
                    <a href="<?PHP echo AURL;?>" <?php if($controler=='admin'){ ?> class="active" <?php }?>>
                        <i class="fa fa-dashboard"></i>
                        <span class="hidden-xs">Dashboard</span>
                    </a>
                </li>
              <?php echo $menu?>
                
            </ul>
        </div>
        <!--Start Content-->
        <div id="content" class="col-xs-12 col-sm-10">
            <div class="preloader">
                <img src="<?=AIMG;?>/devoops_getdata.gif" class="devoops-getdata" alt="preloader"/>
            </div>
            <div id="datashow"><?php $this->load->view($content);?></div>
        </div>
        <!--End Content-->
    </div>
</div>





 


<script>

 CKEDITOR.replace( 'editor1' ,{
 extraPlugins: 'cleanuploader',
    filebrowserBrowseUrl: '/browser/browse.php',
    filebrowserUploadUrl: '/uploader/upload.php',
 });
 







</script>






        <script type="text/javascript"></script>

        <!-- Button Builder Example -->
        
     









</body>
</html>
<script>

    function portfolio_detail(form_id) {

      var  dataString = new FormData($(form_id)[0]);
      console.log($(form_id).find( '#service_slug').val());
      if($(form_id).find( '#service_slug').val()==''){
        $(form_id).find( '#service_slug').focus();
        return false;
      }
       $(form_id).find( '#fm-2').prop('disabled', true);
         $.ajax({
            type: "POST",
            url: "<?=SURL?>home_page_portfolio/insert_detail",
            data:dataString ,
            processData:false,
            cache:false,
            contentType:false,
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);

                $(data.id).fadeIn();
                $(data.id).html(data.message); 
                $(data.id).addClass(data.class);
                $(data.id).fadeOut(10000);
                setTimeout(
         function() 
                   {
                location.reload();
    //do something special
                    }, 5000);
            }

        });

          //stop the actual form post !important!

    }
</script>
<script>

    function service_detail(form_id,func) {

      var  dataString = new FormData($(form_id)[0]);
      if($('#service_category').val()==''){
        $('#service_category').focus();
        return false;
      }
       dataString.append('service_category',$('#service_category').val());
         $.ajax({
            type: "POST",
            url: "<?=SURL?>service_management/"+func,
            data:dataString ,
            processData:false,
            cache:false,
            contentType:false,
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);

                $(data.id).fadeIn();
                $(data.id).html(data.message); 
                $(data.id).addClass(data.class);
                $(data.id).fadeOut(10000);
                setTimeout(
         function() 
                   {
                location.reload();
    //do something special
                    }, 5000);
            }

        });

          //stop the actual form post !important!

    }
</script>
<script>

    function update_service_detail(id) {

      var  id = id;
         $.ajax({
            type: "POST",
            url: "<?=SURL?>our_services/edit_service_sliders",
            data:{id:id },
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);


                $('#slider_heading').val(data.heading);
                $('#slider_description').val(data.description);
                $('#slider_status').val(data.status);
                $('#slider_id').val(data.id); 

            }

        });

          //stop the actual form post !important!

    }
</script>
<script>

    function update_service_types(id) {

      var  id = id;
         $.ajax({
            type: "POST",
            url: "<?=SURL?>our_services/edit_service_types",
            data:{id:id },
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);
                
                $('#srv_name').val(data.service_name);
                $('#service_status').val(data.status);
                $('#type_id').val(data.id);
                $('#srv_img').val(data.service_img);
                $('#ser_img').append('<img width="100px" src="<?=IMG?>slider_section/'+ data.service_img + '" />'); 

            }

        });
     }
</script>
<script>

    function update_pricing_packages(id) {

      var  id = id;
         $.ajax({
            type: "POST",
            url: "<?=SURL?>our_services/edit_pricing_packages",
            data:{id:id },
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);
                
                $('#features').val(data.feature);
                $('#lite').val(data.lite);
                $('#optimum').val(data.optimum);
                $('#premium').val(data.premium);
                $('#pricing_status').val(data.status);
                $('#pricing_id').val(data.id);


            }

        });
     }
</script>
<script>

    function update_procedure(id) {

      var  id = id;
         $.ajax({
            type: "POST",
            url: "<?=SURL?>our_services/edit_procedure",
            data:{id:id },
            success: function(data){
                // alert('Successful!');
                var data=jQuery.parseJSON(data);
                
                $('#pro_heading').val(data.heading);
                $('#pro_id').val(data.id);
                $('#pro_description').val(data.description);
                $('#pro_status').val(data.status);
                $('#pro_icon_img').val(data.icon_img);
                $('#icn_img').append('<img width="100px" src="<?=IMG?>slider_section/'+ data.icon_img + '" />');


            }

        });
     }
</script>
<script>

    function delete_row(id,table,tb_id) {

      var  id = id;
      var table = table;
      var tb_id = tb_id;
      if(confirm(' Are you sure you want to delete?')){
        $.ajax({
            type: "POST",
            url: "<?=SURL?>service_management/delete_record",
            data:{id:id,table:table,tb_id:tb_id },
            success: function(data){
                var data=jQuery.parseJSON(data);
                $(data.id).fadeIn();
                $(data.id).html(data.message); 
                $(data.id).addClass(data.class);
                $(data.id).fadeOut(10000);
                setTimeout(
         function() 
                   {
                location.reload();
    //do something special
                    }, 5000);
            }

        });
     }
         
     }
</script>
<script type="text/javascript">
        function delete_portfolio(id) {

      var  id = id;
      alert(id);
      if(confirm(' Are you sure you want to delete?')){
        $.ajax({
            type: "POST",
            url: "<?=SURL?>home_page_portfolio/delete_record",
            data:{id:id },
            success: function(data){
                var data=jQuery.parseJSON(data);
                $(data.id).fadeIn();
                $(data.id).html(data.message); 
                $(data.id).addClass(data.class);
                $(data.id).fadeOut(10000);
                setTimeout(
         function() 
                   {
                location.reload();
    //do something special
                    }, 5000);
            }

        });
     }
         
     }
</script>
<script type="text/javascript">
    $(document).ready(function() {
if($('select[name="category_id"]').val()!=""){
             var category_id = $('select[name="category_id"]').val();
             var current_vel=$('#project_id').val();
             var selected='';   
            $.ajax({
                    url: '<?=SURL?>project_gallery/projects/'+category_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="project_id"]').empty();
                        $.each(data, function(key, value) {
                            if(value.id==current_vel){
                                selected='selected';
                            }
                            else{
                                selected='';
                            }
                            $('select[name="project_id"]').append('<option value="'+ value.id +'" '+selected +'>'+ value.title +'</option>');
                        });

                        if($('select[name="project_id"]').val()!=""){

                        var project_id = $('select[name="project_id"]').val();
                        var current_pro=$('#property_id').val();
                        var selected_prop='';
                            $.ajax({
                    url: '<?=SURL?>property_gallery/properties/'+project_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="property_id"]').empty();
                        $.each(data, function(keys, values) {
                            if(values.prop_id==current_pro){
                                selected_prop='selected';
                            }
                            else{
                                selected_prop='';
                            }
                            $('select[name="property_id"]').append('<option value="'+ values.prop_id +'" '+selected_prop +'>'+ values.prop_title +'</option>');
                        });
                    }
                });
    }
                    }
                });
        }
          
        $('select[name="category_id"]').on('change', function() {
            var category_id = $(this).val();
            if(category_id) {
                $.ajax({
                    url: '<?=SURL?>project_gallery/projects/'+category_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="project_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="project_id"]').append('<option value="'+ value.id +'">'+ value.title +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="project_id"]').empty();
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        
        $('select[name="project_id"]').on('change', function() {
            var project_id = $(this).val();
            if(project_id){
                $.ajax({
                    url: '<?=SURL?>property_gallery/properties/'+project_id,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="property_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="property_id"]').append('<option value="'+ value.prop_id +'">'+ value.prop_title +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="project_id"]').empty();
            }
        });
    });

    function replace_iframe(id) {
text_area_data =$("#"+id).val();
res=text_area_data.replace('iframe ','xyz ');

res=res.replace('</iframe>','</xyz>');
$("#"+id).val(res);
}
</script>
<script type="text/javascript">
    $(document).ready(function() {

        
        $('select[name="proj"]').on('change', function() {
            var proj = $(this).val();
            if(proj){
                $.ajax({
                    url: '<?=SURL?>property_gallery/properties/'+proj,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="prop"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="prop"]').append('<option value="'+ value.prop_id +'">'+ value.prop_title +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="proj"]').empty();
            }
        });
    });
    </script>
<script type="text/javascript">
  $('#layout').on('change', function () {
     var selectVal = $("#layout option:selected").val();
     if(selectVal=='listing_1'){
      $("#layout_img").attr("src",'<?=IMG."listing-01.png"?>');
      $("#lay_img").attr("value","listing-01.png");
     }else{
      $("#layout_img").attr("src",'<?=IMG."listing-02.png"?>');
      $("#lay_img").attr("value","listing-02.png");
     }
});
</script>

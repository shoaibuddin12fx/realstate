<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- The slideshow -->
<div class="carousel-inner">

    <?php foreach ($sliders as $key => $value) {
      extract($value);
          if($key==0){
            $active='active';
          }else{
            $active='';
          }
    echo '<div class="carousel-item '.$active.'">';
    if(!empty($image)){
        echo '<img src="'.IMG.'sliders/'.$image.'" alt="Los Angeles" width="1100" height="500">';
      }else{
        echo '<img src="'.IMG.'no_image.png'.'" alt="Los Angeles" width="1100" height="500">';
      }
        echo'<div class="caption">
         <img src="'.IMG."/c0e30-marker.png".'" alt="Map marker" class="map-marker">
         <p class="address">'.$address.'</p>
         <h2>'.$heading.'</h2>
         <p class="desc">'.$description.' </p>
         <a href="'.$link.'" class="caption-btn" data-toggle="modal" data-target="#exampleModalCenter">'.$button_title.'</a>
         </div>
        </div>';
    } ?>

  <!-- Left and right controls -->
  
  <div class="container">
<div class="forms_outer">
     <div class="nl-form-drager"><i class="fa fa-envelope"></i></div>
      <form method="post" class="form-horizontal nl--slider-form header-form-homepage" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>
        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-12">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">

            </div>
          </div>


          <!-- Text input-->


          <!-- Select Basic -->


        </fieldset>
      </form>
      <form class="form-horizontal nl--filter-form" method="post" action='<?php echo SURL.'nl_listing/search_detail'?>'>
        <fieldset>
          <!-- Text input-->
          <div class="row">

            <div class="col-md-3  m-0 p-0">
              <label class="col-md-6  m-0 p-0" for="Load Number">I want to:</label> 
              <select name="prop_for" id="prop_for" class="col-md-5  m-0 p-0 border-0 slect">
                <option value="">Select </option>
                <option value="Sale">Buy</option>
                <option value="Rent">Rent</option>
              </select>
            </div> 
            <div class="col-md-4  m-0 p-0">
              <label class="col-md-5 m-0 p-0" for="Load Number">Property Type:</label> 
              <select name="prop_type" class="col-md-6  m-0 p-0 border-0 slect">
                <option value="">Select </option>
                <option value="Residential">Residential</option>
                <option value="Comercial">Comercial</option>
              </select>
            </div> 
            <div class="col-md-3  m-0 p-0">
              <label class="col-md-6  m-0 p-0" for="Load Number">Location:</label> 
              <select name="city" class="col-md-5  m-0 p-0 border-0 slect">
                <option value="">Select </option>
                <option value="UAE">UAE</option>
                <option value="Dubai">Dubai</option>
                <option value="Sharjah">Sharjah</option>
              </select>
            </div> 
            <div class="col-md-2 m-0 p-0">
              <input type="submit" name="submit" id="submit" value="Search" class="form-control search-btn input-md">
            </div> 
          </div>
        </fieldset>
      </form>
  </div>
</div>
  <!-- Left and right controls -->
 
</div>
  </div>

</div>
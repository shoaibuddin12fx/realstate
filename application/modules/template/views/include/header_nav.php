<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  <!-- Stylesheet #1: Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
  <!-- Stylesheet #2: Vanilla CSS -->

  <link rel="stylesheet" href="<?=STYLES?>slick.css">
  <link rel="stylesheet" href="<?=STYLES?>slick-theme.css">
  <link rel="stylesheet" href="<?=STYLES?>app.css">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- JQUERY LIBRARY Revolution Start-->

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>

  <!-- LOADING FONTS AND ICONS -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400%7CRoboto:500" rel="stylesheet" property="stylesheet" type="text/css" media="all">


  <link rel="stylesheet" type="text/css" href="<?=FONTS?>pe-icon-7-stroke/css/pe-icon-7-stroke.css">
  <link rel="stylesheet" type="text/css" href="<?=FONTS?>font-awesome/css/font-awesome.css">

  <!-- REVOLUTION STYLE SHEETS -->
  <!-- REVOLUTION LAYERS STYLES -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <style type="text/css">
    .alert-warning{
      color: red;
      background-color:transparent;
      border-color:transparent; 
    }
    .error{
      color:red;
    }
  </style>

  <!-- ADD-ONS CSS FILES -->

  <!-- ADD-ONS JS FILES -->

  <!-- JQUERY LIBRARY Revolution End-->

</head>

<body>
  <div class="container">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg" data-spy="affix" data-offset-top="60">

      <!-- Navbar brand -->
      <a class="navbar-brand" href="<?php echo SURL.'home';?>"><img src="<?=IMG?>logo-black.png"></a>

      <!-- Collapse button -->
      <button class="navbar-toggler  navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><i class="fa fa-bars"></i></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse nl--menu" id="navbarSupportedContent2">

      <!-- Links -->
      <ul class="navbar-nav ml-auto">

        <!-- Features -->
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'home';?>" id="navbarDropdownMenuLink2">Home
          </a>
        </li>      
        <li class="nav-item dropdown">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'about_us'?>" id="navbarDropdownMenuLink2">About Us
          </a>
        </li>      
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'team'?>" id="navbarDropdownMenuLink2">Team
          </a>
        </li>
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'nl_listing'?>" id="navbarDropdownMenuLink2">Properties
          </a>
        </li>
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'builders'?>" id="navbarDropdownMenuLink2">Builders
          </a>
        </li>
        
        
        <li class="nav-item dropdown ">
          <a class="nav-link text-uppercase" href="<?php echo SURL.'contact_us' ?>" id="navbarDropdownMenuLink2">Contact Us
          </a>
        </li>   

      </ul>
      <!-- Links -->
    </div>
    <!-- Collapsible content -->
  </nav>
</div>
<div class="container">
  <!--Section: Group of personal cards-->
  <div class="blog-section">
  <section class="pt-sm-5 mt-3 pb-sm-3">
    <div class="row nl--projects">
      <div class="col-12 col-sm-2 col-lg-1 nl--icon-box text-center">
      <div class="nl--icon-line-left d-none d-lg-block"></div>
        <i class="fa fa-3x fa-home "></i>
      </div>
      <div class="col-12 col-sm-10 col-lg-11 text-left">
        <h4 class="font-weight-bold">OUR LATEST BLOG</h4>
        <p class="grey-text">Subscribe to our blog and find out all the latest news and details from the real estate industry.</p>
      </div>
    </div>
  </section>
    <div class="row">
        <!--Card group-->

          <!--Card-->
          <?php foreach ($blogs as $key => $value) {

            echo '<div class="nl--card-personal mb-4 col-lg-4 col-md-4 col-sm-4 ">
                    <div class="card ">
                      <!--Card image-->
                        <img class="card-img-top" src="'.IMG.'news/'.$value['image'].'" alt="Card image cap">
                        <a href="#!">
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      <!--Card image-->
                      <!--Card content-->
                      <div class="card-body">
                        <!--Title-->
                        <a>
                          <h4 class="card-title">'.ucwords($value['title']).'</h4>
                        </a>
                        <!--Text-->
                        <p class="card-text">'.$value['description'].'</p>
                        
                      </div>
                      <a href="" class="link">GET IN TOUCH</a>
                    </div>
                  </div>';

          } ?>
        <!--Card group-->
    </div>

  </div>
  
  <div class="row">
    <div class="nl--event mt-sm-4 mb-sm-5 pb-sm-2 pt-sm-2 col-lg-12"> 
      <p><i class="fa fa-3x  fa-calendar mr-sm-4 col-12 col-lg-1 m-lg-0 p-lg-0"></i> <span class="heading pr-sm-2 mr-sm-2">Upcoming events</span>
        <span class="desc">Never miss a future offer or opening event,</span> <a href="#" >sign up for exclusive offers</a></p>
      </div>
    </div>
  </div>
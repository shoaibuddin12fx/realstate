<div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-sm-5 mt-sm-3 pb-sm-3">
    <div class="row nl--projects">
      <div class="col-12 col-sm-2 col-lg-1 nl--icon-box">
      <div class="nl--after-home d-none d-lg-block"></div>
        <i class="fa fa-3x fa-home "></i>
      </div>
      <div class="col-12 col-sm-10 col-lg-11 text-left">
        <h4 class="font-weight-bold">Our Projects</h4>
        <p class="grey-text">We are working on some of the top notch projects. Check out some of our exciting projects and get in touch to discuss more. </p>
      </div>
    </div>
    <!--Grid row-->


    <div class="row mt-5">
        <div id="owl-demo25" class="owl-carousel">
          <?php foreach ($projects as $key => $value) {
          
        echo  '<div class="item">
                      <a href="'.SURL.'pages/project_detail/'.$value['proj_id'].'"><div class="box16">
                        <img src="'.IMG.'projects/'.$value['proj_image'].'">
                        <div class="box-content">
                          <h2 class="title heading"><strong>'.$value['proj_title'].'</strong></h2>
                          <span class="post">'.$value['cat_title'].'</span>
                          <h5 class="price-tag"><strong>Price</strong></h5>
                          <h5 class="price mb-2">Starting from:<br><span class="price-span">AED '.$value['price'].'</span></h5>
                          <h5 class="delivery_date mb-2">Expected Delivery:<span class="price-span"><br>'.$value['delivery_date'].'</span></h5>
                          <h5 class="title mb-1 box-line-desc mr-4">'.$value['one_line_desc'].'</h5>
                          <span class="details">'.$value['short_description'].'</span>
                        </div>
                      </div>
                      </a>
                  </div>';
        }?>
        </div>
      </div>
    <div class="row  mt-lg-3 mb-lg-4 text-center">
      <div class="register-call nl--cards-button mt-lg-5 m-auto m-lg-0 text-center col-lg-12 col-sm-12 col-md-12">
        <a href="" data-toggle="modal" data-target="#exampleModalCenter">Register now for a call back </a>
      </div>
    </div>
               <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
<div class="modal-body">
  <div class="container-fluid">

 <div class="forms_outer">      

      <form method="post" class="form-horizontal nl--slider-form position-relative nl--propertydetail-model" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>  

        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-12 col-12">
              <input id="f_name" name="f_name" type="text" placeholder="Name*" class="form-control input-md">
              <input id="page" name="page" type="hidden" value="Home" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div>
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country Code(+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Telephone Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row"> 
            <div class="col-md-12  col-12">
              <input id="nationality" name="nationality" type="text" placeholder="Country*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-8">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">
            </div><div class="col-md-4  col-8">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </fieldset>
      </form>
    </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- Model ends -->
    <!--Grid row-->

  </section>

  <!--Section: Group of personal cards-->
</div>
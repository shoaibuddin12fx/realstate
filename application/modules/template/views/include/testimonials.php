<div class="nl--testimonials pb-5 mt-5 nl-black-background">
  <div class="container">
    <!--Section: Group of personal cards-->
    <section class="pt-sm-5 mt-sm-3 pb-sm-3">
      <div class="row nl--projects">  
        <div class="col-12 col-sm-2 col-lg-1 nl--icon-box">
        <div class="nl--icon-line-top  d-none d-lg-block"></div>
          <i class="fa fa-3x fa-quote-right text-white"></i>
        </div>
        <div class="col-12 col-sm-10 col-lg-9 text-left pl-sm-5 pt-sm-1">
          <h4 class="font-weight-bold">People talking about us</h4>
          <p class="grey-text">Next Level Real Estate is a mid-sized company with a Team of 15 Members, Fully focused and dedicated on a commitment to provide excellent experience to all our customer</p>

        </div>
        <div class="col-12 nl--testimonial-button feedback-btn pt-lg-5 pb-5 col-sm-2">
          <a href="" class="m-auto m-sm-0">FEEDBACK</a>
        </div>
      </div>
    </section>
    <div class="last_slider">

      <div id="demo2">
        <div id="owl-demo24" class="owl-carousel">
          <?php foreach ($testimonials as $key => $value) {?>
          
        <div class="item">
                    <div class="brands_col">
                      <div class="row">
                        <div class="col-12 col-sm-3">
                          <img src="<?php if(!empty($value['image'])){echo SURL.'assets/images/testimonials/'.$value['image'] ;}else{echo SURL.'assets/images/dummy.jpg' ;}?>" alt="" class="rounded-circle" style="height: 55px;">
                        </div>
                        <div class="col-12 col-sm-9">
        
                          <p class="pr-2 text-center-mobile" style="/*height:37px;*/"><?=$value['description']?></p>
                          <p class="ml-0 pl-0 name"><?=$value['name']?></p>
                        </div>
                      </div>
                    </div>
                  </div>;
       <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>
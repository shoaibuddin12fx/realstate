
<div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-sm-5 mt-sm-3 pb-sm-3 ">
    <div class="row nl--projects">
      <div class="col-12 col-sm-2 col-lg-1 nl--icon-box">
      <div class="nl--icon-line-left d-none d-lg-block lft-line"></div>
        <i class="fa fa-3x fa-user "></i>
      </div>
      <div class="col-12 col-sm-10 col-lg-11 text-left">
        <h4 class="font-weight-bold">Our Achievement</h4>
        <p class="grey-text">Next Level Real Estate is one of the top property agencies in Dubai and it wasn’t possible without your trust in us. </p>
      </div>
    </div>
    <!--Grid row-->
    <div class="row  mt-5 nl--achivements">
      <?php foreach ($achievements as $key => $value) {
        extract($value);
      
             echo '<div class="col-12 col-sm-4 col-md-4 text-center">
                      <img src="'.IMG."achievements/".$image.'" class="img-fluid">
                      <h5 class="mt-4">'.$title.'</h5>
                  </div>';
      } ?>
    </div>
  </section>
  <section class="pt-sm-5 mt-4 pb-sm-3">

    <div class="row mt-sm-2">
      <div class="nl--cards-button m-auto">
        <a href="" data-toggle="modal" data-target="#exampleModalCenter">Chat with us now </a>
      </div>
    </div>
    <!--Grid row-->
  </section>

  <!--Section: Group of personal cards-->
</div>
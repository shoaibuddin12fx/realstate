<div class="container">
  <!--Section: Group of personal cards-->
  <section class="pt-lg-5 mt-lg-3 pb-lg-3">
<div class="row nl--projects mb-5">
     <div class="col-12 col-sm-2 col-lg-1 col-md-1 nl--icon-box">
     <div class="nl--icon-line-top  d-none d-lg-block"></div> 
        <i class="fa fa-3x fa-diamond "></i>
      </div>
      <div class="col-12  col-sm-10 col-lg-11 col-md-11 text-left pl-sm-5 pt-sm-1">
        <h4 class="font-weight-bold">Featured Properties</h4>
        <p class="grey-text">Learn about the best properties available for sale and lease in Dubai only at Next Level Real Estate. Check out your dream properties.  </p>
      </div>
    </div>
    <!--Grid row-->
    <div class="row">
        <!--Card group-->

          <!--Card-->
          <?php foreach ($our_properties as $key => $value) {

            echo '<div class="nl--card-personal mb-4 col-lg-4 col-md-6 col-sm-6 ">
                    <div class="card ">
                      <!--Card image-->
                      <a href="'.SURL.'nl_listing/property_detail/'.$value['prop_id'].'">
                        <img class="card-img-top" src="'.IMG.'properties/'.$value['image'].'" alt="Card image cap">
                        
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      <!--Card image-->
                      <div class="location">
                        <span class="ml-1"><i class="fa fa-map-marker mr-1"></i>'.ucwords($value['address']).'</span>
                      </div>
                      <!--Card content-->
                      <div class="card-body">
                        <!--Title-->
                        <a>
                          <h4 class="card-title">'.ucwords($value['prop_title']).'</h4>
                        </a>
                        <!--Text-->
                        <p class="card-text">'.$value['prop_short_description'].'</p>
                        
                      </div>
                      <div class="card-footer bg-transparent">
                          <span><i class="fa fa-bookmark"></i>'.$value['diemensions'].' sq ft</span>
                          <span><i class="fa fa-bed"></i>'.$value['bedrooms'].' bedrooms</span>
                          <span><i class="fa fa-tint"></i>'.$value['washrooms'].' baths</span>
                      </div>
                      <!--Card content-->
                      <a href="'.SURL.'nl_listing/property_detail/'.$value['prop_id'].'" class="link">GET IN TOUCH</a>
                    </div>
                  </div>';

          } ?>
        <!--Card group-->
    </div>
    <!--Grid row-->
    <div class="register-call nl--cards-button mt-sm-5 mt-sm-5 m-auto text-center">
      <a href="#" class="m-auto" data-toggle="modal" data-target="#exampleModalCenter">LET US CALL YOU BACK !</a>
    </div>

  </section>
  <!--Section: Group of personal cards-->
</div>
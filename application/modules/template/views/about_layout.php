
<?php $this->load->view("include/header_nav"); ?>
<!--slider start-->
<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?=IMG.'sliders/'.$sliders[0]['image']?>" alt="Los Angeles" width="1100" height="500">
        <div class="caption">
          <img src="<?=IMG?>c0e30-marker.png" alt="Map marker" class="map-marker">
          <p class="address"><?=$sliders[0]['address']?></p>
          <h2><?=$sliders[0]['heading']?></h2>
          <p class="desc"><?=$sliders[0]['description']?></p>
          <a href="<?=$sliders[0]['link']?>" class="caption-btn"><?=$sliders[0]['button_title']?></a>
        </div>
      </div>
    </div>
  </div>
<!----slider end----->


<!--content start-->
<?php 
echo $page_detail[0]['content'] ;
?>
<!--content ends-->

<!-- SCRIPTS -->
<?php $this->load->view("include/front_footer"); ?>
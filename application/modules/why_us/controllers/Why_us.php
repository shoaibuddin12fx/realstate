<?php

class Why_us extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}
	function index(){
		$data['why_us']=$this->Crud->getRows('why_us');
		$data['content']='why_us/all_record';
		$this->template->admin_template($data);
	}
	function add_record(){
		$data['content']='why_us/add_record';
		$this->template->admin_template($data);
	}
	function insert_record(){
		$table = 'why_us';
		if($this->input->post('create')){
			extract($this->input->post());
			$created_date = date('Y-m-d H:i:s');

                      $filename_f='';
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','<h5 class="alert alert-danger">Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels </h5>');
   redirect(SURL.'why_us/add_record');
}else{    
                          
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/achievements/';
                        $projects_folder_path_main = './assets/images/achievements/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }

			$data = array(
				"title" => $title,
                "description"=>$description,
				"image" => $filename_f,
				"created_at" => $created_date,
				"status" => $status,
				
			);


			$inserted= $this->Crud->insert($table,$data);
			
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Record is added successfully</h5>');
                redirect(SURL.'why_us');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add record please try again</h5>');
                redirect(SURL.'why_us');
            }
		}
	}
	function edit_record($id){
		$table = 'why_us';
		$id = $id;
		$where = array('id' => $id );
		$data['why_us'] = $this->Crud->getRows($table,'*',$where);
		$data['content'] = 'why_us/edit_record';
		$this->template->admin_template($data);
	}
	function update_record($id){
		$table = 'why_us';
		$id = $id;
		$where = array('id' =>  $id);
		if($this->input->post('update_btn')){
			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');

			$filename_f = $udate_image ;


list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "1000" || $height > "1000") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 1000 * 1000 pixels ');
   redirect(SURL.'why_us/edit_record/'.$id);
}else{    

            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/achievements/';
                        $projects_folder_path_main = './assets/images/achievements/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 70;
                        $config_resize['height'] = 71;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                            $error_file_arr = array('error' => $this->upload->display_errors());
                            $filename_f="Testing";
                            print_r($error_file_arr);exit;
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
				}

			$data = array(
				"title" => $title,
                "description"=>$description,
				"image" => $filename_f,
				"updated_at" => $updated_at,
				"status" => $status,
				
			);
		}
			$updated = $this->Crud->update($table,$data,$where);
			if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">record is updated successfully</h5>');
                redirect(SURL.'why_us');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update record please try again</h5>');
                redirect(SURL.'why_us/edit_record/'.$id);
            }
		}
	function del_record($id){
			$table = 'why_us';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Record is delted successfully</h5>');
                redirect(SURL.'why_us');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete record please try again</h5>');
                redirect(SURL.'why_us');
            }
	}
	function activate_record($id){

        $table='why_us';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Record is activated successfully</h5>');
                redirect(SURL.'why_us');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate record please try again</h5>');
                redirect(SURL.'why_us');
            }
    }
    function deactivate_record($id){
        $table='why_us';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Record is deactivated successfully</h5>');
                redirect(SURL.'why_us');
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate record please try again</h5>');
                redirect(SURL.'why_us');
            }
    }
}

 <?php
class Our_partners extends MY_Controller
{
	function __construct(){
		parent::__construct();
	}

	function index()
	{
		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
	}

	function add_partners()
	{        
		$data['content']='our_partners/add_partners';
		$this->template->admin_template($data);
	}
    
    function insert_partner(){
		$table = 'our_partners';
		 if($this->input->post('create')){
		 	extract($this->input->post());
		 	$created_date = date('Y-m-d H:i:s');

                      $filename_f='';
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "88" || $height > "88") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 88 * 88 pixels ');
		$data['content']='our_partners/add_partners';
		$this->template->admin_template($data);
}else{            
                    if ($_FILES['image']['name'] != "") {
                        
                        $projects_folder_path = './assets/images/partners/';
                        $projects_folder_path_main = './assets/images/partners/';
                        $width=1920;
                        $height=800;

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config['quality'] = 100;
                        $config['overwrite'] = TRUE;    
                        $config['width'] = $width;
                        $config['height'] = $height;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                		$data['content']='our_partners/add_partners';
		$this->template->admin_template($data);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main,$width,$height);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
		 	$data = array(
		 		"name"=>$name,
		 		"description"=>$description,
		 		"order"=>$order,
		 		"image"=>$filename_f,
		 		"status"=>$status,
		 		"created_at"=>$created_date,	
		 	);
		 	$inserted = $this->Crud->insert($table,$data);
			if($inserted){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Partner is added successfully</h5>');
		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to add partner please try again</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }
		 }
	}


	function edit_partner($id){
		$table = 'our_partners';
		$id = $id;
        $where=array("id" =>$id);
		$data['partner']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_partners/update_partner';
		$this->template->admin_template($data);
	}


	function update_partner($id){
		$table = 'our_partners';
		$id = $id;
		$where = array("id"=> $id);
		if($this->input->post('update_btn')){

			extract($this->input->post());
			$updated_at = date('Y-m-d H:i:s');
			$filename_f = $udate_image ;
list($width, $height) = getimagesize($_FILES["image"]['tmp_name']);

if($width > "88" || $height > "88") {
   $this->session->set_flashdata('message','Maximum Upload Image Dimensions Must Be Or Less Then 88 * 88 pixels ');
   		$data['partner']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_partners/update_partner';
		$this->template->admin_template($data);
}else{    
            if($_FILES['image']['name'] != ""){

                        $projects_folder_path = './assets/images/partners/';
                        $projects_folder_path_main = './assets/images/partners/';

                        $thumb = $projects_folder_path_main . 'thumb';
                        //$f240x240 = $projects_folder_path_main . '240x240';

                        $config['upload_path'] = $projects_folder_path;
                        $config['allowed_types'] = 'jpg|jpeg|gif|tiff|tif|png|JPG|JPEG|GIF|TIFF|TIF|PNG';
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = TRUE;
                         
                        $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                        $config['maintain_ratio'] = FALSE;
                        $config_resize['quality'] = 100;
                        $config_resize['overwrite'] = TRUE;    
                        $config_resize['maintain_ratio'] = FALSE;
                        $config_resize['width'] = 1920;
                        $config_resize['height'] = 800;

                        $this->load->library('image_lib', $config);

              
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('image')) {
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Please upload image instead of any other file</h5>');
                		$data['partner']=$this->Crud->getRows($table,'*',$where);
		$data['content'] = 'our_partners/update_partner';
		$this->template->admin_template($data);
                            
                        } else {

                            $data_image_upload = array('upload_image_data' => $this->upload->data());
                            $filename_f= $data_image_upload['upload_image_data']['file_name'];
                            $full_path = $data_image_upload['upload_image_data']['full_path'];
                            
                           $this->File_upload->create_optimize($filename_f, $full_path, $projects_folder_path_main);
                            
                             //create_thumbnail($filename, $full_path, $f240x240, 240, 240); 
                            
                        }
                    }
                }
			$data = array(
				"name"=>$name,
		 		"description"=>$description,
		 		"order"=>$order,
		 		"image"=>$filename_f,
		 		"status"=>$status,
		 		"created_at"=>$created_date,
		 		"updated_at"=>$updated_at,	
		 	);
		 	//print_r($data);exit();
		 	$updated = $this->Crud->update($table,$data,$where);
		 
		 	if($updated){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Partner is updated successfully</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to update partner please try again</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }
		}
	}
	function del_partner($id){
			$table = 'our_partners';
			$id= $id;
			$where = array("id"=>$id);
			$del=$this->Crud->delete($table,$where);
			if($del){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">Partner is deleted successfully</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to delete partner please try again</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }
	}
	function activate_partner($id){

        $table='our_partners';
        $id=$id;
        $data=array("status" => "true");
        $where=array("id" => $id);
        $activ=$this->Crud->update($table,$data,$where);
        if($activ){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">partner is activated successfully</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to activate partner please try again</h5>');
               		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }
    }
    function deactivate_partner($id){
        $table='our_partners';
        $id=$id;
        $where=array("id" => $id);
        $data=array("status" => "false");
        $deactive=$this->Crud->update($table,$data,$where);
        if($deactive){
                $this->session->set_flashdata('message','<h5 class="alert alert-success">partner is deactivated successfully</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }else{
                $this->session->set_flashdata('message','<h5 class="alert alert-danger">Failed to deactivate partner please try again</h5>');
                		$data['partners']=$this->Crud->getRows('our_partners','*');
		$data['content']='our_partners/all_partners';
		$this->template->admin_template($data);
            }
    }
}
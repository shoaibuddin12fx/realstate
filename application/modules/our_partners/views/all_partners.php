  <div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Our Partners</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<span><b>Our Partners</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'our_partners/add_partners' ;?>">
						<i class="fa fa-plus txt-success " title="Add partner"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content no-padding">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo $this->session->flashdata('message');}?>
						</div>
					</div>
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>Image</th>
							<th>Name</th>
							<th>Display Order</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<!-- Start: list_row -->
					<?php foreach($partners as $key => $value){
						extract($value);
						?>
						<tr>
							
							<td> <?php if(!empty($image)){ ?><img src="<?php echo SURL.'assets/images/partners/'.$image ?>">  <?php }  else echo '<img src="'.SURL.'assets/images/dummy.jpg">';?> </td>
							<td> <?php echo $name ?> </td>
							<td> <?php echo $order ?> </td>
							<td> <?php if($status == 'true'){echo 'Activated';}else{echo 'Deactivated';} ?> </td>
							
							<td>
                          <?php if($status == "true"){?>
                                <a href="<?php echo SURL.'our_partners/deactivate_partner/'.$id?>" title="Deactivate"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            <?php }else{?>
                                <a href="<?php echo SURL.'our_partners/activate_partner/'.$id?>" title="Activate"><i class="fa fa-check-circle" aria-hidden="true"></i></a>
                            <?php }?>

<?php 
$sess=$this->session->userdata('slug');
$cntt='';
$cntt=count($sess);
for($i=0;$i<$cntt;$i++){
if($sess[$i]=='our_partners/edit_partner'){

								echo'<a href="'.SURL.'our_partners/edit_partner/'.$id.'">
										<i class="fa fa-pencil" title="Update"></i>
									</a>'; }
if($sess[$i]=='our_partners/del_partner'){?>

								<a href="<?php echo SURL.'our_partners/del_partner/'.$id?>" onclick='return confirm(" Are you sure you want to delete?");'>
										<i class="fa fa-trash-o" title="Delete"></i>
									</a><?php }
}?>
							</td>
						</tr>
						
					<?php } ?>
					<!-- End: list_row -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
// Run Datables plugin and create 3 variants of settings
function AllTables(){
	TestTable1();
	LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
	$('select').select2();
	$('.dataTables_filter').each(function(){
		$(this).find('label input[type=text]').attr('placeholder', 'Search');
	});
}
$(document).ready(function() {
	// Load Datatables and run plugin on tables 
	LoadDataTablesScripts(AllTables);
	// Add Drag-n-Drop feature
	WinMove();
});
</script>

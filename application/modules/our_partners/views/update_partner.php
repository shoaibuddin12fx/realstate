<?php 

extract($partner[0]); ?>
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo AURL;?>">Dashboard</a></li>
			<li><a>Update member</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					
					<span><b>Edit member information</b></span>
				</div>
				<div class="box-icons">
					<a href="<?php echo SURL.'our_partners/add_partners'?>">
						<i class="fa fa-plus txt-success " title="Add New User"></i>
					</a>
					<a class="collapse-link"  title="Collapse">
						<i class="fa fa-chevron-up  txt-primary"></i>
					</a>
					<a class="expand-link"  title="Full Screen">
						<i class="fa fa-expand  txt-warning"></i>
					</a>
					<a class="close-link" title="Close">
						<i class="fa fa-times  txt-danger "></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<form id="defaultForm" method="post" action="<?php echo SURL.'our_partners/update_partner/'.$id?>" enctype="multipart/form-data" class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
						<?php if(!empty($this->session->flashdata('message'))){echo '<h5 class="alert alert-danger">'.$this->session->flashdata('message').'</h5>';}?>
						</div>
					</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Partner Name</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $name ?>" class="form-control" name="name" id="name" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-5">
								<textarea type="text" value="<?php echo $description ?>" class="form-control" name="description" id="description" ><?=$description?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Display Order</label>
							<div class="col-sm-5">
								<input type="number" value="<?php echo $order ?>" class="form-control" name="order" id="order" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Image</label>
							<div class="col-sm-2">
								<input type="file"  name="image" id="image" />
								<input type="hidden" id="udate_image" value="<?php echo $image ?>"  name="udate_image">
							</div>
							<div class="col-sm-2">
								<?php if(!empty($image)){echo "<img src=".SURL."assets/images/partners/".$image." width='100'>";}?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Status</label>
							<div class="col-sm-5">
								<select class="populate placeholder form-control" name="status" id="status">
									<option value="">-- Select a status --</option>
									<option value="true" <?php if($status == 'true'){echo 'selected';} ?> >Activate</option>
									<option value="false" <?php if($status == 'false'){echo 'selected';} ?> >Deactivate</option>
								</select>
							</div>
						</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" name="update_btn" id="update_btn" value="update_btn" class="btn btn-primary">Save</button>
							<a role="button" href="<?=SURL?>our_partners" class="btn btn-primary">Go Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
// Run Select2 plugin on elements
// Run timepicker

$(document).ready(function() {

	// Initialize datepicker
	// Load Timepicker plugin
	// Add tooltip to form-controls
	
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>

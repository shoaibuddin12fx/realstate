  <div id="demo" class="carousel slide" data-ride="carousel">

    <!-- The slideshow -->
    <div class="carousel-inner">
    <?php foreach ($sliders as $key => $value) {
      extract($value);
          if($key==0){
            $active='active';
          }else{
            $active='';
          }
    echo '<div class="carousel-item '.$active.'">
         <img src="'.IMG.'sliders/'.$image.'" alt="Los Angeles" width="1100" height="500">
         <div class="caption">
         <img src="'.IMG."/c0e30-marker.png".'" alt="Map marker" class="map-marker">
         <p class="address">'.$address.'</p>
         <h2>'.$heading.'</h2>
         <p class="desc">'.$description.' </p>
         <a href="'.$link.'" class="caption-btn">'.$button_title.'</a>
         </div>
        </div>';
    } ?>
    </div>
               <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
<div class="modal-body">
  <div class="container-fluid">

 <div class="forms_outer">      

      <form method="post" class="form-horizontal nl--slider-form position-relative nl--propertydetail-model" name="myForm1" id="myForm1" enctype="multipart/form-data" >
        <div id="reslt" style="text-align: center;"></div>  

        <fieldset>
          <!-- Text input-->
          <div class="form-group row">
            <div class="col-md-6 col-6">
              <input id="f_name" name="f_name" type="text" placeholder="First Name*" class="form-control input-md">
              <div id="fname"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="l_name" name="l_name" type="text" placeholder="Last Name*" class="form-control input-md">
              <div id="lname"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="email" name="email" type="text" placeholder="Email address*" class="form-control input-md">
              <div id="mail"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="c_code" name="c_code" type="text" placeholder="Country (+971)" class="form-control input-md">
              <div id="ccode"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="number" name="number" type="text" placeholder="Mobile Number*" class="form-control input-md">
              <div id="phone"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-6  col-6">
              <input id="residence" name="residence" type="text" placeholder="Residence*" class="form-control input-md">
              <div id="residc"></div>
            </div> 
            <div class="col-md-6  col-6">
              <input id="nationality" name="nationality" type="text" placeholder="Nationality*" class="form-control input-md">
              <div id="nationalty"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <input id="inquery" name="inquery" type="text" placeholder="My enquiry is about*" class="form-control input-md">
              <div id="inqury"></div>
            </div> 
          </div>
          <div class="form-group row">

            <div class="col-md-12  col-12">
              <textarea name="message" id="message" placeholder="Your Message*" class="form-control input-md" rows="4"></textarea>
              <div id="msg"></div>
            </div> 
          </div>

          <!-- Text input-->
          <div class="form-group row">

            <div class="col-md-4  col-8">
              <input type="button" value="Send" onclick=" inquerys()" class="form-control input-md">
            </div><div class="col-md-4  col-8">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </fieldset>
      </form>
    </div>
    </div>
  </div>
</div>
</div>
</div>
<!-- Model ends -->

    <!-- Left and right controls -->

  </div>
    <div class="nl--team">
    <div class="container">
      <div class="row mb-lg-5 pb-lg-5 mt-lg-5 bt-lg-5">
        <div class="col-md-6 col-lg-6 col-12 img mt-3">
          <img src="<?php if(!empty($manager[0]['image'])){ echo IMG.'team/'.$manager[0]['image'] ;}?>" class="img-fluid">
        </div>
        <div class="col-md-6 col-lg-6 col-12 detail ">
          <span class="d-none d-lg-block">MEET THE TEAM</span>
          <h2 class="mt-3"><?php if(!empty($manager[0]['name'])){echo $manager[0]['name']; }?></h2>
          <h4><?php if(!empty($manager[0]['designation'])){echo $manager[0]['designation']; }?></h4>
          <p class="display-on-desk"><?php if(!empty($manager[0]['description'])){echo ($manager[0]['description']);}?></p>
          <p class="display-on-tab"><?php if(!empty($manager[0]['description'])){
            $wrds=strlen($manager[0]['description']);
            echo substr($manager[0]['description'],0,206).'<span id="dots" class="team-span">...</span><span class="more team-span" id="more">'.substr($manager[0]['description'],228,$wrds).'</span>' ; }?>
          <button onclick="myFunction()" id="myBtn" class="team-desc-btn">read more</button>
          </p>
          <div class="marketing mt-lg-4 mt-xl-5">
            <div class="row">
              <div class="col-md-3">
                <img src="<?=IMG?>pie-chart-2.png" class="img-fluid">
              </div>
              <div class="col-md-2 mt-lg-2 pt-lg-0 ml-lg-0 pl-lg-1 pr-lg-0"> 
                <h2><?php if(!empty($manager[0]['first_strength'])){echo $manager[0]['first_strength']; }?><sub class="mt-0">%</sub></h2>
              </div>
              <div class="col-md-7 mt-lg-3 pl-lg-3">
                <h5><?php if(!empty($manager[0]['first_skill'])){echo $manager[0]['first_skill']; }?></h5>
                <p class="text-center text-md-left"><?php if(!empty($manager[0]['f_description'])){echo $manager[0]['f_description']; }?></p>
              </div>
            </div>
          </div>
          <div class="manager mt-lg-2 mt-xl-5">
            <div class="row">
              <div class="col-md-3">
                <img src="<?=IMG?>pie-chart-1.png" class="img-fluid">
              </div>
              <div class="col-md-2 mt-lg-2 pt-lg-0 ml-lg-0 pl-lg-1 pr-lg-0"> 
                <h2><?php if(!empty($manager[0]['second_strength'])){echo $manager[0]['second_strength']; }?><sub class="mt-0">%</sub></h2>
              </div>
              <div class="col-md-7 mt-lg-3 pl-lg-3">
                <h5><?php if(!empty($manager[0]['second_skill'])){echo $manager[0]['second_skill']; }?></h5>
                <p class="text-center text-md-left"><?php if(!empty($manager[0]['s_description'])){echo $manager[0]['s_description']; }?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Page Content -->
      <div class="container mt-40">
        <div class="row mt-30">
          <?php foreach ($team as $key => $value) {?>
          
            <div class="col-md-4 col-sm-6 mt-4 text-center">
                                <div class="box19">
                                  <img src="<?= IMG.'team/'.$value['image']?>" alt=""  class="img-fluid">
                                  <div class="box-content">
                                    <ul class="icon">
            <?php if(!empty($value['facebook'])){?>  <li><a href="<?=$value['facebook']?>"><i class="fa fa-facebook"></i></a></li><?php }?>
            <?php if(!empty($value['twitter'])){?>  <li><a href="<?=$value['twitter']?>"><i class="fa fa-twitter"></i></a></li><?php }?>
            <?php if(!empty($value['gplus'])){?>  <li><a href="<?=$value['gplus']?>"><i class="fa fa-google-plus"></i></a></li><?php }?>
            <?php if(!empty($value['insta'])){?>  <li><a href="<?=$value['insta']?>"><i class="fa fa-instagram"></i></a></li><?php }?>
                                    </ul>
                                  </div>
                                </div>
                                <h4 class="pt-3"><?=$value['name']?></h4>
                              </div>
         <?php } ?>
        </div>
      </div>
    </div>
  </div>
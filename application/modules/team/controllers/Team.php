<?php

class Team extends MY_Controller {

	function __construct(){

		parent::__construct();
	}
		
	function index()
	{	
           $data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"team"),'id','desc','','');
	   $data['team']=$this->Crud->getRows('our_team','*',array("status"=>"true","slug !="=>"managing director"));
	   $data['manager']=$this->Crud->getR('our_team','*',array("slug"=>"managing director"),'id','desc','',1);
           $data['content']='team/team_view';
           $this->template->listing_page_layout($data);
	}
}
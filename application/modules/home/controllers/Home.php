<?php

class Home extends MY_Controller {

	function __construct(){

		parent::__construct();
	}
		
	function index()
	{	
       $data['sliders']=$this->Crud->getR('sliders','*',array("status"=>"true","page"=>"home"),'display_order','asc','','');
       $data['achievements']=$this->Crud->getR('achievements','*',array("status"=>"true"),'','','','');
       $data['blogs']=$this->Crud->getR('news','*',array("status"=>"true"),'id','desc','','3');
       $data['testimonials']=$this->Crud->getR('testimonial','*',array("status"=>"true"),'id','desc','','');
       $data['projects']=$this->Crud->join_on_tbl('properties','our_projects','properties.project_id=our_projects.id','*,our_projects.id as proj_id,our_projects.image as proj_image,our_projects.short_description as proj_short_desc,our_projects.title as proj_title','project_categories','our_projects.category_id=project_categories.cat_id',array("our_projects.status"=>"true","our_projects.feature"=>"yes"),'properties.price','asc');
       $data['our_properties']=$this->Crud->getR('properties','*',array("status"=>"true","isfeature"=>"yes"),'prop_id','desc','','6');
       $this->template->realestate_template($data);
	}
	function inquery_from(){
		

		$f_name=$this->input->post('f_name');
		$email=$this->input->post('email');
		$c_code=$this->input->post('c_code');
		$nationality=$this->input->post('nationality');
		$phone=$this->input->post('number');
		$inquery=$this->input->post('inquery');
		$message=$this->input->post('message');
		$page=$this->input->post('page');
		$table='inquery_data';
		$this->form_validation->set_rules('number', 'Phone number', 'required',array('required' => 'Number is required.'));
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('inquery', 'Inquery', 'required');
		$this->form_validation->set_rules('f_name', 'First Name', 'required|min_length[2]|max_length[25]|alpha',array('min_length[2]' => 'First Name length must be greater then 2.','required'=>'First name is required'));
		$this->form_validation->set_rules('nationality', 'Nationality', 'required|alpha',array('alpha' => 'You must enter the alphabets only.','required'=>'Nationality is required'));
		$this->form_validation->set_rules('c_code', 'Country code', 'required|min_length[2]|max_length[20]|alpha_numeric',array('required' => 'Country code is required.'));
		$this->form_validation->set_rules('message', 'Message', 'required',array('required' => 'Message is required.'));
		
		if($this->form_validation->run()== false){
			$arr = array(
			    'phone' => form_error('number'),
			    'email' => form_error('email'),
			    'message' => form_error('message'),
			    'c_code' => form_error('c_code'),
			    'inquery' => form_error('inquery'),
			    'nationality' => form_error('nationality'),
			    'f_name' => form_error('f_name')
			);
			echo json_encode($arr);
		}else{
                        $created_date=date('Y-m-d H:i:s');
			$data = array(
		 		"first_name"=>$f_name,
		 		"phone"=>$phone,
		 		"country_code"=>$c_code,
                                "email"=>$email,
                                "message"=>$message,
                                "page"=>$page,
                                "enquiry"=>$inquery,
                                "nationality"=>$nationality,
		 		"request_date"=>$created_date,	
		 	);
		 	$this->Crud->insert("customer_enquiry",$data);
			$data['footer']=$this->Crud->getRows('site_settings','*','','id','DESC');
			$to_email = $data['footer'][0]['contact_page_email'];
			$this->load->library('email');

			$config = Array(
			    'protocol' =>  $data['footer'][0]['email_protocol'],
			    'smtp_host' =>  $data['footer'][0]['email_host'],
			    'smtp_port' => $data['footer'][0]['email_port'],
			    'smtp_user' => $data['footer'][0]['contact_page_email'],
			    'smtp_pass' => $data['footer'][0]['email_password'],
			     
			    'smtp_crypto'=>'ssl',
			    'mailtype'  => 'html',
			  'charset'   => 'iso-8859-1'
			            );

			 $this->load->library('email',$config); 
   
	         $this->email->from($email, $f_name); 
	         $this->email->to($to_email);
	         $this->email->subject($inquery); 
	         $this->email->message('Name: '.$f_name.'<br>Phone: '.$phone.'<br>  Country: '.$nationality.'<br>Country Code: '.$c_code.'<br>Message: '.$message); 
	   
	         //Send mail 
	         if($this->email->send()) {
	         	$arr=array('reslt'=>'<span class="text-success">Your Email Is Sent Successfully<span>');
	         	echo json_encode($arr);
	         }
	         else{	   

	         	$arr=array('reslt'=>'<span class="text-danger">Email Is Not Sent Please Try Again<span>');
	         	echo json_encode($arr);
	         }

		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function getservices_by_catid($cat_id='',$page=1){

	$perpage=4;

	$ci =& get_instance();
  if($page>1){
    $offset=($page-1)*$perpage;
  }else{
  $offset=0;

  }	
	
		$ci->db->limit($perpage,$offset);

	// if($cat_id!=0){
	// 	$query =$ci->db->get_where('our_services',array('category_id'=>$cat_id,'status'=>'true'));
	// }else{
		$query = $ci->db->get_where('our_services',array('status'=>'true'));
	//}
//echo    $ci->db->last_query();exit();
  $tab='';
    $row = $query->result_array();
   	foreach ($row as $key => $value) {
   		if($key % 2==0){
        	$float='left';
        	$color='';
        	$text_float='';
        }else{
            $float='right';
        	$text_float='-left';
        	$color='gray';
        }
        $tab.='<div class="clearfix"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padd pull-'.$float.'">
                     <a href="'.SURL.'services/service_in_detail/'.$value['slug'].'"> <img src="'.IMG.'sliders/'.$value['image'].'" alt="" class="img-responsive"></a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="services-text'.$text_float.'">
                        <a style="text-decoration:none;" href="'.SURL.'services/service_in_detail/'.$value['slug'].'"><h1 class="black">'.$value['title'].'</h1></a>
                        <p>'.word_limiter($value['description'],37).'</p><a href="'.SURL.'services/service_in_detail/'.$value['slug'].'">Read More</a>
                        <h4>Tools Used</h4>
                        <img src="'.IMG.'icon-tool1.png" alt="">
                        <img src="'.IMG.'icon-tool2.png" alt="">
                        <img src="'.IMG.'icon-tool3.png" alt="">
                        <img src="'.IMG.'icon-tool4.png" alt="">
                      </div>
                 </div><div class="clearfix '.$color.'"></div>';

    }
    return $tab;
 
}
function gettotalrows($cat_id=''){
  $ci =& get_instance();
    // if($cat_id!=0){
    //   $ci->db->where('category_id',$cat_id);
    // }
  $ci->db->where('status','true');
  $ci->db->from('our_services');
  $xyz=$ci->db->count_all_results();
  return $xyz;
}
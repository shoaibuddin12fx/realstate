/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'cleanuploader';
	config.allowedContent = true;
	config.protectedSource.push(/<i[^>]*><\/i>/g);

	   config.extraAllowedContent = 'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};i(*)[*]{*};ul(*)[*]{*};h2(*)[*]{*};h3(*)[*]{*};h4(*)[*]{*};h5(*)[*]{*};a(*)[*]{*};*(*);*{*};';
config.protectedSource.push(/<span[^>]*><\/span>/g);
};  


/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

//CKEDITOR.replace( 'editor1', {
//  extraPlugins: 'imageuploader'
//});



